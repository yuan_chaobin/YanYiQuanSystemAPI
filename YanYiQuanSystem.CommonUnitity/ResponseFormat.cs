﻿using System;
using System.Collections.Generic;

namespace YanYiQuanSystem.CommonUnitity
{
    public class ResponseFormat<T>
    {
        /// <summary>
        /// 返回状态码 
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 数据数量
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 记录错误信息
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 返回数据
        /// </summary>
        public T Data { get; set; }
    }
    public class DtreeFormat
    {
        public int code { get; set; } = 200;

        public string msg { get; set; } = "操作成功";

        public List<TreeNodes> data { get; set; }
    }

    public class TreeNodes
    {

        /// <summary>
        /// 菜单id
        /// </summary>
        /// 
        /// 
      
        public string value { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        /// 
  
        public string label { get; set; }
        /// <summary>
        /// 菜单URL
        /// </summary>
        /// 
        public List<TreeNodes> children { get; set; }

        public string MenuUrl { get; set; }
        /// <summary>
        /// 菜单父ID
        /// </summary>
        ///      


        public string MenuPid { get; set; }
        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        /// s  
        public int Isdelete { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        /// 

        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string MenuIcon1 { get; set; }
        public string LabelTypeID { get; set; }
        public string LabelName { get; set; }

        public string LabelPid { get; set; }

    }

}
