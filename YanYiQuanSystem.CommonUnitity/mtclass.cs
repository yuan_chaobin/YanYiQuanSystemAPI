﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using static YanYiQuanSystem.CommonUnitity.mtclass.Lamdba;

namespace YanYiQuanSystem.CommonUnitity
{
    public static class mtclass
    {
        public class Lamdba
        {
            /// <summary>
            /// 对Lamdba表达式进行封装扩展
            /// </summary>
            public class ParameterRebinder : ExpressionVisitor
            {
                private readonly Dictionary<ParameterExpression, ParameterExpression> map;
                public ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
                {
                    this.map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
                }
                public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
                {
                    return new ParameterRebinder(map).Visit(exp);
                }
                protected override Expression VisitParameter(ParameterExpression p)
                {
                    ParameterExpression replacement;
                    if (map.TryGetValue(p, out replacement))
                    {
                        p = replacement;
                    }
                    return base.VisitParameter(p);
                }
            }
        }
    }


    
}
public static class LambdaHelper
{
    public static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
    {
        var map = first.Parameters.Select((f, i) => new { f, s = second.Parameters[i] }).ToDictionary(p => p.s, p => p.f);
        var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);
        return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
    }
    /// <summary>
    /// 添加And条件
    /// </summary>
    /// <typeparam name="T"><peparam>
    /// <param name="first"></param>
    /// <param name="second"></param>
    /// <returns></returns>
    public static Expression<Func<T, bool>> And<T>(
    this Expression<Func<T, bool>> first,
    Expression<Func<T, bool>> second)
    {
        return first.AndAlso<T>(second, Expression.AndAlso);
    }
    /// <summary>
    /// 添加Or条件
    /// </summary>
    /// <typeparam name="T"><peparam>
    /// <param name="first"></param>
    /// <param name="second"></param>
    /// <returns></returns>
    public static Expression<Func<T, bool>> Or<T>(
        this Expression<Func<T, bool>> first,
        Expression<Func<T, bool>> second)
    {
        return first.AndAlso<T>(second, Expression.OrElse);
    }
    /// <summary>
    /// 合并表达式以及参数
    /// </summary>
    /// <typeparam name="T"><peparam>
    /// <param name="expr1"></param>
    /// <param name="expr2"></param>
    /// <param name="func"></param>
    /// <returns></returns>
    private static Expression<Func<T, bool>> AndAlso<T>(
    this Expression<Func<T, bool>> expr1,
    Expression<Func<T, bool>> expr2,
    Func<Expression, Expression, BinaryExpression> func)
    {
        var parameter = Expression.Parameter(typeof(T));

        var leftVisitor = new ReplaceExpressionVisitor(expr1.Parameters[0], parameter);
        var left = leftVisitor.Visit(expr1.Body);

        var rightVisitor = new ReplaceExpressionVisitor(expr2.Parameters[0], parameter);
        var right = rightVisitor.Visit(expr2.Body);

        return Expression.Lambda<Func<T, bool>>(
            func(left, right), parameter);
    }
    private class ReplaceExpressionVisitor : ExpressionVisitor
    {
        private readonly Expression _oldValue;
        private readonly Expression _newValue;
        public ReplaceExpressionVisitor(Expression oldValue, Expression newValue)
        {
            _oldValue = oldValue;
            _newValue = newValue;
        }
        public override Expression Visit(Expression node)
        {
            if (node == _oldValue)
                return _newValue;
            return base.Visit(node);
        }
    }
    public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
    {
        HashSet<TKey> seenKeys = new HashSet<TKey>();
        foreach (TSource element in source)
        {
            if (seenKeys.Add(keySelector(element)))
            {
                yield return element;
            }
        }
    }
}

