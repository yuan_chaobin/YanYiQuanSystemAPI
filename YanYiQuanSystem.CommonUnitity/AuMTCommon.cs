﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.CommonUnitity
{
    /// <summary>
    /// 认证模特的帮助类
    /// </summary>
    public   class AuMTCommon
    {
    public string    ModelName{get;set;}
    public string      PnoneNumber{get;set;}
    public string      IdentityNumber{get;set;}
    public int?      IsIdentity{get;set;}
    public int?      Sex{get;set;}
    public int?      Weight{get;set;}
    public string      PresentAddress{get;set;}
    public string      NickName{get;set;}
    public string      Constellation{get;set;}
    public int? Height {get;set;}
    public int? Size {get;set;}
    public int? TheChest {get;set;}
    public int? Waistline {get;set;}
    public int? Hipline {get;set;}
    public string      BloodType{get;set;}
        /// <summary>
        /// 审核人
        /// </summary>
        public string AuditName { get; set; }
        /// <summary>
        /// 证件照片
        /// </summary>
        public string zjZhaopian { get; set; }

        /// <summary>
        /// 营业执照
        /// </summary>
        public string Yingyezhizhao { get; set; }
        /// <summary>
        /// 邀请码
        /// </summary>
        public string InvitationCode { get; set; }

        public DateTime ? Birthday { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Standby2 { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPortrait { get; set; }
    }
}
