﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.CommonUnitity
{
    public  class AuGScommon
    {
      public  string   GSHeadPortraits{get;set;}
      public  string     Name{get;set;}
      public  string     PnoneNumber{get;set;}
      public  string     IdentityNumber{get;set;}
      public  string     IsIdentity{get;set;}
      public  int?     Sex{get;set;}
      public  string     InvitationCode{get;set;}//邀請
      public  int?     CompanyType{get;set;}//企業類型
      public  string     CompanyName{get;set;}//公司名稱
      public  string     CompanyPhone{get;set;}//公司電話
      public  string     CompanyPerson{get;set;}//公司法人
      public  string     CompanyAddress{get;set;}//公司地址
      public  string     TaxpayerNum{get;set;}//纳税人识别号
      public  string     WorkPosition{get;set;}//工作崗位
        public string GSemail { get; set; }//公司邮箱
    }
}
