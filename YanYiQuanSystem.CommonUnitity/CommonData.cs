﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.CommonUnitity
{
    public class CommonData
    {
        public string ModelId { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        
        public string NickName { get; set; }
        public int? IsFirstPage { get; set; }

        /// <summary>
        /// 用户外键ID
        /// </summary>

        public string UserID { get; set; }

        /// <summary>
        /// 是否有助理 1:是  2: 否
        /// </summary>
        public int? IsAssistant { get; set; }


        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegisterTime { get; set; }

        /// <summary>
        /// 邀请码
        /// </summary>
        
        public string InvitationCode { get; set; }

        /// <summary>
        /// 我的邀请码
        /// </summary>
        
        public string MyCode { get; set; }

        /// <summary>
        /// 星座
        /// </summary>
        
        public string Constellation { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        public int? Height { get; set; }

        /// <summary>
        /// 体重
        /// </summary>
        public int? Weight { get; set; }

        /// <summary>
        /// 鞋码
        /// </summary>
        public int? Size { get; set; }

        /// <summary>
        /// 胸围
        /// </summary>
        public int? TheChest { get; set; }

        /// <summary>
        /// 腰围
        /// </summary>
        public int? Waistline { get; set; }

        /// <summary>
        /// 臀围
        /// </summary>
        public int? Hipline { get; set; }

        /// <summary>
        /// 血型
        /// </summary>
        public string BloodType { get; set; }

        /// <summary>
        /// 是否参加活动  1: 是  2:否
        /// </summary>
        public int? IsActivity { get; set; }

        /// <summary>
        /// 活动表外键ID
        /// </summary>
        public int? ActivityId { get; set; }

        /// <summary>
        /// 登录账户
        /// </summary>
        
        public string LoginAccount { get; set; }

        /// <summary>
        /// 是否允许登陆  1:允许  2:不允许
        /// </summary>
        public int? IsLogging1 { get; set; }

        /// <summary>
        /// 认证状态    1:未认证   2:已认证
        /// </summary>
        public int? ApproveStatus { get; set; }

        /// <summary>
        /// 相册状态   1:有待审核的照片  2:已审核
        /// </summary>
        public int? PhotoStatus { get; set; }

        /// <summary>
        /// 登录状态  1:未登录  2:已登录
        /// </summary>
        public int? LoggingStatus { get; set; }

        public string UserId { get; set; }

        
        public string UserName { get; set; }

        
        public string Password { get; set; }

        public int? Sex { get; set; }

        /// <summary>
        /// 当前角色  1: 模特 2:企业  3: 个人企业  4: 助理
        /// </summary>
        public int? CurrentRole { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPortrait { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        
        public string IdentityNumber { get; set; }

        public string AssistantId { get; set; }



        /// <summary>
        /// 是否允许登录 1:允许 2: 不允许
        /// </summary>
        public int IsLogging { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>

        public string PhoneNumber { get; set; }

        public string Assistant_ModelId { get; set; }

        /// <summary>
        /// 助理人ID
        /// </summary>
        public string AssistantID { get; set; }

        /// <summary>
        /// 模特ID
        /// </summary>
        public string ModelID { get; set; }

        /// <summary>
        /// 当前居住地
        /// </summary>
        /// 


        public string PresentAddress { get; set; }

        /// <summary>
        /// 用户等级
        /// </summary>
        
        public string UsersRank { get; set; }

        public string HrId { get; set; }

        /// <summary>
        /// 用户外键ID
        /// </summary>
        
        public string UsersID { get; set; }

       

        /// <summary>
        /// 企业类型 1:个人  2:企业
        /// </summary>
        public int? CompanyType { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        
        public string CompanyName { get; set; }

        /// <summary>
        /// 公司电话
        /// </summary>
        
        public string CompanyPhone { get; set; }

        /// <summary>
        /// 公司法人
        /// </summary>
        
        public string CompanyPerson { get; set; }

        /// <summary>
        /// 公司地址
        /// </summary>
        public string CompanyAddress { get; set; }

        /// <summary>
        /// 纳税人识别号
        /// </summary>
        
        public string TaxpayerNum { get; set; }

        /// <summary>
        /// 工作岗位
        /// </summary>
        public string WorkPosition { get; set; }

        /// <summary>
        /// 认证状态
        /// </summary>
        public int? ApproveState { get; set; }

        public string SensibilityManagerID { get; set; }
        /// <summary>
        /// 敏感字名称
        /// </summary>
        public string SensibilityManagerName { get; set; }
        public string UsefulExpressionsManagerID { get; set; }
        /// <summary>
        /// 常用语管理名称
        /// </summary>

        public string UsefulExpressionsManagerName { get; set; }
        /// <summary>
        /// 常用语排序id
        /// </summary>
        public int? UsefulOrderId { get; set; }

        /// <summary>
        /// 菜单id
        /// </summary>
        /// \
        /// 

        public string MenuID { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        /// 

        public string MenuName { get; set; }
        /// <summary>
        /// 菜单URL
        /// </summary>
        /// 
        public string MenuUrl { get; set; }
        /// <summary>
        /// 菜单父ID
        /// </summary>
        ///      
        public string MenuPid { get; set; }
        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        /// s  
        public int? Is_delete { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        /// 

        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string MenuIcon1 { get; set; }

        /// <summary>
        /// 短信id
        /// </summary>

        public string NodeID { get; set; }


        /// <summary>
        /// 验证码
        /// </summary>

        public string VerificationCode { get; set; }


        public string PhotoId { get; set; }


        /// <summary>
        /// 审核状态  1  通过   2  拒绝
        /// </summary>
        public int? AuditStatus { get; set; }

        public string ProductId { get; set; }

        public string CanPinID { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 实际金额
        /// </summary>

        public string ActualAmount { get; set; }
        /// <summary>
        /// 原始金额
        /// </summary>

        public string Originalamount { get; set; }
        /// <summary>
        /// 天数
        /// </summary>

        public int? DayNumber { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public int? IsState { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        /// 

        public string Remark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int OrderById { get; set; }
        /// <summary>
        /// 用户表权益表关联主键 用来查询手机号
        /// </summary>
        /// 
        public string UserRightsId { get; set; }
        /// <summary>
        /// 赠送权益天数
        /// </summary>
        public int GiveAsDay { get; set; }

        /// <summary>
        /// 系统通知id
        /// </summary>
    
        public string InfromID { get; set; }
        /// <summary>
        /// 内容
        /// </summary>

        public string Content { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>


        /// <summary>
        /// 是否有效
        /// </summary>
        public int IsValid { get; set; }


        /// <summary>
        /// 权益适用角色
        /// </summary>
        public int RightsRole { get; set; }

        /// <summary>
        /// 权益名称
        /// </summary>
        public string RightsName { get; set; }

        /// <summary>
        /// 权益类型
        /// </summary>
        public int RightsType { get; set; }

        /// <summary>
        /// 权益类型方式
        /// </summary>
        public int RightsTypeWay { get; set; }

        /// <summary>
        /// 次数
        /// </summary>
        public int Times { get; set; }

        /// <summary>
        /// 权益开始时间
        /// </summary>
        public DateTime StratTime { get; set; }

        /// <summary>
        /// 权益结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 权益说明
        /// </summary>
        public string Remarks { get; set; }

        public string PurchaseRecordId { get; set; }

        /// <summary>
        /// 用户权益外键ID
        /// </summary>
        public string UserRightsID { get; set; }

        /// <summary>
        /// 购买时角色 1: 模特 2:企业  3: 个人企业  4: 助理
        /// </summary>
        public int BuyRole { get; set; }


        /// <summary>
        /// 金额
        /// </summary>
        public Decimal MoneySum { get; set; }

        /// <summary>
        /// 请求支付时间
        /// </summary>
        public DateTime DemandPayTime { get; set; }

        /// <summary>
        /// 成功支付时间
        /// </summary>
        public DateTime SuccessPayTime { get; set; }

        public string EmpId { get; set; }//员工Id

       
        public string EmpName { get; set; }//员工姓名

        public string Phone { get; set; }//手机号
     
 
  
        public string RoleId { get; set; }//角色名
 
        public string IdentityCard { get; set; }//身份证
   
        public string UserPass { get; set; }//登录密码

        public int IsStates { get; set; }//状态


    
        public string RoleName { get; set; }//角色名称

        public string FirstModelId { get; set; }


        public int SortNum { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// 荣誉说明
        /// </summary>
        public string HonorThat { get; set; }

        public string AttentionId { get; set; }

       
        public int UserType { get; set; }//用户类型

        public string FollowerID { get; set; }//关注人ID

        public int FollowerType { get; set; }//关注人类型

        public DateTime AttentionTime { get; set; }//关注时间

    }
}
