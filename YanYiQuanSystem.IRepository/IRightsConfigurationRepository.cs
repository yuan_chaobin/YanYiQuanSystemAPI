﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IRepository
{
    using YanYiQuanSystem.Domain;
    public interface IRightsConfigurationRepository:IRepository<RightsConfiguration>
    {
    }
}
