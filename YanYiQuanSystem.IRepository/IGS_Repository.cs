﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.CommonUnitity;
using YanYiQuanSystem.Domain;

namespace YanYiQuanSystem.IRepository
{
    public interface IGS_Repository : IRepository<Authentication_GS>
    {
        Task<bool> UpdateUserModel(CommonData entity);

        Task<bool> UpdateUserModelZL(CommonData entity);

    }
}
