﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.CommonUnitity;
using YanYiQuanSystem.Domain;

namespace YanYiQuanSystem.IRepository
{

    public  interface IMT_Repository: IRepository<Authentication_MT>
    {
        Task<bool> wxUpdateUserModel(CommonData entity);

        Task<bool> wxExitorImg(string id, string img);
    }
}
