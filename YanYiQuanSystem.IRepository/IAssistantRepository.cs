﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.CommonUnitity;
using YanYiQuanSystem.Domain;

namespace YanYiQuanSystem.IRepository
{
    public interface IAssistantRepository:IRepository<Assistant>
    {
        Task<bool> UpdateAssistant(CommonData entity);
    }
}
