﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.CommonUnitity;
using YanYiQuanSystem.Domain;

namespace YanYiQuanSystem.IRepository
{
    public interface IModelLibraryRepository : IRepository<ModelsLibrary>
    {
        Task<bool> UpdateFirstPage(List<string> Ids);

        Task<bool> UpdateUserModel(CommonData entity);

        Task<bool> UpdateUserRank(User user);

        Task<bool> UpdateAuditStatus(string photoId);

        Task<bool> UpdateFirst(ModelsLibrary modelsLibrary);
    }
}
