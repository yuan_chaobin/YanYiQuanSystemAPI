﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IRepository
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;

    public interface IReCompanyRepository : IRepository<RecruitmentCompanys>
    {
        Task<bool> UpdateCompany(CommonData entity);
    }
}
