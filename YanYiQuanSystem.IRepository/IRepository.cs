﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IRepository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;

    public interface IRepository<T>
    {
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> AddData(T entity);


        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<T> entity);


        /// <summary>
        /// 根据Id删除单个数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteData(string Id);


        /// <summary>
        /// 批量删除Ids
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task<bool> BatchDelete(List<string> Ids);

        

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateData(T entity);

        

        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<T>> GetListAsync();

        /// <summary>
        /// 根据条件获取数据
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<T>> GetListByWhereAsync(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<T>> GetListByPage<TKey>(Expression<Func<T, bool>> whereExpression, Expression<Func<T, TKey>> orderExpression, int pageIndex, int pageSize);

    }
}
