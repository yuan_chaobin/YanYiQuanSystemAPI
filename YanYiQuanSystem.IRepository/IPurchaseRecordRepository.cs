﻿using System;
using System.Collections.Generic;
using System.Text;
using YanYiQuanSystem.Domain;

namespace YanYiQuanSystem.IRepository
{
    public interface IPurchaseRecordRepository:IRepository<PurchaseRecord>
    {
    }
}
