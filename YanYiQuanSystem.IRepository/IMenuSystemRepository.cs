﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IRepository
{
    using YanYiQuanSystem.Domain;
    /// <summary>
    /// 菜单仓储接口层
    /// </summary>
    public interface IMenuSystemRepository:IRepository<MenuSystem>
    {

    }
}
