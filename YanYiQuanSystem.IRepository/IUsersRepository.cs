﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IRepository
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;

    public interface IUsersRepository:IRepository<User>
    {
        Task<bool> UpdateCurrentRole(User user);
    }
}
