﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public  interface ILabelService
    {
        /// <summary>
        /// 获取标签数据
        /// </summary>
        /// <returns></returns>
        Task<List<Label>> GetLabels();
        Task<bool> AddLabel(Label label);
        Task<bool> DeleteLabel(string Id);
        
    }
}
