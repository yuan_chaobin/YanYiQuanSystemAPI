﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public interface IRole_MenuService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        Task<bool> AddEmp(Role_Menu entity);
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        Task<List<Role_Menu>> GetRole_MenuAsync();
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<Role_Menu>> GetRole_MenuWhereAsync(Expression<Func<Role_Menu, bool>> whereExpression);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <returns></returns>
        Task<bool> DeleteRole_Menu(string Id);
        /// <summary>
        /// 根据菜单Id条件查询
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<Role_Menu>> GetRole_MenuWhereMenuId(Expression<Func<Role_Menu, bool>> whereExpression);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<Role_Menu> entity);
    }
}
