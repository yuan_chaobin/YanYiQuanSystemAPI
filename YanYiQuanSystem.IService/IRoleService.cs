﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public interface IRoleService
    {
        /// <summary>
        /// 获取角色数据
        /// </summary>
        /// <returns></returns>
        Task<List<Role>> GetRoleAsync();
        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        Task<bool> AddRole(Role entity);
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <returns></returns>
        Task<bool> BatchAddRole(List<Role> entity);
        /// <summary>
        /// 删除菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> DeleteRole(string Id);
        /// <summary>
        /// 批量删除菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> BatchDeleteRole(List<Role> entity);
        /// <summary>
        /// 修改菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> UpdateRole(Role entity);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<Role>> GetRoleWhereAsync(Expression<Func<Role, bool>> whereExpression);

        /// <summary>
        /// 分页显示
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <param name=""></param>
        /// <returns></returns>
        Task<List<Role>> GetListByPage(Expression<Func<Role, bool>> whereExpression, Expression<Func<Role>> orderExpression, int pageIndex, int pageSize);
    }
}
