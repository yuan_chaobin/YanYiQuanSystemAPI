﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    
    public interface IUsefulExpressionsManagerService
    {
        /// <summary>
        /// 获取常用语数据
        /// </summary>
        /// <returns></returns>
        Task<List<UsefulExpressionsManager>> GetUsefulExpressionsManagerAsync();
        /// <summary>
        /// 添加常用语
        /// </summary>
        /// <param name="usefulExpressionsManager"></param>
        /// <returns></returns>
        Task<bool> AddUsefulExpressionsManager(UsefulExpressionsManager usefulExpressionsManager);
        /// <summary>
        /// 删除常用语
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteUsefulExpressionsManager(string Id);
        /// <summary>
        /// 修改常用语
        /// </summary>
        /// <param name="usefulExpressionsManager"></param>
        /// <returns></returns>
        Task<bool> UpdateUsefulExpressionsManager(UsefulExpressionsManager usefulExpressionsManager);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<UsefulExpressionsManager>> GetUsefulExpressionsManagerWhereAsync(Expression<Func<UsefulExpressionsManager, bool>> whereExpression);

    }
}
