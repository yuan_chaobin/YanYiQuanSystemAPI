﻿using System;
using System.Collections.Generic;
using System.Text;


namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public interface ISystemInfromService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="systemInfrom"></param>
        /// <returns></returns>
        Task<bool> AddSystemInfrom(SystemInfrom systemInfrom);

        Task<bool> UpdateSystemInfrom(SystemInfrom systemInfrom);

        Task<bool> DeleteSystemInfrom(string id);
        Task<List<SystemInfrom>> GetSystemInfromBywere(Expression<Func<SystemInfrom, bool>> whereExpression);
        Task<List<SystemInfrom>> GetSystemInfrom();
    }
}
