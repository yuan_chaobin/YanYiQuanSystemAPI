﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;

    public interface IPhotoAblumService
    {
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateData(PhotoAlbums entity);

        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<PhotoAlbums>> GetListAsync();
        /// <summary>
        /// 批量删除个人相册
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteData(string Id);
        /// <summary>
        /// 批量添加相册
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<PhotoAlbums> entity);
    }
}
