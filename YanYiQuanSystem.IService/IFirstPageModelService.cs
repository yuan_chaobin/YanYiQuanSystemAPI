﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;

namespace YanYiQuanSystem.IService
{
    public interface IFirstPageModelService
    {
        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<FirstPageModels>> GetListAsync();
    }
}
