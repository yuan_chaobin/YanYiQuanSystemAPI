﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public interface ISensibilityManagerService
    {
        /// <summary>
        /// 获取敏感字数据
        /// </summary>
        /// <returns></returns>
        Task<List<SensibilityManager>> GetSensibilityManagerAsync();
        /// <summary>
        /// 添加敏感字
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        Task<bool> AddSensibilityManager(SensibilityManager sensibilityManager);
        /// <summary>
        /// 删除铭感字
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteSensibilityManager(string Id);
        /// <summary>
        /// 修改敏感字
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        Task<bool> UpdateSensibilityManager(SensibilityManager sensibilityManager);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<SensibilityManager>> GetSensibilityManagerWhereAsync(Expression<Func<SensibilityManager, bool>> whereExpression);
    }
}
