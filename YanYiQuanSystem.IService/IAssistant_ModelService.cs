﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;

namespace YanYiQuanSystem.IService
{
    public interface IAssistant_ModelService
    {
        /// 添加数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> AddData(Assistant_Model entity);


        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<Assistant_Model> entity);


        /// <summary>
        /// 根据Id删除单个数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteData(string Id);


        /// <summary>
        /// 批量删除Ids
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task<bool> BatchDelete(List<string> Ids);


        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateData(Assistant_Model entity);

        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<Assistant_Model>> GetListAsync();

        /// <summary>
        /// 根据条件获取数据
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<Assistant_Model>> GetListByWhereAsync(Expression<Func<Assistant_Model, bool>> whereExpression);

        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<Assistant_Model>> GetListByPage<TKey>(Expression<Func<Assistant_Model, bool>> whereExpression, Expression<Func<Assistant_Model, TKey>> orderExpression, int pageIndex, int pageSize);


        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<Assistant_Model>> GetEmpWhereAsync(Expression<Func<Assistant_Model, bool>> whereExpression);
    }
}
