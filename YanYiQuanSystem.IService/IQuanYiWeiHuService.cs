﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public  interface IQuanYiWeiHuService
    {
        Task<List<QuanYiWeiHu>> GetQuanYiWeiHus();
        Task<bool> AddQuanYiWeiHu(QuanYiWeiHu quanYiWeiHu);
        Task<bool> DeleteQuanYiWeiHu(string Id);
        Task<bool> UpdateQuanYiWeiHu(QuanYiWeiHu quanYiWeiHu);
        Task<List<QuanYiWeiHu>> GetQuanYiWeiHuBywere(Expression<Func<QuanYiWeiHu, bool>> whereExpression);
    }
}
