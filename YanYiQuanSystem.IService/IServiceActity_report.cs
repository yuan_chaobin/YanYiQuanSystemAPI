﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    /// <summary>
    /// 举报信息表服务层
    /// </summary>
    public interface IServiceActity_report
    { 
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> AddData(Activity_report entity);


        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<Activity_report> entity);


        /// <summary>
        /// 根据Id删除单个数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteData(string Id);


        /// <summary>
        /// 批量删除Ids
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task<bool> BatchDelete(List<string> Ids);


        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateData(Activity_report entity);


        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<Activity_report>> GetListAsync();

        /// <summary>
        /// 根据条件获取数据
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<Activity_report>> GetListByWhereAsync(Expression<Func<Activity_report, bool>> whereExpression);

        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<Activity_report>> GetListByPage<TKey>(Expression<Func<Activity_report, bool>> whereExpression, Expression<Func<Activity_report, TKey>> orderExpression, int pageIndex, int pageSize);
    }
}
