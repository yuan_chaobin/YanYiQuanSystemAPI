﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public interface INoteService
    {
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<NoteManager>> GetNoteWhereAsync(Expression<Func<NoteManager, bool>> whereExpression);
        /// <summary>
        /// 获取短信
        /// </summary>
        /// <returns></returns>
        Task<List<NoteManager>> GetNoteAsync();
    }
}
