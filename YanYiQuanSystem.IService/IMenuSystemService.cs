﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
     public  interface IMenuSystemService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        Task<bool> AddMenuSystemAsync(MenuSystem menuSystem);
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <returns></returns>
        Task<bool> BatchAddMenuSystem();
        /// <summary>
        /// 获取菜单数据
        /// </summary>
        /// <returns></returns>
        Task<List<MenuSystem>> GetMenuSystemAsync();
        /// <summary>
        /// 删除菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> DeleteMenuSystemAsync(string Id);
        /// <summary>
        /// 批量删除菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> BatchDeleteMenuSystemAsync();
        /// <summary>
        /// 修改菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> UpdateMenuSystemAsync(MenuSystem menuSystem);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<MenuSystem>> GetMenuSystemsWhereAsync(Expression<Func<MenuSystem, bool>> whereExpression);
    }
}
