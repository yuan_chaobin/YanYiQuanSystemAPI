﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using System.Linq.Expressions;

    public interface IUsersService
    {
        Task<List<User>> GetUsersAsync();

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> AddData(User entity);

        Task<bool> UpdateData(User user);

        Task<bool> UpdateCurrentRole(User user);
        Task<List<User>> GetUsreByWhere(Expression<Func<User, bool>> whereExpression);
    }
}
