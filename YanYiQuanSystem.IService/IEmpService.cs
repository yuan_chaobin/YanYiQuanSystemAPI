﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public interface IEmpService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        Task<bool> AddEmp(Emp entity);
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <returns></returns>
        Task<bool> BatchAddEmp(List<Emp> entity);
        /// <summary>
        /// 获取菜单数据
        /// </summary>
        /// <returns></returns>
        Task<List<Emp>> GetEmpAsync();
        /// <summary>
        /// 删除菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> DeleteEmp(string Id);
        /// <summary>
        /// 批量删除菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> BatchDeleteEmp(List<Emp> entity);
        /// <summary>
        /// 修改菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> UpdateEmp(Emp entity);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<Emp>> GetEmpWhereAsync(Expression<Func<Emp, bool>> whereExpression);

        /// <summary>
        /// 分页显示
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <param name=""></param>
        /// <returns></returns>
       Task<List<Emp>> GetListByPage(Expression<Func<Emp, bool>> whereExpression,Expression<Func<Emp>> orderExpression, int pageIndex, int pageSize);
    }
}
