﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;

namespace YanYiQuanSystem.IService
{
    public interface IPurchaseRecordService
    {
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> AddData(PurchaseRecord entity);


        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<PurchaseRecord> entity);


        /// <summary>
        /// 根据Id删除单个数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteData(string Id);


        /// <summary>
        /// 批量删除Ids
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task<bool> BatchDelete(List<string> Ids);


        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateData(PurchaseRecord entity);

        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<PurchaseRecord>> GetListAsync();

        /// <summary>
        /// 根据条件获取数据
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<PurchaseRecord>> GetListByWhereAsync(Expression<Func<PurchaseRecord, bool>> whereExpression);

    }
}
