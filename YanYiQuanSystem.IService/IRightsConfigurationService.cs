﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public interface IRightsConfigurationService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        Task<bool> AddRightsConfiguration(RightsConfiguration entity);
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        Task<List<RightsConfiguration>> GetRightsConfigurationAsync();
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <returns></returns>
        Task<bool> DeleteRightsConfiguration(string Id);

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <returns></returns>
        Task<bool> UpdateRightsConfiguration(RightsConfiguration entity);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<RightsConfiguration>> GetEmpWhereAsync(Expression<Func<RightsConfiguration, bool>> whereExpression);

      
    }
}

