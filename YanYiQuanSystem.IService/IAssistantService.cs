﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;

    public interface IAssistantService
    {
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> AddData(Assistant entity);


        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<Assistant> entity);


        /// <summary>
        /// 根据Id删除单个数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteData(string Id);


        /// <summary>
        /// 批量删除Ids
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task<bool> BatchDelete(List<string> Ids);


        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateData(Assistant entity);

        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<Assistant>> GetListAsync();

        Task<bool> UpdateAssistant(CommonData entity);

        Task<List<Assistant>> GetListByWhereAsync(Expression<Func<Assistant, bool>> whereExpression);
    }
}
