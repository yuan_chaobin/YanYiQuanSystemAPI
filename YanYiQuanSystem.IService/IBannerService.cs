﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    public interface IBannerService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        Task<bool> AddBanner(Banners entity);
        
        /// <summary>
        /// 获取菜单数据
        /// </summary>
        /// <returns></returns>
        Task<List<Banners>> GetBannerAsync();
        
       
        /// <summary>
        /// 修改菜单数据
        /// </summary>
        /// <returns></returns>
        Task<bool> UpdateBanner(Banners entity);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<Banners>> GetBannerWhereAsync(Expression<Func<Banners, bool>> whereExpression);

        /// <summary>
        /// 分页显示
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <param name=""></param>
        /// <returns></returns>
        Task<List<Banners>> GetListByPage(Expression<Func<Banners, bool>> whereExpression, Expression<Func<Banners>> orderExpression, int pageIndex, int pageSize);
    }
}
