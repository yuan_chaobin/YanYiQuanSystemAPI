﻿using System;
using System.Collections.Generic;
using System.Text;


namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;

    public interface IUserRightsService
    {
        Task<bool> AddUserRightsManager(UserRights userRights);

        Task<bool> DeleteUserRightsManager(string Id);

        Task<bool> UpdateUserRightsManager(UserRights userRights);

        Task<List<UserRights>> GetSensibilityManagerWhereAsync(Expression<Func<UserRights, bool>> whereExpression);
        Task<List<UserRights>> GetUserRights();
    }
}
