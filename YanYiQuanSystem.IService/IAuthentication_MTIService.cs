﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using System.Linq.Expressions;
    using YanYiQuanSystem.CommonUnitity;

    public interface IAuthentication_MTIService
    {
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> AddData(Authentication_MT entity);


        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<Authentication_MT> entity);


        /// <summary>
        /// 根据Id删除单个数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteData(string Id);


        /// <summary>
        /// 批量删除Ids
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task<bool> BatchDelete(List<string> Ids);


        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateData(Authentication_MT entity);


        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<Authentication_MT>> GetListAsync();

        /// <summary>
        /// 根据条件获取数据
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<Authentication_MT>> GetListByWhereAsync(Expression<Func<Authentication_MT, bool>> whereExpression);

        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<Authentication_MT>> GetListByPage<TKey>(Expression<Func<Authentication_MT, bool>> whereExpression, Expression<Func<Authentication_MT, TKey>> orderExpression, int pageIndex, int pageSize);
        /// <summary>
        /// 修改模特的基本信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> wxUpdateUserModel(CommonData entity);
        /// <summary>
        /// 修改用户头像信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> wxExitorImg(string id,string  img );
    }
}
