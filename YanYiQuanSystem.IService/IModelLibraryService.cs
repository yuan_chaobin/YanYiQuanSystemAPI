﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.IService
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;

    public interface IModelLibraryService
    {
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> AddData(ModelsLibrary entity);


        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> BatchAddData(List<ModelsLibrary> entity);


        /// <summary>
        /// 根据Id删除单个数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> DeleteData(string  Id);


        /// <summary>
        /// 批量删除Ids
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task<bool> BatchDelete(List<string> Ids);


        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> UpdateData(ModelsLibrary entity);

        Task<bool> UpdateFirstPage(List<string> Ids);

        Task<bool> UpdateUserModel(CommonData entity);

        Task<bool> UpdateUserRank(User entity);

        Task<bool> UpdateAduitStatus(string photoId);

        Task<bool> UpdateFirst(ModelsLibrary modelsLibrary);

        /// <summary>
        /// 获取集合数据
        /// </summary>
        /// <returns></returns>
        Task<List<ModelsLibrary>> GetListAsync();

        /// <summary>
        /// 根据条件获取数据
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        Task<List<ModelsLibrary>> GetListByWhereAsync(Expression<Func<ModelsLibrary, bool>> whereExpression);

        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<ModelsLibrary>> GetListByPage<TKey>(Expression<Func<ModelsLibrary, bool>> whereExpression, Expression<Func<ModelsLibrary, TKey>> orderExpression, int pageIndex, int pageSize);


        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        Task<List<ModelsLibrary>> GetEmpWhereAsync(Expression<Func<ModelsLibrary, bool>> whereExpression);
    }
}
