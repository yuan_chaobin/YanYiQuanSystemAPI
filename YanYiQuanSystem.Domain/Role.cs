﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 角色表
    /// </summary>
    [Table("Role")]
    public class Role
    {
        [Key]
        [StringLength(50)]
        public string RoleId { get; set; }//角色Id

        [StringLength(50)]
        public string RoleName { get; set; }//角色名称
        [StringLength(100)]
        public string Remarks { get; set; }//备注

    }
}
