﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 票选表
    /// </summary>
    [Table("Activity_vote")]
    public class Activity_vote
    {
        /// <summary>
        /// 选票编号
        /// </summary>
        [Key]
        [StringLength(36)]
        public string Uuid { get; set; }
        /// <summary>
        /// 活动编号
        /// </summary>
        [StringLength(36)]
        public string Id_activity { get; set; }
        /// <summary>
        /// 模特编号
        /// </summary>
        [StringLength(36)]
        public string Id_user { get; set; }
        /// <summary>
        /// 投票人
        /// </summary>
        [StringLength(36)]
        public string Id_user_dianzan { get; set; }
        /// <summary>
        /// 投票时间
        /// </summary>
        public DateTime Time_dianzan { get; set; }
    }
}
