﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    /// <summary>
    /// 敏感字管理表
    /// </summary>
    [Table("SensibilityManager")]
    public  class SensibilityManager
    {
        /// <summary>
        /// 敏感字id
        /// </summary>
        [Key]
        [StringLength(50)]
        public string SensibilityManagerID { get; set; }
        /// <summary>
        /// 敏感字名称
        /// </summary>
        [StringLength(50)]
        public string SensibilityManagerName { get; set; }
    }
}
