﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("NoteManager")]
    public class NoteManager
    {
        /// <summary>
        /// 短信id
        /// </summary>
        [Key]
        [StringLength(50)]
        public string NodeID { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [StringLength(50)]
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 验证码
        /// </summary>
        [StringLength(6)]
        public string VerificationCode { get; set; }
        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
