﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 举报信息表
    /// </summary>
    [Table("Activity_report")]
    public class Activity_report
    {
        /// <summary>
        /// 举报编号
        /// </summary>
        [Key]
        [StringLength(36)]
        public string Uuid { get; set; }
        /// <summary>
        /// 活动编号
        /// </summary>
        [StringLength(32)]
        public string Id_activity { get; set; }
        /// <summary>
        /// 被举报人()
        /// </summary>
        [StringLength(36)]
        public string Id_user { get; set; }
        /// <summary>
        /// 举报人
        /// </summary>
        [StringLength(36)]
        public string Id_user_report_by { get; set; }
        /// <summary>
        /// 举报内容
        /// </summary>
        [StringLength(200)]
        public string Report_content { get; set; }
        /// <summary>
        /// 举报时间
        /// </summary>
        public DateTime Time_trans { get; set; }
    }
}
