﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 助理列表
    /// </summary>
    [Table("Assistant")]
    public class Assistant
    {
        /// <summary>
        /// 助理人ID
        /// </summary>
        [Key]
        [StringLength(50)]
        public string AssistantId { get; set; }

        /// <summary>
        /// 用户外键ID
        /// </summary>
        [StringLength(50)]
        public string UsersID { get; set; }

        /// <summary>
        /// 邀请码
        /// </summary>
        [StringLength(50)]
        public string InvitationCode { get; set; }

        /// <summary>
        /// 我的邀请码
        /// </summary>
        [StringLength(50)]
        public string MyCode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime RegisterTime { get; set; }

        /// <summary>
        /// 是否允许登录 1:允许 2: 不允许
        /// </summary>
        public int IsLogging { get; set; }
    }
}
