﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    /// <summary>
    /// 关注信息表
    /// </summary>
    [Table("Attention")]
    public class Attention
    {
        [Key]
        [StringLength(50)]
        public string AttentionId { get; set; }

        [StringLength(50)]
        public string UserID { get; set; }//用户ID

        public int UserType { get; set; }//用户类型

        [StringLength(50)]
        public string FollowerID { get; set; }//关注人ID

        public int FollowerType { get; set; }//关注人类型

        public DateTime AttentionTime { get; set; }//关注时间
    }
}
