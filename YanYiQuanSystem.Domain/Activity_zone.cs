﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 赛区表
    /// </summary>
    [Table("Activity_zone")]
    public  class Activity_zone
    { 
        /// <summary>
        /// 赛区编号
        /// </summary>
        [Key]
        [StringLength(36)]
        public string Uuid { get; set; }
        /// <summary>
        /// 活动编号
        /// </summary>
        [StringLength(36)]
        public string Id_activity { get; set; }
        /// <summary>
        /// 赛区名
        /// </summary>
        [StringLength(32)]
        public string Zone_name { get; set; }
        /// <summary>
        /// 评论
        /// </summary>
        [StringLength(200)]
        public string Comments { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Order_num { get; set; }
    }
}
