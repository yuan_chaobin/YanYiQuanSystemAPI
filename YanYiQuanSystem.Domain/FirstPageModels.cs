﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    /// <summary>
    /// 首页模特表
    /// </summary>
    [Table("FirstPageModels")]
    public class FirstPageModels
    {

        [Key]
        [StringLength(50)]
        public string FirstModelId { get; set; }

        [StringLength(50)]
        public string ModelID { get; set; }

        public int SortNum { get; set; }


    }
}
