﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 员工表
    /// </summary>
    [Table("Emp")]
    public class Emp
    {
        [Key]
        [StringLength(50)]
        public string EmpId { get; set; }//员工Id

        [StringLength(50)]
        public string EmpName { get; set; }//员工姓名
        [StringLength(11)]
        public string Phone { get; set; }//手机号
        [StringLength(50)]
        public string UserName { get; set; }//登录名
        [StringLength(50)]
        public string RoleId { get; set; }//角色名
        [StringLength(50)]
        public string IdentityCard { get; set; }//身份证
        [StringLength(20)]
        public string UserPass { get; set; }//登录密码

        public int IsStates { get; set; }//状态
    }
}
