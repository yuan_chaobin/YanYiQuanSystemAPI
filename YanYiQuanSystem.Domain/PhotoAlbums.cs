﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    /// <summary>
    /// 相册库
    /// </summary>
    [Table("PhotoAlbums")]
    public class PhotoAlbums
    {
        [Key]
        [StringLength(50)]
        public string PhotoId { get; set; }

        /// <summary>
        /// 用户外键
        /// </summary>
        [StringLength(50)]
        public string UserID { get; set; }

        /// <summary>
        /// 相片
        /// </summary>
        [StringLength(500000)]
        public string Photos { get; set; }

        /// <summary>
        /// 审核状态  1  通过   2  拒绝
        /// </summary>
        public int AuditStatus { get; set; }
    }
}
