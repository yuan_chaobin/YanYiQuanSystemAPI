﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 用户权益表
    /// </summary>
    [Table("UserRights")]
    public class UserRights
    {
        /// <summary>
        /// 用户权益ID
        /// </summary>
        [StringLength(50)]
        public string UserRightsId { get; set; }

        /// <summary>
        /// 用户外键ID
        /// </summary>
        [StringLength(50)]
        public string UsersID { get; set; }

        /// <summary>
        /// 权益适用角色  1 个人企业  2  企业
        /// </summary>
        public int RightsRole { get; set; }

        /// <summary>
        /// 权益名称
        /// </summary>
        [StringLength(50)]
        public string RightsName { get; set; }

        /// <summary>
        /// 权益类型  1  购买会员权益  2  认证权益
        /// </summary>
        public int RightsType { get; set; }

        /// <summary>
        /// 权益类型方式 1  加次数
        /// </summary>
        public int RightsTypeWay { get; set; }

        /// <summary>
        /// 次数
        /// </summary>
        public int Times { get; set; }

        /// <summary>
        /// 权益开始时间
        /// </summary>
        public DateTime StratTime { get; set; }

        /// <summary>
        /// 权益结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 权益说明
        /// </summary>
        [StringLength(100)]
        public string Remarks { get; set; }
    }
}
