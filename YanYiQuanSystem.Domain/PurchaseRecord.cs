﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 用户权益购买记录表
    /// </summary>
    [Table("PurchaseRecord")]
    public class PurchaseRecord
    {
        [Key]
        [StringLength(50)]
        public string PurchaseRecordId { get; set; }

        /// <summary>
        /// 用户权益外键ID
        /// </summary>
        [StringLength(50)]
        public string UserRightsID { get; set; }

        /// <summary>
        /// 购买时角色 1: 模特 2:企业  3: 个人企业  4: 助理
        /// </summary>
        public int BuyRole { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        [StringLength(50)]
        public string ProductName { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public Decimal MoneySum { get; set; }

        /// <summary>
        /// 请求支付时间
        /// </summary>
        public DateTime DemandPayTime { get; set; }

        /// <summary>
        /// 成功支付时间
        /// </summary>
        public DateTime SuccessPayTime { get; set; }

        
    }
}
