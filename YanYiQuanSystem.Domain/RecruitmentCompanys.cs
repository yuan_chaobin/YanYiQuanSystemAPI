﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 招聘公司表
    /// </summary>
    [Table("RecruitmentCompanys")]
    public class RecruitmentCompanys
    {
        /// <summary>
        /// 招聘公司人员ID
        /// </summary>
        [Key]
        [StringLength(50)]
        public string HrId { get; set; }

        /// <summary>
        /// 用户外键ID
        /// </summary>
        [StringLength(50)]
        public string UsersID { get; set; }

        /// <summary>
        /// 邀请码
        /// </summary>
        [StringLength(100)]
        public string? InvitationCode { get; set; }

        /// <summary>
        /// 我的邀请码
        /// </summary>
        [StringLength(50)]
        public string? MyCode { get; set; }

        /// <summary>
        /// 企业类型 1:个人  2:企业
        /// </summary>
        public int CompanyType { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        [StringLength(50)]
        public string CompanyName { get; set; }

        /// <summary>
        /// 公司电话
        /// </summary>
        [StringLength(50)]
        public string CompanyPhone { get; set; }

        /// <summary>
        /// 公司法人
        /// </summary>
        [StringLength(50)]
        public string CompanyPerson { get; set; }

        /// <summary>
        /// 公司地址
        /// </summary>
        [StringLength(100)]
        public string CompanyAddress { get; set; }

        /// <summary>
        /// 纳税人识别号
        /// </summary>
        [StringLength(50)]
        public string TaxpayerNum { get; set; }

        /// <summary>
        /// 工作岗位
        /// </summary>
        [StringLength(20)]
        public string WorkPosition { get; set; }

        /// <summary>
        /// 认证状态
        /// </summary>
        public int ApproveState { get; set; }

        /// <summary>
        /// 登录状态
        /// </summary>
        public int LoggingStatus { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegisterTime { get; set; }

    }
}
