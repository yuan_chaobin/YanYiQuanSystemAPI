﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    /// <summary>
    /// 认证模特/助理
    /// </summary>
    [Table("Authentication_MT")]
    public class Authentication_MT
    {
        [Key]
        [StringLength(36)]
        public string AuMTid { get; set; }
        [StringLength(500000)]

        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPortrait { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 姓名
        /// </summary>
        public string ModelName { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 手机号
        /// </summary>
        public string PnoneNumber { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 身份证
        /// </summary>
        public string IdentityNumber { get; set; }
        /// <summary>
        /// 身份类型
        /// </summary>
        public int? IsIdentity { get; set; }//1.模特 2.助理
        /// <summary>
        /// 认证状态
        /// </summary>
        public int? Austate { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int? Sex { get; set; }
        [StringLength(500000)]

        /// <summary>
        /// 证件照片
        /// </summary>
        public string zjZhaopian { get; set; }
        [StringLength(500000)]

        /// <summary>
        /// 营业执照
        /// </summary>
        public string Yingyezhizhao { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 审核人
        /// </summary>
        public string AuditName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? AuditTime { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 线上线下
        /// </summary>
        public string Standby1 { get; set; }
        [StringLength(200)]

        /// <summary>
        /// 地址
        /// </summary>
        public string Standby2 { get; set; }
        /// <summary>
        /// 出生年月日
        /// </summary>
        public DateTime? Birthday { get; set; }
        /// <summary>
        /// 当前居住地
        /// </summary>
        [StringLength(50)]
        public string PresentAddress { get; set; }
    }
}