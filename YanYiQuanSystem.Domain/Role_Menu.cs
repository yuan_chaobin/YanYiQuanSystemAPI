﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    /// <summary>
    /// 角色菜单映射表
    /// </summary>
    [Table("Role_Menu")]
    public class Role_Menu
    {
        [Key]
        [StringLength(50)]
        public string EMId { get; set; }//映射主健Id

        
        [StringLength(50)]
        public string RoleId { get; set; }//角色Id

        

        [StringLength(50)]
        public string MenuID { get; set; }// 菜单id


        [NotMapped]
        public string PID { get; set; }

        [NotMapped]
        public string Pname { get; set; }
    }
}
