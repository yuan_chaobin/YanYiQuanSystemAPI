﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("SystemInfrom")]
   public class SystemInfrom
    {
        /// <summary>
        /// 系统通知id
        /// </summary>
        /// 
        [Key]
        [StringLength(50)]
        public string InfromID { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [StringLength(400)]
        public string Content { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>

        [StringLength(50)]
        public string UserId { get; set; }
        /// <summary>
        /// 是否有效
        /// </summary>
        public int IsValid { get; set; }
        /// <summary>
        /// 模特id
        /// </summary>
        [StringLength(50)]
        public string ModelId { get; set; }
    }
}
