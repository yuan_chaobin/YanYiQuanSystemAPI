﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    /// <summary>
    /// 招聘库列表
    /// </summary>
    [Table("RecruitmentLibrary")]
    public class RecruitmentLibrary
    {
        [Key]
        [StringLength(50)]
        public string RecLibraryId { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        [StringLength(50)]
        public string CompanyName { get; set; }

        /// <summary>
        /// 职位名称
        /// </summary>
        [StringLength(50)]
        public string PostName { get; set; }

        /// <summary>
        /// 审核状态  1:审核通过  2:未审核：3 拒绝
        /// </summary>
        public int? AuditStatus { get; set; }

        /// <summary>
        /// 是否有效 1:公开  2:私密
        /// </summary>
        public int? IsValid { get; set; }

        /// <summary>
        /// 薪资类型 1: 日新  2:月薪  3:年薪
        /// </summary>
        public int? SalaryType { get; set; }

        /// <summary>
        /// 薪资范围
        /// </summary>
        [StringLength(50)]
        public string SalaryScope { get; set; }

        /// <summary>
        /// 最低薪资
        /// </summary>
        public int? MinSalary { get; set; }

        /// <summary>
        /// 最高薪资
        /// </summary>
        public int? MaxSalary { get; set; }

        /// <summary>
        /// 工作类型 1:会展活动  2:平面拍摄 3:影视表演
        /// </summary>
        public int? WorkType { get; set; }

        /// <summary>
        /// 职位性别  1：男  2 女
        /// </summary>
        public int? PostSex { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        [StringLength(50)]
        public string Height { get; set; }

        /// <summary>
        /// 学历 1:不限学历  2 :大专  3:本科  
        /// </summary>
        public int? Education { get; set; }

        /// <summary>
        /// 工作年限
        /// </summary>
        [StringLength(50)]
        public string WorkYears { get; set; }

        /// <summary>
        /// 工作地点
        /// </summary>
        [StringLength(200)]
        public string WorkPlace { get; set; }

        /// <summary>
        /// 招聘人数
        /// </summary>
        public int? RecPeoples { get; set; }

        /// <summary>
        /// 职位标签 1: 可私拍  2:主持  3:公演
        /// </summary>
        public int? PostTag { get; set; }

        /// <summary>
        /// 职位描述
        /// </summary>
        [StringLength(12000)]
        public string PostRemarks { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }
    }
}
