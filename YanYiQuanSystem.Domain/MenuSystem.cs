﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("MenuSystem")]
    public class MenuSystem
    {
        
        /// <summary>
        /// 菜单id
        /// </summary>
        /// \
        /// 
        [Key]
        [StringLength(50)]
        public string MenuID { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        /// 
        [StringLength(50)]
        public string MenuName { get; set; }
        /// <summary>
        /// 菜单URL
        /// </summary>
        /// 
        [StringLength(50)]
        public string MenuUrl { get; set; }
        /// <summary>
        /// 菜单父ID
        /// </summary>
        ///      

        [StringLength(51)]
        public string MenuPid { get; set; }
        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        /// s  
        public int Isdelete { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        /// 
       
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
         public string MenuIcon { get; set; }
        public string MenuIcon1 { get; set; }
        public string MenuSystemMenuID { get; set; }
        /// <summary>
        /// 菜单子集
        /// </summary>
        //public List<MenuSystem> children { get; set; }

    }
}
