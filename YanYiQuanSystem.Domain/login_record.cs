﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    /// <summary>
    /// 登录的基本表 
    /// </summary>
    /// 
    [Table("login_record")]
    public class login_record
    {
        [Key]
        [StringLength(50)]
        /// <summary>
        /// 登录基本表id
        /// </summary>
        public string userbasicid { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 关联用户表的id
        /// </summary>
        public string uuid { get; set; }
        
        /// <summary>
        /// 用户类型
        /// </summary>
        public int? usertype { get; set; }

        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime? logintime { get; set; }
        /// <summary>
        /// 0-微信公众号 1-安卓  2-ios
        /// </summary>
        public int? logintype { get; set; }
        /// <summary>
        ///0-微信一键 1-账号密码 2-短信验证码
        /// </summary>
        public int? loginmethod { get; set; }
    }
}
