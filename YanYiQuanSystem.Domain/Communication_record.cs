﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YanYiQuanSystem.Domain
{

    /// <summary>
    /// 职位邀请次数查看
    /// </summary>
    /// 
    [Table("Communication_record")]
    public class Communication_record
    {
        [Key]
        [StringLength(50)]
        /// <summary>
        /// 招聘方id
        /// </summary>
        public string ZPId { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 邀请次数id
        /// </summary>
        public string CommunicationId { get; set; }
        /// <summary>
        /// 发起时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 1.拨通求职电话 2.邀约评级次数  3. 职位邀请次数
        /// </summary>
        public int? Style { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 接收方名称
        /// </summary>
        public string JSName { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 接收方账号
        /// </summary>
        public string JSPhone { get; set; }
    }
}
