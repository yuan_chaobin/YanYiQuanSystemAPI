﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    public class Activity
    {
        [Key]
        [StringLength(36)]
        /// <summary>
        /// 活动编号
        /// </summary>
        public string Uuid { get; set; }
        /// <summary>
        /// 活动主题
        /// </summary>
        [StringLength(32)]
        public string Desc_activity { get; set; }
        /// <summary>
        /// 报名开始时间
        /// </summary>
        public DateTime Time_from_baoming { get; set; }
        /// <summary>
        /// 报名截止时间
        /// </summary>
        public DateTime Time_to_baoming { get; set; }
        /// <summary>
        /// 选拔开始时间
        /// </summary>
        public DateTime Time_from_xuanba { get; set; }
        /// <summary>
        /// 选拔结束时间
        /// </summary>
        public DateTime Time_to_xuanba { get; set; }
        /// <summary>
        /// 报名人数上限
        /// </summary>
        public int Count_max { get; set; }
        /// <summary>
        /// 活动类型
        /// </summary>
        public string Category_activity { get; set; }
        /// <summary>
        /// 评论
        /// </summary>
        [StringLength(200)]
        public string Comments { get; set; }
        /// <summary>
        /// 海报图片
        /// </summary>
        [StringLength(500000)]
        public string Url_pic_activity { get; set; }
        /// <summary>
        /// 活动规则
        /// </summary>
        [StringLength(280)]
        public string Rule_activity { get; set; }
    }
}
