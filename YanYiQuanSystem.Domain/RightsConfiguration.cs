﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    /// <summary>
    /// 权益配置表
    /// </summary>
    [Table("RightsConfiguration")]
    public  class RightsConfiguration
    {
        //权益Id
        [Key]
        [StringLength(50)]
        public string RightsId { get; set; }
        //权益名称
        [StringLength(50)]
        public string RightsName { get; set; }
        //排序
        public int? Sort { get; set; }
        //权益类型
        public int? RightsClass { get; set; }
        //权益使用角色
        public int? RightsRole { get; set; }
        //权益方式
        public int? RightWay { get; set; }
        [StringLength(50)]
        //说明
        public string Explain { get; set; }
        //是否启用
        public int? Start { get; set; }
    }
}
