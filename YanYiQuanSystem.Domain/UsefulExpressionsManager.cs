﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    /// <summary>
    /// 常用语管理表
    /// </summary>
    /// 
    [Table("UsefulExpressionsManager")]
    public  class UsefulExpressionsManager
    {
        /// <summary>
        /// 常用语管理ID 
        /// </summary>
        [Key]
        [StringLength(50)]

        public string UsefulExpressionsManagerID { get; set; }
        /// <summary>
        /// 常用语管理名称
        /// </summary>
        [StringLength(50)]
        public string UsefulExpressionsManagerName { get; set; }
        /// <summary>
        /// 常用语排序id
        /// </summary>
        public int UsefulOrderId { get; set; }
    }
}
