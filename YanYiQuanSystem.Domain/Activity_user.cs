﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 活动用户表
    /// </summary>
    [Table("Activity_user")]
    public class Activity_user
    {
        /// <summary>
        /// 模特编号
        /// </summary>
        [Key]
        [StringLength(36)]
        public string Uuid { get; set; }
        /// <summary>
        /// 活动编号
        /// </summary>
        [StringLength(36)]
        public string Id_activity { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        [StringLength(36)]
        public string Id_user { get; set; }
        /// <summary>
        /// 赛区名
        /// </summary>
        [StringLength(36)]
        public string Activity_zone_name { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [StringLength(36)]
        public string Name_user { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [StringLength(36)]
        public string Phone_user { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        [StringLength(32)]
        public string Nickname { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        [StringLength(32)]
        public string Idcard_user { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        [StringLength(500000)]
        public string Url_pic_head { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int Gender { get; set; }
        /// <summary>
        /// 血型
        /// </summary>
        [StringLength(32)]
        public string Blood_type { get; set; }
        /// <summary>
        /// 身高
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// 体重
        /// </summary>
        public decimal Weight { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Date_birthday { get; set; }
        /// <summary>
        /// 星座
        /// </summary>
        [StringLength(32)]
        public string Xingzuo { get; set; }
        /// <summary>
        /// 所在地区
        /// </summary>
        [StringLength(32)]
        public string Region { get; set; }
        /// <summary>
        /// 鞋码
        /// </summary>
        public int Xiema { get; set; }
        /// <summary>
        /// 腰围
        /// </summary>
        public decimal Yaowei { get; set; }
        /// <summary>
        /// 胸围
        /// </summary>
        public decimal Xiongwei { get; set; }
        /// <summary>
        /// 臀围
        /// </summary>
        public decimal Tunwei { get; set; }
        /// <summary>
        /// 微博
        /// </summary>
        [StringLength(32)]
        public string Weibo { get; set; }
        /// <summary>
        /// QQ
        /// </summary>
        [StringLength(32)]
        public string Qq { get; set; }
        /// <summary>
        /// 评论
        /// </summary>
        [StringLength(200)]
        public string Comments { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        public int Shenhe_status { get; set; }
        /// <summary>
        /// 是否是入选
        /// </summary>
        public int Is_mainland { get; set; }
        /// <summary>
        /// 是否推荐到首页
        /// </summary>
        public int Is_allow_index { get; set; }
        /// <summary>
        /// 报名时间
        /// </summary>
        public DateTime Create_time { get; set; }
        /// <summary>
        /// 家访地址
        /// </summary>
        [StringLength(32)]
        public string Url_homecover { get; set; }
        /// <summary>
        /// 增加点赞票数
        /// </summary>
        public int Piaoshu { get; set; } = 0;
    }
}
