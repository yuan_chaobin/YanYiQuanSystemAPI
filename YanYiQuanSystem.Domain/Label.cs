﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    //标签
    [Table("Label")]
    public class Label
    {
        /// <summary>
        /// 标签id
        /// </summary>
        /// 
        [Key]
        [StringLength(50)]
        public string LabelTypeID { get; set; }
        [StringLength(50)]
        public string LabelName { get; set; }
        [StringLength(50)]
        public string LabelPid { get; set; }
       
    }
}
