﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 模特列表
    /// </summary>
    [Table("ModelsLibrary")]
    public class ModelsLibrary
    {
        /// <summary>
        /// 模特ID
        /// </summary>
        [Key]
        [StringLength(50)]
        public string ModelId { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [StringLength(50)]
        public string NickName { get; set; }

        /// <summary>
        /// 用户外键ID
        /// </summary>
        [StringLength(50)]
        public string UserID { get; set; }

        /// <summary>
        /// 是否有助理 1:是  2: 否
        /// </summary>
        public int? IsAssistant { get; set; }

        /// <summary>
        /// 是否推荐到首页  1:是 2:否
        /// </summary>
        public int? IsFirstPage { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegisterTime { get; set; }

        /// <summary>
        /// 邀请码
        /// </summary>
        [StringLength(50)]
        public string? InvitationCode { get; set; }

        /// <summary>
        /// 我的邀请码
        /// </summary>
        [StringLength(50)]
        public string MyCode { get; set; }

        /// <summary>
        /// 星座
        /// </summary>
        [StringLength(20)]
        public string Constellation { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        public int? Height { get; set; }

        /// <summary>
        /// 体重
        /// </summary>
        public int? Weight { get; set; }

        /// <summary>
        /// 鞋码
        /// </summary>
        public int? Size { get; set; }

        /// <summary>
        /// 胸围
        /// </summary>
        public int? TheChest { get; set; }

        /// <summary>
        /// 腰围
        /// </summary>
        public int? Waistline { get; set; }

        /// <summary>
        /// 臀围
        /// </summary>
        public int? Hipline { get; set; }

        /// <summary>
        /// 血型
        /// </summary>
        [StringLength(20)]
        public string BloodType { get; set; }

        /// <summary>
        /// 是否参加活动  1: 是  2:否
        /// </summary>
        public int? IsActivity { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 活动表外键ID
        /// </summary>
        public string ActivityId { get; set; }

        /// <summary>
        /// 登录账户
        /// </summary>
        [StringLength(50)]
        public string LoginAccount { get; set; }

        /// <summary>
        /// 是否允许登陆  1:允许  2:不允许
        /// </summary>
        public int? IsLogging1 { get; set; }

        /// <summary>
        /// 认证状态    1:未认证   2:已认证
        /// </summary>
        public int? ApproveStatus { get; set; }

        /// <summary>
        /// 相册状态   1:有待审核的照片  2:已审核
        /// </summary>
        public int? PhotoStatus { get; set; }

        /// <summary>
        /// 登录状态  1:未登录  2:已登录
        /// </summary>
        public int? LoggingStatus { get; set; }
        /// <summary>
        /// 被关注次数
        /// </summary>
        public int BeAttentionClick { get; set; }
        /// <summary>
        /// 被联系次数
        /// </summary>
        public int BeRelationClick { get; set; }
        /// <summary>
        /// 拨打招聘方电话次数
        /// </summary>
        public int DialInviteClick { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// 荣誉说明
        /// </summary>
        public string HonorThat { get; set; }
    }
}
