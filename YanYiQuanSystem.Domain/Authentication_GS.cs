﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("Authentication_GS")]
    /// <summary>
    /// 认证公司
    /// </summary>
    public  class Authentication_GS
    {
        [Key]
        [StringLength(36)]
        /// <summary>
        /// 认证公司id
        /// </summary>
        public string AuGSid { get; set; }
        [StringLength(500000)]
        /// <summary>
        /// 头像
        /// </summary>
        public string GSHeadPortraits { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 手机号
        /// </summary>
        public string PnoneNumber { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 身份证
        /// </summary>
        public string IdentityNumber { get; set; }
  

        /// <summary>
        /// 身份类型
        /// </summary>
        public int? IsIdentity { get; set; }

        /// <summary>
        /// 认证状态  1 待认证  2 认证成功  3 认证失败
        /// </summary>
        public int? Austate { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int? Sex { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 公司名称
        /// </summary>
        public string GSName { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 公司法人
        /// </summary>
        public string Legalperson { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 统一社会信用代码
        /// </summary>
        public string  Credit { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 公司邮箱
        /// </summary>
        public string  GSemail { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 公司地址
        /// </summary>
        public string  GSdizhi { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 审核人
        /// </summary>
        public string AuditName { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime? AuditTime { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 纳税人识别号
        /// </summary>
        public string Taxpayer { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 备用字段1
        /// </summary>
        public string Standby1 { get; set; }
        [StringLength(50)]

        /// <summary>
        /// 备用字段2
        /// </summary>
        public string  Standby2 { get; set; }
        [StringLength(500000)]
        /// <summary>
        /// 证件照片
        /// </summary>
        public string Idphoto { get; set; }
        [StringLength(50000)]
        /// <summary>
        /// 营业执照
        /// </summary>
        public string businesslicense { get; set; }
    }
}
