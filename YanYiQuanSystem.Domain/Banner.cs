﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    /// <summary>
    /// Banner维护表
    /// </summary>
    [Table("Banner")]
    public class Banner
    {
        //ID
        [Key]
        [StringLength(50)]
        public string BannerId { get; set; }
        [StringLength(100)]
        //广告描述
        public string AdvertisingPlanning{get;set;}
        //启用时间
        public DateTime ActiveTime { get; set; }
        //排序
        public int? Sort { get; set; }
        //是否显示分享
        public int? ToShare { get; set; }
        //分享文Title 
        [StringLength(50)]
        public string Title { get; set; }
        //移动端图片
        [StringLength(50)]
        public string Images { get; set; }
        //活动ID
        [StringLength(50)]
        public string Uuid { get; set; }
        //到期时间
        public DateTime ExpirationTime { get; set; }
        //分享URL
        [StringLength(100)]
        public string ShareURL { get; set; }
        //产品分享文字内容
        [StringLength(100)]
        public string Content{ get; set; }
        //	移动端内容URL
        [StringLength(100)]
        public string MobileURL { get; set; }

    }
}
