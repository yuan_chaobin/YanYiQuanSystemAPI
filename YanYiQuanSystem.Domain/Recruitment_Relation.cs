﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    /// <summary>
    /// 模特于招聘方的联系
    /// </summary>
    /// 
    [Table("Recruitment_Relation")]
    public  class Recruitment_Relation
    {
        [Key]
        [StringLength(50)]
        /// <summary>
        // 联系Id
        /// </summary>
        public string RecruitmentId { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 模特id
        /// </summary>
        public string MT_Id { get; set; }

        public  DateTime? CreateTime { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 公司名称
        /// </summary>
        public string GS_Name { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string Name { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 类型 1.联系招聘方  2. 被联系
        /// </summary>
        public int? RecruitmentType { get; set; }




    }
}
