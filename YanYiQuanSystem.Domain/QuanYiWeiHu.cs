﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    /// <summary>
    /// 系统权益产品维护
    /// </summary>
    /// 
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("QuanYiWeiHu")]
    public class QuanYiWeiHu
    {
        /// <summary>
        /// 产品id
        /// </summary>
        [Key]
        [StringLength(50)]
        public string ProductId { get; set; }

        public string CanPinID { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        [StringLength(50)]
        public string ProductName { get; set; }
        /// <summary>
        /// 实际金额
        /// </summary>
        /// 
        [StringLength(50)]
        public string ActualAmount { get; set; }
        /// <summary>
        /// 原始金额
        /// </summary>
        [StringLength(50)]
        public string Originalamount { get; set; }
        /// <summary>
        /// 天数
        /// </summary>

        public int? DayNumber { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public int? IsState { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        /// 
        [StringLength(100)]
        public string Remark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int OrderById { get; set; }
        /// <summary>
        /// 用户表权益表关联主键 用来查询手机号
        /// </summary>
        /// 
        [StringLength(50)]
        public string UserRightsId { get; set; }
        /// <summary>
        /// 赠送权益天数
        /// </summary>
        public int GiveAsDay { get; set; }
    }
}
