﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YanYiQuanSystem.Domain
{

    /// <summary>
    /// 关注基本表
    /// </summary>
    /// 
    [Table("attention_record")]
    public class attention_record
    {
        [Key]
        [StringLength(50)]
        /// <summary>
        /// 关注表id
        /// </summary>
        public string AttId { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 模特id
        /// </summary>
        public string MT_Id { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 关注人姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int? Sex { get; set; }
        [StringLength(50)]
        /// <summary>
        /// 关注人的账户
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 关注时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
    }
}
