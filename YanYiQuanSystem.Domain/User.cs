﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 用户表
    /// </summary>
    [Table("User")]
    public class User
    {
        [Key]
        [StringLength(50)]
        public string UserId { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        public int? Sex { get; set; }

        /// <summary>
        /// 当前角色  1: 模特 2:企业  3: 个人企业  4: 助理
        /// </summary>
        public int? CurrentRole { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [StringLength(500000)]
        public string HeadPortrait { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        [StringLength(50)]
        public string IdentityNumber { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [StringLength(50)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 当前居住地
        /// </summary>
        [StringLength(50)]
        public string PresentAddress { get; set; }

        /// <summary>
        /// 用户等级
        /// </summary>
        [StringLength(50)]
        public string UsersRank { get; set; }

        /// <summary>
        /// 是否禁言
        /// </summary>
        public int IsBanned { get; set; }
        /// <summary>
        /// 禁言时间
        /// </summary>
        public DateTime? BannedTime { get; set; }
        /// <summary>
        /// 登录次数
        /// </summary>
        public int LoginClick { get; set; }
        /// <summary>
        /// 沟通次数
        /// </summary>
        public int Communication { get; set; } 
        /// <summary>
        /// 拨打求职电话次数
        /// </summary>
        public int RingUpClick { get; set; }
        /// <summary>
        /// 邀约评级次数
        /// </summary>
        public int InviteClick { get; set; }
        /// <summary>
        /// 职位邀请次数
        /// </summary>
        public int PositionInviteClick { get; set; }
    }
}
