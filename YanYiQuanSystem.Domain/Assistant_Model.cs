﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// 助理模特表
    /// </summary>
    [Table("Assistant_Model")]
    public class Assistant_Model
    {
        /// <summary>
        /// 助理模特ID
        /// </summary>
        [Key]
        [StringLength(50)]
        public string Assistant_ModelId { get; set; }

        /// <summary>
        /// 助理人ID
        /// </summary>
        [StringLength(50)]
        public string AssistantID { get; set; }

        /// <summary>
        /// 模特ID
        /// </summary>
        [StringLength(50)]
        public string ModelID { get; set; }
    }
}
