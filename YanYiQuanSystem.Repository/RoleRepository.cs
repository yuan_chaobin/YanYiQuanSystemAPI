﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using YanYiQuanSystem.CommonUnitity;

    public class RoleRepository : IRoleRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public RoleRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }
        public async Task<bool> AddData(Role entity)
        {
            await YanYiQuanSystemDbContext.roles.AddAsync(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> BatchAddData(List<Role> entity)
        {
            foreach (var item in entity)
            {
                YanYiQuanSystemDbContext.roles.Add(item);
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> BatchDelete(List<string> Ids)
        {
            foreach (var item in Ids)
            {
                YanYiQuanSystemDbContext.roles.Remove(YanYiQuanSystemDbContext.roles.Find(item));
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;    
        }

        public async Task<bool> DeleteData(string Id)
        {
            YanYiQuanSystemDbContext.roles.Remove(YanYiQuanSystemDbContext.roles.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        //获取角色标的全部数据
        public async Task<List<Role>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.roles.ToListAsync();
        }

        public async Task<List<Role>> GetListByPage<TKey>(Expression<Func<Role, bool>> whereExpression, Expression<Func<Role, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return await YanYiQuanSystemDbContext.Set<Role>().Where(whereExpression).OrderBy(orderExpression).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        public async Task<List<Role>> GetListByWhereAsync(Expression<Func<Role, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.roles.Where(whereExpression).ToListAsync();
        }

        public async Task<bool> UpdateData(Role entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }
    }
}
