﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using YanYiQuanSystem.CommonUnitity;
    public class Role_MenuRepository:IRole_MenuRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public Role_MenuRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public async Task<bool> AddData(Role_Menu entity)
        {
            await YanYiQuanSystemDbContext.role_Menus.AddAsync(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> BatchAddData(List<Role_Menu> entity)
        {
            foreach (var item in entity)
            {
                YanYiQuanSystemDbContext.role_Menus.Add(item);
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> DeleteData(string Id)
        {
            YanYiQuanSystemDbContext.role_Menus.Remove(YanYiQuanSystemDbContext.role_Menus.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(Role_Menu entity)
        {
            throw new NotImplementedException();
        }

        public Task<List<Role_Menu>> GetListAsync()
        {
            return YanYiQuanSystemDbContext.role_Menus.ToListAsync();
        }

        public async Task<List<Role_Menu>> GetListByWhereAsync(Expression<Func<Role_Menu, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.role_Menus.Where(whereExpression).ToListAsync();
        }

        public Task<List<Role_Menu>> GetListByPage<TKey>(Expression<Func<Role_Menu, bool>> whereExpression, Expression<Func<Role_Menu, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
