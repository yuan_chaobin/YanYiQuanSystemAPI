﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using YanYiQuanSystem.CommonUnitity;

    public  class Authentication_GSRepository: IGS_Repository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public Authentication_GSRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public async Task<bool> AddData(Authentication_GS entity)
        {
             YanYiQuanSystemDbContext.DBAT_GSs.Add(entity);

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<Authentication_GS> entity)
        {
            throw new NotImplementedException();
        }

       
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateData(Authentication_GS entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<List<Authentication_GS>> GetListAsync()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<Authentication_GS>> GetListByWhereAsync(Expression<Func<Authentication_GS, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.DBAT_GSs.Where(whereExpression).ToListAsync();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Authentication_GS>> GetListByPage<TKey>(Expression<Func<Authentication_GS, bool>> whereExpression, Expression<Func<Authentication_GS, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return await YanYiQuanSystemDbContext.DBAT_GSs.Where(whereExpression).OrderBy(orderExpression).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        public async Task<bool> DeleteData(string Id)
        {
            var del = YanYiQuanSystemDbContext.DBAT_GSs.Find(Id);
            YanYiQuanSystemDbContext.DBAT_GSs.Remove(del);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

     
        /// <summary>
        /// 修改公司的基本信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUserModel(CommonData entity)
        {
            RecruitmentCompanys companys = YanYiQuanSystemDbContext.recruitmentCompanys.Find(entity.HrId);
            companys.CompanyName = entity.CompanyName;
            companys.CompanyAddress = entity.CompanyAddress;
            companys.CompanyPerson = entity.CompanyPerson;
            companys.CompanyPhone = entity.CompanyPhone;
            companys.WorkPosition = entity.WorkPosition;
            User user = YanYiQuanSystemDbContext.users.Find(entity.UserId);
            user.UserName = entity.UserName;
            user.Sex = entity.Sex;
            user.IdentityNumber = entity.IdentityNumber;

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUserModelZL(CommonData entity)
        {
            Assistant companys = YanYiQuanSystemDbContext.assistants.Find(entity.AssistantId);
            companys.InvitationCode = entity.InvitationCode;
            User user = YanYiQuanSystemDbContext.users.Find(entity.UserId);
            user.UserName = entity.UserName;
            user.Sex = entity.Sex;
            user.IdentityNumber = entity.IdentityNumber;
            user.PhoneNumber = entity.PhoneNumber;
            user.PresentAddress = entity.PresentAddress;

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }
    }
}
