﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;

    public class RecruitmentLibraryRepository : IRepository<RecruitmentLibrary>
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public RecruitmentLibraryRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }
        /// <summary>
        /// 添加招聘计划
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddData(RecruitmentLibrary entity)
        {
            await YanYiQuanSystemDbContext.recruitments.AddAsync(entity);

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<RecruitmentLibrary> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<RecruitmentLibrary>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.recruitments.ToListAsync();
        }

        public async Task<List<RecruitmentLibrary>> GetListByPage<TKey>(Expression<Func<RecruitmentLibrary, bool>> whereExpression, Expression<Func<RecruitmentLibrary, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            List<RecruitmentLibrary> sss = await YanYiQuanSystemDbContext.recruitments.Where(whereExpression).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            return sss;
        }

        public async Task<List<RecruitmentLibrary>> GetListByWhereAsync(Expression<Func<RecruitmentLibrary, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.recruitments.Where(whereExpression).ToListAsync();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async  Task<bool> UpdateData(RecruitmentLibrary entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
    }
}
