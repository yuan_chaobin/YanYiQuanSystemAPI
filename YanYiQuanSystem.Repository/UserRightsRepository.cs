﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;
    public  class UserRightsRepository : IUserRightsRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public UserRightsRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }
        public async Task<bool> AddData(UserRights entity)
        {
            YanYiQuanSystemDbContext.userRights.Add(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<UserRights> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(UserRights entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<UserRights>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.userRights.ToListAsync();
        }

        public async Task<List<UserRights>> GetListByWhereAsync(Expression<Func<UserRights, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.userRights.Where(whereExpression).ToListAsync();
        }

        public Task<List<UserRights>> GetListByPage<TKey>(Expression<Func<UserRights, bool>> whereExpression, Expression<Func<UserRights, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }
    }
}
