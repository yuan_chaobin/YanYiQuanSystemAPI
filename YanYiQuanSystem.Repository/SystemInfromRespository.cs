﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using YanYiQuanSystem.CommonUnitity;
    public  class SystemInfromRespository:ISystemInfromRespository
    {
        public YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public SystemInfromRespository(YanYiQuanSystemDbContext yanYiQuanSystemDbContext)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDbContext;
        }

        public async Task<bool> AddData(SystemInfrom entity)
        {
            YanYiQuanSystemDbContext.systemInfroms.Add(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<SystemInfrom> entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
           YanYiQuanSystemDbContext.systemInfroms.Remove( YanYiQuanSystemDbContext.systemInfroms.Find(Id));
            return   YanYiQuanSystemDbContext.SaveChanges() > 0 ? true : false;
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public  async Task<bool> UpdateData(SystemInfrom entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<List<SystemInfrom>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.systemInfroms.ToListAsync();
        }

        public async Task<List<SystemInfrom>> GetListByWhereAsync(Expression<Func<SystemInfrom, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.systemInfroms.Where(whereExpression).ToListAsync();
        }

        public Task<List<SystemInfrom>> GetListByPage<TKey>(Expression<Func<SystemInfrom, bool>> whereExpression, Expression<Func<SystemInfrom, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
