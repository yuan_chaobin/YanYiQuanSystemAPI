﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;
using YanYiQuanSystem.EntityFrameWorkCoreOracle;
using YanYiQuanSystem.IRepository;

namespace YanYiQuanSystem.Repository
{
    public class FirstPageModelRepository : IFirstPageModelRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public FirstPageModelRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public Task<bool> AddData(FirstPageModels entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchAddData(List<FirstPageModels> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<FirstPageModels>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.pageModels.ToListAsync();
        }

        public Task<List<FirstPageModels>> GetListByPage<TKey>(Expression<Func<FirstPageModels, bool>> whereExpression, Expression<Func<FirstPageModels, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<FirstPageModels>> GetListByWhereAsync(Expression<Func<FirstPageModels, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(FirstPageModels entity)
        {
            throw new NotImplementedException();
        }
    }
}
