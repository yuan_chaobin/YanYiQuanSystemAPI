﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;

    /// <summary>
    /// 模特列表仓储层
    /// </summary>
    public class ModelLibraryRepository : IModelLibraryRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public ModelLibraryRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public async Task<bool> AddData(ModelsLibrary entity)
        {
            YanYiQuanSystemDbContext.modelsLibraries.Add(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<ModelsLibrary> entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> BatchDelete(List<string> Ids)
        {
            foreach (var item in Ids)
            {
                YanYiQuanSystemDbContext.modelsLibraries.Remove(YanYiQuanSystemDbContext.modelsLibraries.Find(item));
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> DeleteData(string Id)
        {
            var del = YanYiQuanSystemDbContext.modelsLibraries.Find(Id);
            YanYiQuanSystemDbContext.modelsLibraries.Remove(del);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<List<ModelsLibrary>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.modelsLibraries.ToListAsync();
        }

        public Task<List<ModelsLibrary>> GetListByPage<TKey>(Expression<Func<ModelsLibrary, bool>> whereExpression, Expression<Func<ModelsLibrary, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ModelsLibrary>> GetListByWhereAsync(Expression<Func<ModelsLibrary, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.modelsLibraries.Where(whereExpression).ToListAsync();
        }

        public async Task<bool> UpdateUserModel(CommonData entity)
        {
            ModelsLibrary models = YanYiQuanSystemDbContext.modelsLibraries.Find(entity.ModelId);
            models.NickName = entity.NickName;
            models.Constellation = entity.Constellation;
            models.Height = entity.Height;
            models.Weight = entity.Weight;
            models.TheChest = entity.TheChest;
            models.Waistline = entity.Waistline;
            models.Hipline = entity.Hipline;
            User user = YanYiQuanSystemDbContext.users.Find(entity.UserId);
            user.Birthday = entity.Birthday;
            user.HeadPortrait = entity.HeadPortrait;
            user.UserName = entity.UserName;
            user.PhoneNumber = entity.PhoneNumber;
            user.PresentAddress = entity.PresentAddress;
            

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        /// <summary>
        /// 根据ID批量修改是否在首页状态
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateFirstPage(List<string> Ids)
        {
            foreach (var item in Ids)
            {
                ModelsLibrary models = YanYiQuanSystemDbContext.modelsLibraries.Find(item);
                models.IsFirstPage = (models.IsFirstPage == 1 ? 2 : 1);
                YanYiQuanSystemDbContext.Entry(models).State = EntityState.Modified;
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> UpdateData(ModelsLibrary entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateUserRank(User user)
        {
            User user1 = YanYiQuanSystemDbContext.users.Find(user.UserId);
            user1.UsersRank = user.UsersRank;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> UpdateAuditStatus(string photoId)
        {
            PhotoAlbums photo = YanYiQuanSystemDbContext.photoAlbums.Where(o => o.PhotoId == photoId).FirstOrDefault();
            photo.AuditStatus =(photo.AuditStatus == 1 ? 2 : 1);

            YanYiQuanSystemDbContext.Entry(photo).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> UpdateFirst(ModelsLibrary modelsLibrary)
        {
            ModelsLibrary models = YanYiQuanSystemDbContext.modelsLibraries.Find(modelsLibrary.ModelId);
            models.IsFirstPage = (modelsLibrary.IsFirstPage == 1 ? 2 : 1);

            YanYiQuanSystemDbContext.Entry(models).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
    }
}
