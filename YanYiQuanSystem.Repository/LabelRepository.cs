﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using YanYiQuanSystem.CommonUnitity;

    public class LabelRepository:ILabelRepository
    {
        public YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public LabelRepository(YanYiQuanSystemDbContext yanYiQuanSystemDbContext)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDbContext;
        }

        public async Task<bool> AddData(Label entity)
        {
             YanYiQuanSystemDbContext.labels.Add(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<Label> entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            YanYiQuanSystemDbContext.labels.Remove(YanYiQuanSystemDbContext.labels.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(Label entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Label>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.labels.ToListAsync();
        }

        public Task<List<Label>> GetListByWhereAsync(Expression<Func<Label, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<List<Label>> GetListByPage<TKey>(Expression<Func<Label, bool>> whereExpression, Expression<Func<Label, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
