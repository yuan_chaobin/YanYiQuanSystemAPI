﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using System.Threading.Tasks;
    using System.Linq.Expressions;
    using Microsoft.EntityFrameworkCore;
    using YanYiQuanSystem.CommonUnitity;
    using System.Linq;

    public class UsefulExpressionsManagerRespository:IUsefulExpressionsManagerRespository
    {
        public YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public UsefulExpressionsManagerRespository(YanYiQuanSystemDbContext yanYiQuanSystemDbContext)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDbContext;
        }

        public async Task<bool> AddData(UsefulExpressionsManager entity)
        {
            YanYiQuanSystemDbContext.usefulExpressionsManagers.Add(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<UsefulExpressionsManager> entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            YanYiQuanSystemDbContext.usefulExpressionsManagers.Remove(YanYiQuanSystemDbContext.usefulExpressionsManagers.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>

        public async Task<bool> BatchDelete(List<string> Ids)
        {
            foreach (var item in Ids)
            {
                YanYiQuanSystemDbContext.usefulExpressionsManagers.Remove(YanYiQuanSystemDbContext.usefulExpressionsManagers.Find(item));
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> UpdateData(UsefulExpressionsManager entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<List<UsefulExpressionsManager>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.usefulExpressionsManagers.ToListAsync();
        }

        public async Task<List<UsefulExpressionsManager>> GetListByWhereAsync(Expression<Func<UsefulExpressionsManager, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.usefulExpressionsManagers.Where(whereExpression).ToListAsync();
        }

        public Task<List<UsefulExpressionsManager>> GetListByPage<TKey>(Expression<Func<UsefulExpressionsManager, bool>> whereExpression, Expression<Func<UsefulExpressionsManager, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }
    }
}
