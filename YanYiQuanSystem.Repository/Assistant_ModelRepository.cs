﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;

    public class Assistant_ModelRepository : IAssistant_ModelRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public Assistant_ModelRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public Task<bool> AddData(Assistant_Model entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchAddData(List<Assistant_Model> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            var del = YanYiQuanSystemDbContext.assistant_Models.Find(Id);
            YanYiQuanSystemDbContext.assistant_Models.Remove(del);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<List<Assistant_Model>> GetListAsync()
        {
            return YanYiQuanSystemDbContext.assistant_Models.ToListAsync();
        }

        public Task<List<Assistant_Model>> GetListByPage<TKey>(Expression<Func<Assistant_Model, bool>> whereExpression, Expression<Func<Assistant_Model, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<Assistant_Model>> GetListByWhereAsync(Expression<Func<Assistant_Model, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(Assistant_Model entity)
        {
            throw new NotImplementedException();
        }
    }
}
