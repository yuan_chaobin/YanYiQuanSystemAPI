﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;

    public class ReCompanyRepository : IReCompanyRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public ReCompanyRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }
        /// <summary>
        /// 添加公司的信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddData(RecruitmentCompanys entity)
        {
            await YanYiQuanSystemDbContext.recruitmentCompanys.AddAsync(entity);

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<RecruitmentCompanys> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            var del = YanYiQuanSystemDbContext.recruitmentCompanys.Find(Id);
            YanYiQuanSystemDbContext.recruitmentCompanys.Remove(del);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<List<RecruitmentCompanys>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.recruitmentCompanys.ToListAsync();
        }

        public Task<List<RecruitmentCompanys>> GetListByPage<TKey>(Expression<Func<RecruitmentCompanys, bool>> whereExpression, Expression<Func<RecruitmentCompanys, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<RecruitmentCompanys>> GetListByWhereAsync(Expression<Func<RecruitmentCompanys, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(RecruitmentCompanys entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }


        public async Task<bool> UpdateCompany(CommonData entity)
        {
            RecruitmentCompanys companys = YanYiQuanSystemDbContext.recruitmentCompanys.Find(entity.HrId);
            companys.CompanyName = entity.CompanyName;
            companys.CompanyAddress = entity.CompanyAddress;
            companys.CompanyPerson = entity.CompanyPerson;
            companys.CompanyType = (int)entity.CompanyType;
            User user = YanYiQuanSystemDbContext.users.Find(entity.UserId);
            user.HeadPortrait = entity.HeadPortrait;
            user.UserName = entity.UserName;
            user.PhoneNumber = entity.PhoneNumber;

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
    }
}
