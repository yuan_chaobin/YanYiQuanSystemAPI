﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using System.Threading.Tasks;
    using System.Linq.Expressions;
    using Microsoft.EntityFrameworkCore;
    using YanYiQuanSystem.CommonUnitity;
    using System.Linq;

    public  class SensibilityManagerRepository:ISensibilityManagerRepository
    {
        public YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public SensibilityManagerRepository(YanYiQuanSystemDbContext yanYiQuanSystemDbContext)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDbContext;
        }
        /// <summary>
        /// 添加敏感字
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddData(SensibilityManager entity)
        {
            YanYiQuanSystemDbContext.sensibilityManagers.Add(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<SensibilityManager> entity)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 删除敏感字
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteData(string Id)
        {
            YanYiQuanSystemDbContext.sensibilityManagers.Remove( YanYiQuanSystemDbContext.sensibilityManagers.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public async Task<bool> BatchDelete(List<string> Ids)
        {

            foreach (var item in Ids)
            {
                YanYiQuanSystemDbContext.sensibilityManagers.Remove(YanYiQuanSystemDbContext.sensibilityManagers.Find(item));
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 修改敏感字
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateData(SensibilityManager entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 获取敏感字数据
        /// </summary>
        /// <returns></returns>
        public async Task<List<SensibilityManager>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.sensibilityManagers.ToListAsync();
        }
        //条件查询
        public async Task<List<SensibilityManager>> GetListByWhereAsync(Expression<Func<SensibilityManager, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.sensibilityManagers.Where(whereExpression).ToListAsync();
        }

        public Task<List<SensibilityManager>> GetListByPage<TKey>(Expression<Func<SensibilityManager, bool>> whereExpression, Expression<Func<SensibilityManager, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }
    }
}
