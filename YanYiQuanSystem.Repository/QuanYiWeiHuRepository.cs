﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;
    public  class QuanYiWeiHuRepository: IQuanYiWeiHuRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public QuanYiWeiHuRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public async Task<bool> AddData(QuanYiWeiHu entity)
        {
            YanYiQuanSystemDbContext.quanYiWeiHus.Add(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true:false;
        }

        public Task<bool> BatchAddData(List<QuanYiWeiHu> entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
              YanYiQuanSystemDbContext.quanYiWeiHus.Remove(  YanYiQuanSystemDbContext.quanYiWeiHus.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateData(QuanYiWeiHu entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<List<QuanYiWeiHu>> GetListAsync()
        {
            return YanYiQuanSystemDbContext.quanYiWeiHus.ToListAsync();
        }

        public async Task<List<QuanYiWeiHu>> GetListByWhereAsync(Expression<Func<QuanYiWeiHu, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.quanYiWeiHus.Where(whereExpression).ToListAsync();
        }

        public Task<List<QuanYiWeiHu>> GetListByPage<TKey>(Expression<Func<QuanYiWeiHu, bool>> whereExpression, Expression<Func<QuanYiWeiHu, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
