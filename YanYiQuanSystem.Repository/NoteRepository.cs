﻿using System;
using System.Collections.Generic;
using System.Text;


namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;

    public class NoteRepository: INoteRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public NoteRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public Task<bool> AddData(NoteManager entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchAddData(List<NoteManager> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(NoteManager entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<NoteManager>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.noteManagers.ToListAsync();
        }

        public  async Task<List<NoteManager>> GetListByWhereAsync(Expression<Func<NoteManager, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.noteManagers.Where(whereExpression).ToListAsync();
        }

        public Task<List<NoteManager>> GetListByPage<TKey>(Expression<Func<NoteManager, bool>> whereExpression, Expression<Func<NoteManager, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
