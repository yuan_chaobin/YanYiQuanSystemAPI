﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using YanYiQuanSystem.CommonUnitity;
    public class BannerRepository : IBannerRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public BannerRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }
        public async Task<bool> AddData(Banners entity)
        {
            await YanYiQuanSystemDbContext.banners.AddAsync(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<Banners> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<List<Banners>> GetListAsync()
        {
            return YanYiQuanSystemDbContext.banners.ToListAsync();
        }

        public async Task<List<Banners>> GetListByPage<TKey>(Expression<Func<Banners, bool>> whereExpression, Expression<Func<Banners, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return await YanYiQuanSystemDbContext.Set<Banners>().Where(whereExpression).OrderBy(orderExpression).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        public async Task<List<Banners>> GetListByWhereAsync(Expression<Func<Banners, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.banners.Where(whereExpression).ToListAsync();
        }

        public async Task<bool> UpdateData(Banners entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }
    }
}
