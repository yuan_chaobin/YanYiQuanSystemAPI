﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using YanYiQuanSystem.CommonUnitity;

    public class EmpRepository:IEmpRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public EmpRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }
        /// <summary>
        /// 添加员工
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddData(Emp entity)
        {
            await YanYiQuanSystemDbContext.emps.AddAsync(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> BatchAddData(List<Emp> entity)
        {
            foreach (var item in entity)
            {
                YanYiQuanSystemDbContext.emps.Add(item);
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 获取所有员工数据
        /// </summary>
        /// <returns></returns>
        public Task<List<Emp>> GetListAsync()
        {
            return YanYiQuanSystemDbContext.emps.ToListAsync();
        }
        /// <summary>
        /// 根据条件进行查询
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<Emp>> GetListByWhereAsync(Expression<Func<Emp, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.emps.Where(whereExpression).ToListAsync();
        }

        public async Task<List<Emp>> GetListByPage<TKey>(Expression<Func<Emp, bool>> whereExpression, Expression<Func<Emp, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return await YanYiQuanSystemDbContext.Set<Emp>().Where(whereExpression).OrderBy(orderExpression).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        /// <summary>
        /// 单个删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteData(string Id)
        {
            YanYiQuanSystemDbContext.emps.Remove(YanYiQuanSystemDbContext.emps.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public async Task<bool> BatchDelete(List<string> Ids)
        {
            foreach (var item in Ids)
            {
                YanYiQuanSystemDbContext.emps.Remove(YanYiQuanSystemDbContext.emps.Find(item));
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateData(Emp entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }
    }
}
