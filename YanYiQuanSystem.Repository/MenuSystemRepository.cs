﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using YanYiQuanSystem.CommonUnitity;

    public class MenuSystemRepository : IMenuSystemRepository
    {
        public YanYiQuanSystemDbContext YanYiQuanSystemDbContext;
        public MenuSystemRepository(YanYiQuanSystemDbContext yanYiQuanSystemDbContext)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDbContext;
        }
        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddData(MenuSystem entity)
        {
            await  YanYiQuanSystemDbContext.menuSystems.AddAsync(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> BatchAddData(List<MenuSystem> entity)
        {
            foreach (var item in entity)
            {
                YanYiQuanSystemDbContext.menuSystems.Add(item);
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> BatchDelete(List<string> Ids)
        {
            foreach (var item in Ids)
            {
                YanYiQuanSystemDbContext.menuSystems.Remove(YanYiQuanSystemDbContext.menuSystems.Find(item));
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> DeleteData(string Id)
        {
            YanYiQuanSystemDbContext.menuSystems.Remove(YanYiQuanSystemDbContext.menuSystems.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<List<MenuSystem>> GetListAsync()
        {
          return await YanYiQuanSystemDbContext.menuSystems.ToListAsync();
        }

        public  Task<List<MenuSystem>> GetListByPage<TKey>(Expression<Func<MenuSystem, bool>> whereExpression, Expression<Func<MenuSystem, TKey>> orderExpression, int pageIndex, int pageSize)
        {         
            throw new NotImplementedException();
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<MenuSystem>> GetListByWhereAsync(Expression<Func<MenuSystem, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.menuSystems.Where(whereExpression).ToListAsync();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateData(MenuSystem entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }
    }
}
