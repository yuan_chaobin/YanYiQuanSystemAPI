﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;
    public class RepositoryActivity_report : IRepository<Activity_report>
    {
        /// <summary>
        /// 注入上下文类
        /// </summary>
        public YanYiQuanSystemDbContext YanYiQuanSystemDbContext;
        public RepositoryActivity_report(YanYiQuanSystemDbContext yanYiQuanSystemDbContext)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDbContext;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">类的对象</param>
        /// <returns></returns>
        public async Task<bool> AddData(Activity_report entity)
        {
            YanYiQuanSystemDbContext.Activity_report.Add(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity">类的对象集合</param>
        /// <returns></returns>
        public async Task<bool> BatchAddData(List<Activity_report> entity)
        {
            foreach (var item in entity)
            {
                item.Uuid = Guid.NewGuid().ToString();
                YanYiQuanSystemDbContext.Activity_report.Add(item);
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Ids">编号集合</param>
        /// <returns></returns>
        public async Task<bool> BatchDelete(List<string> Ids)
        {
            foreach (var item in Ids)
            {
                YanYiQuanSystemDbContext.Activity_report.Remove(YanYiQuanSystemDbContext.Activity_report.Find(item));
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id">编号</param>
        /// <returns></returns>
        public async Task<bool> DeleteData(string Id)
        {
            YanYiQuanSystemDbContext.Activity_report.Remove(YanYiQuanSystemDbContext.Activity_report.Find(Id));
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns></returns>
        public async Task<List<Activity_report>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.Activity_report.ToListAsync();
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression">查询条件</param>
        /// <param name="orderExpression">排序</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">尺寸</param>
        /// <returns></returns>
        public async Task<List<Activity_report>> GetListByPage<TKey>(Expression<Func<Activity_report, bool>> whereExpression, Expression<Func<Activity_report, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return await YanYiQuanSystemDbContext.Activity_report.Where(whereExpression).OrderBy(orderExpression).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        public async Task<List<Activity_report>> GetListByWhereAsync(Expression<Func<Activity_report, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.Activity_report.Where(whereExpression).ToListAsync();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity">ID已有的类的对象</param>
        /// <returns></returns>
        public async Task<bool> UpdateData(Activity_report entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }
    }
}
