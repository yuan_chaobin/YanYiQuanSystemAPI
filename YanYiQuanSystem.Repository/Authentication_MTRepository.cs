﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using YanYiQuanSystem.CommonUnitity;

    public class Authentication_MTRepository : IMT_Repository
    {

        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public Authentication_MTRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public async Task<bool> AddData(Authentication_MT entity)
        {
            await YanYiQuanSystemDbContext.DBAT_MTs.AddAsync(entity);

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<Authentication_MT> entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            var del = YanYiQuanSystemDbContext.DBAT_MTs.Find(Id);
            YanYiQuanSystemDbContext.DBAT_MTs.Remove(del);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateData(Authentication_MT entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<List<Authentication_MT>> GetListAsync()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<Authentication_MT>> GetListByWhereAsync(Expression<Func<Authentication_MT, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.DBAT_MTs.Where(whereExpression).ToListAsync();
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Authentication_MT>> GetListByPage<TKey>(Expression<Func<Authentication_MT, bool>> whereExpression, Expression<Func<Authentication_MT, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            List<Authentication_MT> sss = await YanYiQuanSystemDbContext.DBAT_MTs.Where(whereExpression).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            return sss;
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 模特基本信息调用修改 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> wxUpdateUserModel(CommonData entity)
        {
            ModelsLibrary models = YanYiQuanSystemDbContext.modelsLibraries.Find(entity.ModelID);
            models.NickName = entity.NickName;//昵称
            models.BloodType = entity.BloodType;
            models.TheChest = entity.TheChest;//熊
            models.Waistline = entity.Waistline;//姚
            models.Hipline = entity.Hipline;//臀
            models.Size = entity.Size;//脚
            models.Constellation = entity.Constellation;//星座
            models.Weight = entity.Weight;//体重
            models.Height = entity.Height;//身高
            User user = YanYiQuanSystemDbContext.users.Find(entity.UserId);
            user.Birthday = entity.Birthday;
            user.UserName = entity.UserName;//用户名
            user.Sex = entity.Sex;
            user.PhoneNumber = entity.PhoneNumber;
            user.PresentAddress = entity.PresentAddress;//居住地
            user.IdentityNumber = entity.IdentityNumber;

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        /// <summary>
        /// 修改用户头像
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> wxExitorImg(string id,string img )
        {
            User user = YanYiQuanSystemDbContext.users.Find(id);
            user.HeadPortrait = img;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
    }
}
