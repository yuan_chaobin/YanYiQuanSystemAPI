﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;

    public class AssistantRepository : IAssistantRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public AssistantRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }
        /// <summary>
        /// 添加助理
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddData(Assistant entity)
        {
            YanYiQuanSystemDbContext.assistants.Add(entity);

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<Assistant> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            var del = YanYiQuanSystemDbContext.assistants.Find(Id);
            YanYiQuanSystemDbContext.assistants.Remove(del);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<List<Assistant>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.assistants.ToListAsync();
        }

        public Task<List<Assistant>> GetListByPage<TKey>(Expression<Func<Assistant, bool>> whereExpression, Expression<Func<Assistant, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Assistant>> GetListByWhereAsync(Expression<Func<Assistant, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.assistants.Where(whereExpression).ToListAsync();
        }

        public Task<bool> UpdateData(Assistant entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateAssistant(CommonData entity)
        {
            Assistant assistant = YanYiQuanSystemDbContext.assistants.Find(entity.AssistantId);
            
            User user = YanYiQuanSystemDbContext.users.Find(entity.UserId);
            user.Sex = entity.Sex;
            user.HeadPortrait = entity.HeadPortrait;
            user.UserName = entity.UserName;
            user.PhoneNumber = entity.PhoneNumber;
            user.PresentAddress = entity.PresentAddress;
            user.IdentityNumber = entity.IdentityNumber;

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
    }
}
