﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;

    public class PurchaseRecordRepository : IPurchaseRecordRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public PurchaseRecordRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public Task<bool> AddData(PurchaseRecord entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchAddData(List<PurchaseRecord> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<PurchaseRecord>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.purchaseRecords.ToListAsync();
        }

        public Task<List<PurchaseRecord>> GetListByPage<TKey>(Expression<Func<PurchaseRecord, bool>> whereExpression, Expression<Func<PurchaseRecord, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<PurchaseRecord>> GetListByWhereAsync(Expression<Func<PurchaseRecord, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(PurchaseRecord entity)
        {
            throw new NotImplementedException();
        }
    }
}
