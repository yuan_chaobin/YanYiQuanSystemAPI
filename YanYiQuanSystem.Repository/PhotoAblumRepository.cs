﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;

    public class PhotoAblumRepository : IPhotoAlbumRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public PhotoAblumRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public Task<bool> AddData(PhotoAlbums entity)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 添加个人相册
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> BatchAddData(List<PhotoAlbums> entity)
        {
            foreach (PhotoAlbums item in entity)
            {
                await YanYiQuanSystemDbContext.photoAlbums.AddAsync(item);
            }
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;

        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 相册的批量删除
        /// </summary>
        /// <param name="Id">用户id</param>
        /// <returns></returns>
        public async Task<bool> DeleteData(string Id)
        {
            List<PhotoAlbums> photos = YanYiQuanSystemDbContext.photoAlbums.Where(o => o.UserID.Equals(Id)).ToListAsync().Result;
            YanYiQuanSystemDbContext.photoAlbums.RemoveRange(photos);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;

        }

        public async Task<List<PhotoAlbums>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.photoAlbums.ToListAsync();
        }

        public Task<List<PhotoAlbums>> GetListByPage<TKey>(Expression<Func<PhotoAlbums, bool>> whereExpression, Expression<Func<PhotoAlbums, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<PhotoAlbums>> GetListByWhereAsync(Expression<Func<PhotoAlbums, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(PhotoAlbums entity)
        {
            throw new NotImplementedException();
        }
    }
}
