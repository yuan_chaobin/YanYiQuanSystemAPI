﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using YanYiQuanSystem.CommonUnitity;

    public class UsersRepository : IUsersRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public UsersRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public async Task<bool> AddData(User entity)
        {
            await YanYiQuanSystemDbContext.users.AddAsync(entity);

            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;

        }

        public Task<bool> BatchAddData(List<User> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<List<User>> GetListAsync()
        {
            return YanYiQuanSystemDbContext.users.ToListAsync();
        }

        public Task<List<User>> GetListByPage<TKey>(Expression<Func<User, bool>> whereExpression, Expression<Func<User, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<User>> GetListByWhereAsync(Expression<Func<User, bool>> whereExpression)
        {
            return YanYiQuanSystemDbContext.users.Where(whereExpression). ToListAsync();
        }

        public Task<bool> UpdateData(User entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateFirstPage(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateUserModel(CommonData entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateCurrentRole(User user)
        {
            User user1 = YanYiQuanSystemDbContext.users.Find(user.UserId);
            user1.CurrentRole = user.CurrentRole;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }
    }
}
