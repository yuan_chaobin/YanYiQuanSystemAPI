﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Repository
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using YanYiQuanSystem.CommonUnitity;
    public class RightsConfigurationRepository:IRightsConfigurationRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public RightsConfigurationRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public async Task<bool> AddData(RightsConfiguration entity)
        {
            await YanYiQuanSystemDbContext.rightsConfigurations.AddAsync(entity);
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public Task<bool> BatchAddData(List<RightsConfiguration> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateData(RightsConfiguration entity)
        {
            YanYiQuanSystemDbContext.Entry(entity).State = EntityState.Modified;
            return await YanYiQuanSystemDbContext.SaveChangesAsync() > 0 ? true : false;
        }

        public  Task<List<RightsConfiguration>> GetListAsync()
        {
            return YanYiQuanSystemDbContext.rightsConfigurations.ToListAsync();
        }

        public async Task<List<RightsConfiguration>> GetListByWhereAsync(Expression<Func<RightsConfiguration, bool>> whereExpression)
        {
            return await YanYiQuanSystemDbContext.rightsConfigurations.Where(whereExpression).ToListAsync();
        }

        public Task<List<RightsConfiguration>> GetListByPage<TKey>(Expression<Func<RightsConfiguration, bool>> whereExpression, Expression<Func<RightsConfiguration, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
