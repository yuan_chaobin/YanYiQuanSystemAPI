﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;
using YanYiQuanSystem.EntityFrameWorkCoreOracle;
using YanYiQuanSystem.IRepository;

namespace YanYiQuanSystem.Repository
{
    public class AttentionRepository : IAttentionRepository
    {
        private YanYiQuanSystemDbContext YanYiQuanSystemDbContext { get; set; }
        public AttentionRepository(YanYiQuanSystemDbContext yanYiQuanSystemDb)
        {
            this.YanYiQuanSystemDbContext = yanYiQuanSystemDb;
        }

        public Task<bool> AddData(Attention entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchAddData(List<Attention> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Attention>> GetListAsync()
        {
            return await YanYiQuanSystemDbContext.attentions.ToListAsync();
        }

        public Task<List<Attention>> GetListByPage<TKey>(Expression<Func<Attention, bool>> whereExpression, Expression<Func<Attention, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<Attention>> GetListByWhereAsync(Expression<Func<Attention, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(Attention entity)
        {
            throw new NotImplementedException();
        }
    }
}
