﻿using System;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using YanYiQuanSystem.Domain;

    public class YanYiQuanSystemDbContext:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //创建连接通道
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            IConfigurationBuilder builder = configurationBuilder.AddJsonFile("appsettings.json");
            IConfiguration configuration = builder.Build();
            //获取连接Oracle数据库字符串
            string str = configuration.GetSection("OracleConnection").Value;
            optionsBuilder.UseOracle(str,r => r.UseOracleSQLCompatibility("11"));
        }
        
        /// <summary>
        /// 助理列表
        /// </summary>
        public DbSet<Assistant> assistants { get; set; }
        public DbSet<Assistant_Model> assistant_Models { get; set; }//助理_模特表
        public DbSet<PurchaseRecord> purchaseRecords { get; set; }//用户权益购买记录表
        public DbSet<RecruitmentLibrary> recruitments { get; set; }//招聘库表
        public DbSet<UserRights> userRights { get; set; }//用户权益表
        public DbSet<User> users { get; set; }//用户
        public DbSet<Attention> attentions { get; set; }//关注
        public DbSet<ModelsLibrary> modelsLibraries { get; set; }//模特列表
        public DbSet<FirstPageModels> pageModels { get; set; }//首页模特表
        public DbSet<RecruitmentCompanys> recruitmentCompanys { get; set; }//招聘公司
        public DbSet<PhotoAlbums> photoAlbums { get; set; }//相册库

        /// <summary>
        /// 菜单表
        /// </summary>
        public DbSet<MenuSystem> menuSystems { get; set; }
        /// <summary>
        /// 常用语管理表
        /// </summary>
        public DbSet<UsefulExpressionsManager> usefulExpressionsManagers { get; set; }
        /// <summary>
        /// 敏感字管理表
        /// </summary>
        public DbSet<SensibilityManager> sensibilityManagers { get; set; }
        /// <summary>
        /// 员工
        /// </summary>
        public DbSet<Emp> emps { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public DbSet<Role> roles { get; set; }
        /// <summary>
        /// 认证模特
        /// </summary>
        public DbSet<Authentication_MT> DBAT_MTs { get; set; }
        /// <summary>
        /// 认证公司
        /// </summary>
        public DbSet<Authentication_GS> DBAT_GSs { get; set; }

        public DbSet<Activity> Activity { get; set; }
        public DbSet<Activity_report> Activity_report { get; set; }
        public DbSet<Activity_user> Activity_user { get; set; }
        public DbSet<Activity_vote> Activity_vote { get; set; }
        public DbSet<Activity_zone> Activity_zone { get; set; }
        public DbSet<NoteManager> noteManagers { get; set; }
        /// <summary>
        /// Banner维护
        /// </summary>
        public DbSet<Banners> banners { get; set; }
        public DbSet<RightsConfiguration> rightsConfigurations { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        public DbSet<Label> labels { get; set; }
        /// <summary>
        /// 系统权益产品维护
        /// </summary>
        public DbSet<QuanYiWeiHu>  quanYiWeiHus { get; set; }
        /// <summary>
        /// 系统通知
        /// </summary>
        public DbSet<SystemInfrom>  systemInfroms { get; set; }

        public DbSet<Role_Menu> role_Menus { get; set; }
        
    }
}
