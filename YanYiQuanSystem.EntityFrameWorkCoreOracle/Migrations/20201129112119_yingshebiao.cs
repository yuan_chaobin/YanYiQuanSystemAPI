﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class yingshebiao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Role_Menu",
                columns: table => new
                {
                    EMId = table.Column<string>(maxLength: 50, nullable: false),
                    RoleId = table.Column<string>(maxLength: 50, nullable: true),
                    MenuID = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role_Menu", x => x.EMId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Role_Menu");
        }
    }
}
