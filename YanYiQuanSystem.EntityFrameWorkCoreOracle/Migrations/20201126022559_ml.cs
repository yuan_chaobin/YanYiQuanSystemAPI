﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class ml : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PhotoAlbums",
                columns: table => new
                {
                    PhotoId = table.Column<string>(maxLength: 50, nullable: false),
                    UserID = table.Column<string>(maxLength: 50, nullable: true),
                    Photos = table.Column<string>(maxLength: 500000, nullable: true),
                    AuditStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoAlbums", x => x.PhotoId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhotoAlbums");
        }
    }
}
