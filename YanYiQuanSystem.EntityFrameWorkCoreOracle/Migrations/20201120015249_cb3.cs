﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class cb3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Assistant",
                columns: table => new
                {
                    AssistantId = table.Column<string>(maxLength: 50, nullable: false),
                    UsersID = table.Column<string>(maxLength: 50, nullable: true),
                    InvitationCode = table.Column<string>(maxLength: 50, nullable: true),
                    MyCode = table.Column<string>(maxLength: 50, nullable: true),
                    RegisterTime = table.Column<DateTime>(nullable: false),
                    IsLogging = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assistant", x => x.AssistantId);
                });

            migrationBuilder.CreateTable(
                name: "Assistant_Model",
                columns: table => new
                {
                    Assistant_ModelId = table.Column<string>(maxLength: 50, nullable: false),
                    AssistantID = table.Column<string>(maxLength: 50, nullable: true),
                    ModelID = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assistant_Model", x => x.Assistant_ModelId);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseRecord",
                columns: table => new
                {
                    PurchaseRecordId = table.Column<string>(maxLength: 50, nullable: false),
                    UserRightsID = table.Column<string>(maxLength: 50, nullable: true),
                    BuyRole = table.Column<int>(nullable: false),
                    ProductName = table.Column<string>(maxLength: 50, nullable: true),
                    MoneySum = table.Column<decimal>(nullable: false),
                    DemandPayTime = table.Column<DateTime>(nullable: false),
                    SuccessPayTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseRecord", x => x.PurchaseRecordId);
                });

            migrationBuilder.CreateTable(
                name: "RecruitmentLibrary",
                columns: table => new
                {
                    RecLibraryId = table.Column<string>(maxLength: 50, nullable: false),
                    CompanyName = table.Column<string>(maxLength: 50, nullable: true),
                    PostName = table.Column<string>(maxLength: 50, nullable: true),
                    AuditStatus = table.Column<int>(nullable: false),
                    IsValid = table.Column<int>(nullable: false),
                    SalaryType = table.Column<int>(nullable: false),
                    SalaryScope = table.Column<string>(maxLength: 50, nullable: true),
                    MinSalary = table.Column<int>(nullable: false),
                    MaxSalary = table.Column<int>(nullable: false),
                    WorkType = table.Column<int>(nullable: false),
                    PostSex = table.Column<int>(nullable: false),
                    Height = table.Column<string>(maxLength: 50, nullable: true),
                    Education = table.Column<int>(nullable: false),
                    WorkYears = table.Column<string>(maxLength: 50, nullable: true),
                    WorkPlace = table.Column<string>(maxLength: 50, nullable: true),
                    RecPeoples = table.Column<int>(nullable: false),
                    PostTag = table.Column<int>(nullable: false),
                    PostRemarks = table.Column<string>(maxLength: 50, nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecruitmentLibrary", x => x.RecLibraryId);
                });

            migrationBuilder.CreateTable(
                name: "UserRights",
                columns: table => new
                {
                    UserRightsId = table.Column<string>(maxLength: 50, nullable: false),
                    UsersID = table.Column<string>(maxLength: 50, nullable: true),
                    RightsRole = table.Column<int>(nullable: false),
                    RightsName = table.Column<string>(maxLength: 50, nullable: true),
                    RightsType = table.Column<int>(nullable: false),
                    RightsTypeWay = table.Column<int>(nullable: false),
                    Times = table.Column<int>(nullable: false),
                    StratTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRights", x => x.UserRightsId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Assistant");

            migrationBuilder.DropTable(
                name: "Assistant_Model");

            migrationBuilder.DropTable(
                name: "PurchaseRecord");

            migrationBuilder.DropTable(
                name: "RecruitmentLibrary");

            migrationBuilder.DropTable(
                name: "UserRights");
        }
    }
}
