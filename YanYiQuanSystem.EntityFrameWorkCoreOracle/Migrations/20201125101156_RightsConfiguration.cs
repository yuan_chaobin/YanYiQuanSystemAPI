﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class RightsConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Weight",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "Waistline",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "TheChest",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "Size",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "PhotoStatus",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "LoggingStatus",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "IsLogging1",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "IsFirstPage",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "IsAssistant",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "IsActivity",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "Hipline",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "Height",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<int>(
                name: "ApproveStatus",
                table: "ModelsLibrary",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.AlterColumn<string>(
                name: "ActivityId",
                table: "ModelsLibrary",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)");

            migrationBuilder.CreateTable(
                name: "RightsConfiguration",
                columns: table => new
                {
                    RightsId = table.Column<string>(maxLength: 50, nullable: false),
                    RightsName = table.Column<string>(maxLength: 50, nullable: true),
                    Sort = table.Column<int>(nullable: true),
                    RightsClass = table.Column<int>(nullable: true),
                    RightsRole = table.Column<int>(nullable: true),
                    RightWay = table.Column<int>(nullable: true),
                    Explain = table.Column<string>(maxLength: 50, nullable: true),
                    Start = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RightsConfiguration", x => x.RightsId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RightsConfiguration");

            migrationBuilder.AlterColumn<int>(
                name: "Weight",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Waistline",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TheChest",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Size",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PhotoStatus",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LoggingStatus",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IsLogging1",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IsFirstPage",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IsAssistant",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IsActivity",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Hipline",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Height",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ApproveStatus",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ActivityId",
                table: "ModelsLibrary",
                type: "NUMBER(10)",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);
        }
    }
}
