﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class manager : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SensibilityManager",
                columns: table => new
                {
                    SensibilityManagerID = table.Column<string>(maxLength: 32, nullable: false),
                    SensibilityManagerName = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SensibilityManager", x => x.SensibilityManagerID);
                });

            migrationBuilder.CreateTable(
                name: "UsefulExpressionsManager",
                columns: table => new
                {
                    UsefulExpressionsManagerID = table.Column<string>(maxLength: 32, nullable: false),
                    UsefulExpressionsManagerName = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsefulExpressionsManager", x => x.UsefulExpressionsManagerID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SensibilityManager");

            migrationBuilder.DropTable(
                name: "UsefulExpressionsManager");
        }
    }
}
