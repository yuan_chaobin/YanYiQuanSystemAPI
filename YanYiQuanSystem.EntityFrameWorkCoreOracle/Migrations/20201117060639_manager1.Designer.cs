﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Oracle.EntityFrameworkCore.Metadata;
using YanYiQuanSystem.EntityFrameWorkCoreOracle;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    [DbContext(typeof(YanYiQuanSystemDbContext))]
    [Migration("20201117060639_manager1")]
    partial class manager1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn)
                .HasAnnotation("ProductVersion", "3.1.9")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            modelBuilder.Entity("YanYiQuanSystem.Domain.MenuSystem", b =>
                {
                    b.Property<string>("MenuID")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreateTime")
                        .HasColumnType("TIMESTAMP(7)");

                    b.Property<int>("Is_delete")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("MenuName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<int>("MenuPid")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("MenuUrl")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<DateTime>("ModifyTime")
                        .HasColumnType("TIMESTAMP(7)");

                    b.HasKey("MenuID");

                    b.ToTable("MenuSystem");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.SensibilityManager", b =>
                {
                    b.Property<string>("SensibilityManagerID")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("SensibilityManagerName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.HasKey("SensibilityManagerID");

                    b.ToTable("SensibilityManager");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.UsefulExpressionsManager", b =>
                {
                    b.Property<string>("UsefulExpressionsManagerID")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("UsefulExpressionsManagerName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.HasKey("UsefulExpressionsManagerID");

                    b.ToTable("UsefulExpressionsManager");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.Users", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Password")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("UserName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.HasKey("UserId");

                    b.ToTable("Users");
                });
#pragma warning restore 612, 618
        }
    }
}
