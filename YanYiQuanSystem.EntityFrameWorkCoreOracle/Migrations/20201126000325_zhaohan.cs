﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class zhaohan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 36, nullable: false),
                    Desc_activity = table.Column<string>(maxLength: 32, nullable: true),
                    Time_from_baoming = table.Column<DateTime>(nullable: false),
                    Time_to_baoming = table.Column<DateTime>(nullable: false),
                    Time_from_xuanba = table.Column<DateTime>(nullable: false),
                    Time_to_xuanba = table.Column<DateTime>(nullable: false),
                    Count_max = table.Column<int>(nullable: false),
                    Category_activity = table.Column<string>(nullable: true),
                    Comments = table.Column<string>(maxLength: 200, nullable: true),
                    Url_pic_activity = table.Column<string>(maxLength: 500000, nullable: true),
                    Rule_activity = table.Column<string>(maxLength: 280, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Activity_report",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 36, nullable: false),
                    Id_activity = table.Column<string>(maxLength: 32, nullable: true),
                    Id_user = table.Column<string>(maxLength: 36, nullable: true),
                    Id_user_report_by = table.Column<string>(maxLength: 36, nullable: true),
                    Report_content = table.Column<string>(maxLength: 200, nullable: true),
                    Time_trans = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_report", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Activity_user",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 36, nullable: false),
                    Id_activity = table.Column<string>(maxLength: 36, nullable: true),
                    Id_user = table.Column<string>(maxLength: 36, nullable: true),
                    Activity_zone_name = table.Column<string>(maxLength: 36, nullable: true),
                    Name_user = table.Column<string>(maxLength: 36, nullable: true),
                    Phone_user = table.Column<string>(maxLength: 36, nullable: true),
                    Nickname = table.Column<string>(maxLength: 32, nullable: true),
                    Idcard_user = table.Column<string>(maxLength: 32, nullable: true),
                    Url_pic_head = table.Column<string>(maxLength: 500000, nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Blood_type = table.Column<string>(maxLength: 32, nullable: true),
                    Height = table.Column<int>(nullable: false),
                    Weight = table.Column<decimal>(nullable: false),
                    Date_birthday = table.Column<DateTime>(maxLength: 32, nullable: false),
                    Xingzuo = table.Column<string>(maxLength: 32, nullable: true),
                    Region = table.Column<string>(maxLength: 32, nullable: true),
                    Xiema = table.Column<int>(nullable: false),
                    Yaowei = table.Column<decimal>(nullable: false),
                    Xiongwei = table.Column<decimal>(nullable: false),
                    Tunwei = table.Column<decimal>(nullable: false),
                    Weibo = table.Column<string>(maxLength: 32, nullable: true),
                    Qq = table.Column<string>(maxLength: 32, nullable: true),
                    Comments = table.Column<string>(maxLength: 200, nullable: true),
                    Shenhe_status = table.Column<int>(nullable: false),
                    Is_mainland = table.Column<int>(nullable: false),
                    Is_allow_index = table.Column<int>(nullable: false),
                    Create_time = table.Column<DateTime>(nullable: false),
                    Url_homecover = table.Column<string>(maxLength: 32, nullable: true),
                    Piaoshu = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_user", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Activity_vote",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 36, nullable: false),
                    Id_activity = table.Column<string>(maxLength: 36, nullable: true),
                    Id_user = table.Column<string>(maxLength: 36, nullable: true),
                    Id_user_dianzan = table.Column<string>(maxLength: 36, nullable: true),
                    Time_dianzan = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_vote", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Activity_zone",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 36, nullable: false),
                    Id_activity = table.Column<string>(maxLength: 36, nullable: true),
                    Zone_name = table.Column<string>(maxLength: 32, nullable: true),
                    Comments = table.Column<string>(maxLength: 200, nullable: true),
                    Order_num = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_zone", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Authentication_GS",
                columns: table => new
                {
                    AuGSid = table.Column<string>(maxLength: 36, nullable: false),
                    GSHeadPortraits = table.Column<string>(maxLength: 500000, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    PnoneNumber = table.Column<string>(maxLength: 50, nullable: true),
                    IdentityNumber = table.Column<string>(maxLength: 50, nullable: true),
                    IsIdentity = table.Column<int>(nullable: true),
                    Austate = table.Column<int>(nullable: true),
                    Sex = table.Column<int>(nullable: true),
                    GSName = table.Column<string>(maxLength: 50, nullable: true),
                    Legalperson = table.Column<string>(maxLength: 50, nullable: true),
                    Credit = table.Column<string>(maxLength: 50, nullable: true),
                    GSemail = table.Column<string>(maxLength: 50, nullable: true),
                    GSdizhi = table.Column<string>(maxLength: 50, nullable: true),
                    AuditName = table.Column<string>(maxLength: 50, nullable: true),
                    AuditTime = table.Column<DateTime>(nullable: true),
                    Taxpayer = table.Column<string>(maxLength: 50, nullable: true),
                    Standby1 = table.Column<string>(maxLength: 50, nullable: true),
                    Standby2 = table.Column<string>(maxLength: 50, nullable: true),
                    Idphoto = table.Column<string>(maxLength: 500000, nullable: true),
                    businesslicense = table.Column<string>(maxLength: 50000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authentication_GS", x => x.AuGSid);
                });

            migrationBuilder.CreateTable(
                name: "Authentication_MT",
                columns: table => new
                {
                    AuMTid = table.Column<string>(maxLength: 36, nullable: false),
                    HeadPortrait = table.Column<string>(maxLength: 500000, nullable: true),
                    ModelName = table.Column<string>(maxLength: 50, nullable: true),
                    PnoneNumber = table.Column<string>(maxLength: 50, nullable: true),
                    IdentityNumber = table.Column<string>(maxLength: 50, nullable: true),
                    IsIdentity = table.Column<int>(nullable: true),
                    Austate = table.Column<int>(nullable: true),
                    Sex = table.Column<int>(nullable: true),
                    zjZhaopian = table.Column<string>(maxLength: 500000, nullable: true),
                    Yingyezhizhao = table.Column<string>(maxLength: 500000, nullable: true),
                    AuditName = table.Column<string>(maxLength: 50, nullable: true),
                    AuditTime = table.Column<DateTime>(nullable: true),
                    Standby1 = table.Column<string>(maxLength: 50, nullable: true),
                    Standby2 = table.Column<string>(maxLength: 200, nullable: true),
                    Birthday = table.Column<DateTime>(nullable: true),
                    PresentAddress = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authentication_MT", x => x.AuMTid);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserId = table.Column<string>(maxLength: 50, nullable: false),
                    UserName = table.Column<string>(maxLength: 50, nullable: true),
                    Password = table.Column<string>(maxLength: 50, nullable: true),
                    Sex = table.Column<int>(nullable: true),
                    CurrentRole = table.Column<int>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: true),
                    HeadPortrait = table.Column<string>(maxLength: 1000, nullable: true),
                    IdentityNumber = table.Column<string>(maxLength: 50, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 50, nullable: true),
                    PresentAddress = table.Column<string>(maxLength: 50, nullable: true),
                    UsersRank = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activity");

            migrationBuilder.DropTable(
                name: "Activity_report");

            migrationBuilder.DropTable(
                name: "Activity_user");

            migrationBuilder.DropTable(
                name: "Activity_vote");

            migrationBuilder.DropTable(
                name: "Activity_zone");

            migrationBuilder.DropTable(
                name: "Authentication_GS");

            migrationBuilder.DropTable(
                name: "Authentication_MT");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
