﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class zhaohag1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Authentication_GS",
                columns: table => new
                {
                    AuGSid = table.Column<string>(maxLength: 35, nullable: false),
                    GSHeadPortraits = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    PnoneNumber = table.Column<string>(maxLength: 50, nullable: true),
                    IdentityNumber = table.Column<string>(maxLength: 50, nullable: true),
                    IsIdentity = table.Column<int>(maxLength: 50, nullable: true),
                    Austate = table.Column<int>(nullable: true),
                    Sex = table.Column<int>(nullable: true),
                    GSName = table.Column<string>(maxLength: 50, nullable: true),
                    Legalperson = table.Column<string>(maxLength: 50, nullable: true),
                    Credit = table.Column<string>(maxLength: 50, nullable: true),
                    GSemail = table.Column<string>(maxLength: 50, nullable: true),
                    GSdizhi = table.Column<string>(maxLength: 50, nullable: true),
                    AuditName = table.Column<string>(maxLength: 50, nullable: true),
                    AuditTime = table.Column<DateTime>(nullable: true),
                    Taxpayer = table.Column<string>(maxLength: 50, nullable: true),
                    Standby1 = table.Column<string>(maxLength: 50, nullable: true),
                    Standby2 = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authentication_GS", x => x.AuGSid);
                });

            migrationBuilder.CreateTable(
                name: "Authentication_MT",
                columns: table => new
                {
                    AuMTid = table.Column<string>(maxLength: 35, nullable: false),
                    HeadPortrait = table.Column<string>(maxLength: 50, nullable: true),
                    ModelName = table.Column<string>(maxLength: 50, nullable: true),
                    PnoneNumber = table.Column<string>(maxLength: 50, nullable: true),
                    IdentityNumber = table.Column<string>(maxLength: 50, nullable: true),
                    IsIdentity = table.Column<int>(nullable: true),
                    Austate = table.Column<int>(nullable: true),
                    Sex = table.Column<int>(nullable: true),
                    zjZhaopian = table.Column<string>(maxLength: 50, nullable: true),
                    Yingyezhizhao = table.Column<string>(maxLength: 50, nullable: true),
                    AuditName = table.Column<string>(maxLength: 50, nullable: true),
                    AuditTime = table.Column<DateTime>(nullable: true),
                    Standby1 = table.Column<string>(maxLength: 50, nullable: true),
                    Standby2 = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authentication_MT", x => x.AuMTid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Authentication_GS");

            migrationBuilder.DropTable(
                name: "Authentication_MT");
        }
    }
}
