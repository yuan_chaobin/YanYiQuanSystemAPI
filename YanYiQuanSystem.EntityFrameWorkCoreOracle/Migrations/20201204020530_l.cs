﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class l : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Attention",
                columns: table => new
                {
                    AttentionId = table.Column<string>(maxLength: 50, nullable: false),
                    UserID = table.Column<string>(nullable: true),
                    UserType = table.Column<int>(nullable: false),
                    FollowerID = table.Column<string>(nullable: true),
                    FollowerType = table.Column<int>(nullable: false),
                    AttentionTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attention", x => x.AttentionId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Attention");
        }
    }
}
