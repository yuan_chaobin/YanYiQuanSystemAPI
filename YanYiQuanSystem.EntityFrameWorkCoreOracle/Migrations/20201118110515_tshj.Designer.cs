﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Oracle.EntityFrameworkCore.Metadata;
using YanYiQuanSystem.EntityFrameWorkCoreOracle;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    [DbContext(typeof(YanYiQuanSystemDbContext))]
    [Migration("20201118110515_tshj")]
    partial class tshj
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn)
                .HasAnnotation("ProductVersion", "3.1.9")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            modelBuilder.Entity("YanYiQuanSystem.Domain.Activity", b =>
                {
                    b.Property<string>("Uuid")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<int>("Category_activity")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("Comments")
                        .HasColumnType("NVARCHAR2(2000)");

                    b.Property<int>("Count_max")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("Desc_activity")
                        .HasColumnType("NVARCHAR2(2000)");

                    b.Property<string>("Rule_activity")
                        .HasColumnType("NVARCHAR2(2000)");

                    b.Property<string>("Time_from_baoming")
                        .HasColumnType("NVARCHAR2(2000)");

                    b.Property<string>("Time_from_xuanba")
                        .HasColumnType("NVARCHAR2(2000)");

                    b.Property<string>("Time_to_baoming")
                        .HasColumnType("NVARCHAR2(2000)");

                    b.Property<string>("Time_to_xuanba")
                        .HasColumnType("NVARCHAR2(2000)");

                    b.Property<string>("Url_pic_activity")
                        .HasColumnType("NVARCHAR2(2000)");

                    b.HasKey("Uuid");

                    b.ToTable("Activity");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.Activity_report", b =>
                {
                    b.Property<string>("Uuid")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Id_activity")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Id_user")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Id_user_report_by")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Report_content")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<DateTime>("Time_trans")
                        .HasColumnType("TIMESTAMP(7)")
                        .HasMaxLength(32);

                    b.HasKey("Uuid");

                    b.ToTable("Activity_report");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.Activity_user", b =>
                {
                    b.Property<string>("Uuid")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Activity_zone_name")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Blood_type")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Comments")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<DateTime>("Create_time")
                        .HasColumnType("TIMESTAMP(7)");

                    b.Property<string>("Date_birthday")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<int>("Gender")
                        .HasColumnType("NUMBER(10)");

                    b.Property<int>("Height")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("Id_activity")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Id_user")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Idcard_user")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<int>("Is_allow_index")
                        .HasColumnType("NUMBER(10)");

                    b.Property<int>("Is_mainland")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("Name_user")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Nickname")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Phone_user")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<int>("Piaoshu")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("Qq")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Region")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<int>("Shenhe_status")
                        .HasColumnType("NUMBER(10)");

                    b.Property<decimal>("Tunwei")
                        .HasColumnType("DECIMAL(18, 2)");

                    b.Property<string>("Url_homecover")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Url_pic_head")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Weibo")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<decimal>("Weight")
                        .HasColumnType("DECIMAL(18, 2)");

                    b.Property<int>("Xiema")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("Xingzuo")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<decimal>("Xiongwei")
                        .HasColumnType("DECIMAL(18, 2)");

                    b.Property<decimal>("Yaowei")
                        .HasColumnType("DECIMAL(18, 2)");

                    b.HasKey("Uuid");

                    b.ToTable("Activity_user");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.Activity_vote", b =>
                {
                    b.Property<string>("Uuid")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Id_activity")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Id_user")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Id_user_dianzan")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<DateTime>("Time_dianzan")
                        .HasColumnType("TIMESTAMP(7)");

                    b.HasKey("Uuid");

                    b.ToTable("Activity_vote");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.Activity_zone", b =>
                {
                    b.Property<string>("Uuid")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Comments")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Id_activity")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<int>("Order_num")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("Zone_name")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.HasKey("Uuid");

                    b.ToTable("Activity_zone");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.Emp", b =>
                {
                    b.Property<string>("EmpId")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("EmpName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("IdentityCard")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Phone")
                        .HasColumnType("NVARCHAR2(11)")
                        .HasMaxLength(11);

                    b.Property<string>("RoleId")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("UserName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("UserPass")
                        .HasColumnType("NVARCHAR2(20)")
                        .HasMaxLength(20);

                    b.HasKey("EmpId");

                    b.ToTable("Emp");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.MenuSystem", b =>
                {
                    b.Property<string>("MenuID")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreateTime")
                        .HasColumnType("TIMESTAMP(7)");

                    b.Property<int>("Is_delete")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("MenuName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<int>("MenuPid")
                        .HasColumnType("NUMBER(10)");

                    b.Property<string>("MenuUrl")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<DateTime>("ModifyTime")
                        .HasColumnType("TIMESTAMP(7)");

                    b.HasKey("MenuID");

                    b.ToTable("MenuSystem");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.Role", b =>
                {
                    b.Property<string>("RoleId")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Remarks")
                        .HasColumnType("NVARCHAR2(100)")
                        .HasMaxLength(100);

                    b.Property<string>("RoleName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.HasKey("RoleId");

                    b.ToTable("Role");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.SensibilityManager", b =>
                {
                    b.Property<string>("SensibilityManagerID")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("SensibilityManagerName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.HasKey("SensibilityManagerID");

                    b.ToTable("SensibilityManager");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.UsefulExpressionsManager", b =>
                {
                    b.Property<string>("UsefulExpressionsManagerID")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("UsefulExpressionsManagerName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.HasKey("UsefulExpressionsManagerID");

                    b.ToTable("UsefulExpressionsManager");
                });

            modelBuilder.Entity("YanYiQuanSystem.Domain.Users", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("NVARCHAR2(32)")
                        .HasMaxLength(32);

                    b.Property<string>("Password")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.Property<string>("UserName")
                        .HasColumnType("NVARCHAR2(50)")
                        .HasMaxLength(50);

                    b.HasKey("UserId");

                    b.ToTable("Users");
                });
#pragma warning restore 612, 618
        }
    }
}
