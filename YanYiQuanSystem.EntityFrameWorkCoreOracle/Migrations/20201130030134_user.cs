﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class user : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BannedTime",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Communication",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "InviteClick",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IsBanned",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LoginClick",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PositionInviteClick",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RingUpClick",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BeAttentionClick",
                table: "ModelsLibrary",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BeRelationClick",
                table: "ModelsLibrary",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DialInviteClick",
                table: "ModelsLibrary",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BannedTime",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Communication",
                table: "User");

            migrationBuilder.DropColumn(
                name: "InviteClick",
                table: "User");

            migrationBuilder.DropColumn(
                name: "IsBanned",
                table: "User");

            migrationBuilder.DropColumn(
                name: "LoginClick",
                table: "User");

            migrationBuilder.DropColumn(
                name: "PositionInviteClick",
                table: "User");

            migrationBuilder.DropColumn(
                name: "RingUpClick",
                table: "User");

            migrationBuilder.DropColumn(
                name: "BeAttentionClick",
                table: "ModelsLibrary");

            migrationBuilder.DropColumn(
                name: "BeRelationClick",
                table: "ModelsLibrary");

            migrationBuilder.DropColumn(
                name: "DialInviteClick",
                table: "ModelsLibrary");
        }
    }
}
