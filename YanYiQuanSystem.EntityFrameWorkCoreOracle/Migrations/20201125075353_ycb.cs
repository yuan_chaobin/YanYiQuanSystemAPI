﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class ycb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PhotoAlbum",
                columns: table => new
                {
                    PhotoId = table.Column<string>(maxLength: 50, nullable: false),
                    UserID = table.Column<string>(maxLength: 50, nullable: true),
                    HeadPortrait = table.Column<string>(maxLength: 1000, nullable: true),
                    AuditStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoAlbum", x => x.PhotoId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhotoAlbum");
        }
    }
}
