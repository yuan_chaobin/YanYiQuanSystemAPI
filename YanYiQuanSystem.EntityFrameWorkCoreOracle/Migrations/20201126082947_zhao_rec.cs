﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class zhao_rec : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RecruitmentLibrary",
                columns: table => new
                {
                    RecLibraryId = table.Column<string>(maxLength: 50, nullable: false),
                    CompanyName = table.Column<string>(maxLength: 50, nullable: true),
                    PostName = table.Column<string>(maxLength: 50, nullable: true),
                    AuditStatus = table.Column<int>(nullable: true),
                    IsValid = table.Column<int>(nullable: true),
                    SalaryType = table.Column<int>(nullable: true),
                    SalaryScope = table.Column<string>(maxLength: 50, nullable: true),
                    MinSalary = table.Column<int>(nullable: true),
                    MaxSalary = table.Column<int>(nullable: true),
                    WorkType = table.Column<int>(nullable: true),
                    PostSex = table.Column<int>(nullable: true),
                    Height = table.Column<string>(maxLength: 50, nullable: true),
                    Education = table.Column<int>(nullable: true),
                    WorkYears = table.Column<string>(maxLength: 50, nullable: true),
                    WorkPlace = table.Column<string>(maxLength: 200, nullable: true),
                    RecPeoples = table.Column<int>(nullable: true),
                    PostTag = table.Column<int>(nullable: true),
                    PostRemarks = table.Column<string>(maxLength: 12000, nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: true),
                    EndTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecruitmentLibrary", x => x.RecLibraryId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RecruitmentLibrary");
        }
    }
}
