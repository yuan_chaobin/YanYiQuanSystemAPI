﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class menu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MenuSystem",
                columns: table => new
                {
                    MenuID = table.Column<string>(maxLength: 32, nullable: false),
                    MenuName = table.Column<string>(maxLength: 50, nullable: true),
                    MenuUrl = table.Column<string>(maxLength: 50, nullable: true),
                    MenuPid = table.Column<int>(nullable: false),
                    Is_delete = table.Column<int>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    ModifyTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuSystem", x => x.MenuID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MenuSystem");
        }
    }
}
