﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class ery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.CreateTable(
                name: "ModelsLibrary",
                columns: table => new
                {
                    ModelId = table.Column<string>(maxLength: 50, nullable: false),
                    NickName = table.Column<string>(maxLength: 50, nullable: true),
                    UserID = table.Column<string>(maxLength: 50, nullable: true),
                    IsAssistant = table.Column<int>(nullable: false),
                    RegisterTime = table.Column<DateTime>(nullable: false),
                    InvitationCode = table.Column<string>(maxLength: 50, nullable: true),
                    MyCode = table.Column<string>(maxLength: 50, nullable: true),
                    Constellation = table.Column<string>(maxLength: 20, nullable: true),
                    Height = table.Column<int>(nullable: false),
                    Weight = table.Column<int>(nullable: false),
                    Size = table.Column<int>(nullable: false),
                    TheChest = table.Column<int>(nullable: false),
                    Waistline = table.Column<int>(nullable: false),
                    Hipline = table.Column<int>(nullable: false),
                    BloodType = table.Column<string>(maxLength: 20, nullable: true),
                    IsActivity = table.Column<int>(nullable: false),
                    ActivityId = table.Column<int>(nullable: false),
                    LoginAccount = table.Column<string>(maxLength: 50, nullable: true),
                    IsLogging1 = table.Column<int>(nullable: false),
                    ApproveStatus = table.Column<int>(nullable: false),
                    PhotoStatus = table.Column<int>(nullable: false),
                    LoggingStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModelsLibrary", x => x.ModelId);
                });

            migrationBuilder.CreateTable(
                name: "RecruitmentCompanys",
                columns: table => new
                {
                    HrId = table.Column<string>(maxLength: 50, nullable: false),
                    UsersID = table.Column<string>(maxLength: 50, nullable: true),
                    InvitationCode = table.Column<string>(maxLength: 100, nullable: true),
                    MyCode = table.Column<string>(maxLength: 50, nullable: true),
                    CompanyType = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(maxLength: 50, nullable: true),
                    CompanyPhone = table.Column<string>(maxLength: 50, nullable: true),
                    CompanyPerson = table.Column<string>(maxLength: 50, nullable: true),
                    CompanyAddress = table.Column<string>(maxLength: 100, nullable: true),
                    TaxpayerNum = table.Column<string>(maxLength: 50, nullable: true),
                    WorkPosition = table.Column<string>(maxLength: 20, nullable: true),
                    ApproveState = table.Column<int>(nullable: false),
                    LoggingStatus = table.Column<int>(nullable: false),
                    RegisterTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecruitmentCompanys", x => x.HrId);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserId = table.Column<string>(maxLength: 50, nullable: false),
                    UserName = table.Column<string>(maxLength: 50, nullable: true),
                    Password = table.Column<string>(maxLength: 50, nullable: true),
                    Sex = table.Column<int>(nullable: false),
                    CurrentRole = table.Column<int>(nullable: false),
                    Birthday = table.Column<DateTime>(nullable: false),
                    HeadPortrait = table.Column<string>(maxLength: 200, nullable: true),
                    IdentityNumber = table.Column<string>(maxLength: 50, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 50, nullable: true),
                    PresentAddress = table.Column<string>(maxLength: 50, nullable: true),
                    UsersRank = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropTable(
                name: "ModelsLibrary");

            migrationBuilder.DropTable(
                name: "RecruitmentCompanys");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
