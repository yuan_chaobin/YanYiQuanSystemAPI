﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class oo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IsFirstPage",
                table: "ModelsLibrary",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Uuid",
                table: "Activity",
                maxLength: 36,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "NVARCHAR2(450)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFirstPage",
                table: "ModelsLibrary");

            migrationBuilder.AlterColumn<string>(
                name: "Uuid",
                table: "Activity",
                type: "NVARCHAR2(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 36);
        }
    }
}
