﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class zhaoha : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    Uuid = table.Column<string>(nullable: false),
                    Desc_activity = table.Column<string>(nullable: true),
                    Time_from_baoming = table.Column<string>(nullable: true),
                    Time_to_baoming = table.Column<string>(nullable: true),
                    Time_from_xuanba = table.Column<string>(nullable: true),
                    Time_to_xuanba = table.Column<string>(nullable: true),
                    Count_max = table.Column<int>(nullable: false),
                    Category_activity = table.Column<int>(nullable: false),
                    Comments = table.Column<string>(nullable: true),
                    Url_pic_activity = table.Column<string>(nullable: true),
                    Rule_activity = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.Uuid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activity");
        }
    }
}
