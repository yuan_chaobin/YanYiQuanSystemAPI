﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class quanyi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuanYiWeiHu",
                columns: table => new
                {
                    ProductId = table.Column<string>(maxLength: 50, nullable: false),
                    ProductName = table.Column<string>(maxLength: 50, nullable: true),
                    ActualAmount = table.Column<string>(maxLength: 50, nullable: true),
                    Originalamount = table.Column<string>(maxLength: 50, nullable: true),
                    DayNumber = table.Column<int>(nullable: true),
                    IsState = table.Column<int>(nullable: true),
                    Remark = table.Column<string>(maxLength: 100, nullable: true),
                    OrderById = table.Column<int>(nullable: false),
                    UserRightsId = table.Column<string>(maxLength: 50, nullable: true),
                    GiveAsDay = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuanYiWeiHu", x => x.ProductId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuanYiWeiHu");
        }
    }
}
