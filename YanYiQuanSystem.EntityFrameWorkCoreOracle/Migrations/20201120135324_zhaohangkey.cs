﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class zhaohangkey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AuMTid",
                table: "Authentication_MT",
                maxLength: 36,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "NVARCHAR2(35)",
                oldMaxLength: 35);

            migrationBuilder.AlterColumn<string>(
                name: "AuGSid",
                table: "Authentication_GS",
                maxLength: 36,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "NVARCHAR2(35)",
                oldMaxLength: 35);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AuMTid",
                table: "Authentication_MT",
                type: "NVARCHAR2(35)",
                maxLength: 35,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 36);

            migrationBuilder.AlterColumn<string>(
                name: "AuGSid",
                table: "Authentication_GS",
                type: "NVARCHAR2(35)",
                maxLength: 35,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 36);
        }
    }
}
