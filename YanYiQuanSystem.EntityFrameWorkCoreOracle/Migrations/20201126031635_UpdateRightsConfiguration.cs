﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class UpdateRightsConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Number",
                table: "RightsConfiguration",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "vule1",
                table: "RightsConfiguration",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "vule2",
                table: "RightsConfiguration",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Number",
                table: "RightsConfiguration");

            migrationBuilder.DropColumn(
                name: "vule1",
                table: "RightsConfiguration");

            migrationBuilder.DropColumn(
                name: "vule2",
                table: "RightsConfiguration");
        }
    }
}
