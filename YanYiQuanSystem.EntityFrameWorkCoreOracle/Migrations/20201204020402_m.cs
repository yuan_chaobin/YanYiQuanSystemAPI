﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class m : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Education",
                table: "ModelsLibrary",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HonorThat",
                table: "ModelsLibrary",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Education",
                table: "ModelsLibrary");

            migrationBuilder.DropColumn(
                name: "HonorThat",
                table: "ModelsLibrary");
        }
    }
}
