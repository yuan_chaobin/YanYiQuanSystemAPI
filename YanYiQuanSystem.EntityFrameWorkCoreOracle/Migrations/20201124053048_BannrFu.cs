﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class BannrFu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Banner",
                columns: table => new
                {
                    BannerId = table.Column<string>(maxLength: 50, nullable: false),
                    AdvertisingPlanning = table.Column<string>(maxLength: 100, nullable: true),
                    ActiveTime = table.Column<DateTime>(nullable: false),
                    Sort = table.Column<int>(nullable: true),
                    ToShare = table.Column<int>(nullable: true),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    Images = table.Column<string>(maxLength: 50, nullable: true),
                    Uuid = table.Column<string>(maxLength: 50, nullable: true),
                    ExpirationTime = table.Column<DateTime>(nullable: false),
                    ShareURL = table.Column<string>(maxLength: 100, nullable: true),
                    Content = table.Column<string>(maxLength: 100, nullable: true),
                    MobileURL = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banner", x => x.BannerId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Banner");
        }
    }
}
