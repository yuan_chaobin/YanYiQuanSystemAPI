﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YanYiQuanSystem.EntityFrameWorkCoreOracle.Migrations
{
    public partial class tshj : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.CreateTable(
                name: "Activity_report",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 32, nullable: false),
                    Id_activity = table.Column<string>(maxLength: 32, nullable: true),
                    Id_user = table.Column<string>(maxLength: 32, nullable: true),
                    Id_user_report_by = table.Column<string>(maxLength: 32, nullable: true),
                    Report_content = table.Column<string>(maxLength: 32, nullable: true),
                    Time_trans = table.Column<DateTime>(maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_report", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Activity_user",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 32, nullable: false),
                    Id_activity = table.Column<string>(maxLength: 32, nullable: true),
                    Id_user = table.Column<string>(maxLength: 32, nullable: true),
                    Activity_zone_name = table.Column<string>(maxLength: 32, nullable: true),
                    Name_user = table.Column<string>(maxLength: 32, nullable: true),
                    Phone_user = table.Column<string>(maxLength: 32, nullable: true),
                    Nickname = table.Column<string>(maxLength: 32, nullable: true),
                    Idcard_user = table.Column<string>(maxLength: 32, nullable: true),
                    Url_pic_head = table.Column<string>(maxLength: 32, nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Blood_type = table.Column<string>(maxLength: 32, nullable: true),
                    Height = table.Column<int>(nullable: false),
                    Weight = table.Column<decimal>(nullable: false),
                    Date_birthday = table.Column<string>(maxLength: 32, nullable: true),
                    Xingzuo = table.Column<string>(maxLength: 32, nullable: true),
                    Region = table.Column<string>(maxLength: 32, nullable: true),
                    Xiema = table.Column<int>(nullable: false),
                    Yaowei = table.Column<decimal>(nullable: false),
                    Xiongwei = table.Column<decimal>(nullable: false),
                    Tunwei = table.Column<decimal>(nullable: false),
                    Weibo = table.Column<string>(maxLength: 32, nullable: true),
                    Qq = table.Column<string>(maxLength: 32, nullable: true),
                    Comments = table.Column<string>(maxLength: 32, nullable: true),
                    Shenhe_status = table.Column<int>(nullable: false),
                    Is_mainland = table.Column<int>(nullable: false),
                    Is_allow_index = table.Column<int>(nullable: false),
                    Create_time = table.Column<DateTime>(nullable: false),
                    Url_homecover = table.Column<string>(maxLength: 32, nullable: true),
                    Piaoshu = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_user", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Activity_vote",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 32, nullable: false),
                    Id_activity = table.Column<string>(maxLength: 32, nullable: true),
                    Id_user = table.Column<string>(maxLength: 32, nullable: true),
                    Id_user_dianzan = table.Column<string>(maxLength: 32, nullable: true),
                    Time_dianzan = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_vote", x => x.Uuid);
                });

            migrationBuilder.CreateTable(
                name: "Activity_zone",
                columns: table => new
                {
                    Uuid = table.Column<string>(maxLength: 32, nullable: false),
                    Id_activity = table.Column<string>(maxLength: 32, nullable: true),
                    Zone_name = table.Column<string>(maxLength: 32, nullable: true),
                    Comments = table.Column<string>(maxLength: 32, nullable: true),
                    Order_num = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_zone", x => x.Uuid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activity_report");

            migrationBuilder.DropTable(
                name: "Activity_user");

            migrationBuilder.DropTable(
                name: "Activity_vote");

            migrationBuilder.DropTable(
                name: "Activity_zone");

        }
    }
}
