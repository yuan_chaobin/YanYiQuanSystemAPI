﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.RedisHelper
{
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using StackExchange.Redis;
    using System.Linq;

    /// <summary>
    /// Redis库连接 帮助类
    /// </summary>
    public class RedisDbHelper
    {

        private static IDatabase Database;
        private static ConnectionMultiplexer multiplexer;

        //static RedisDbHelper()
        //{
        //    //创建连接通道
        //    ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        //    IConfigurationBuilder builder = configurationBuilder.AddJsonFile("appsettings.json");
        //    IConfiguration configuration = builder.Build();
        //    //连接Redis数据库连接字符串
        //    multiplexer = ConnectionMultiplexer.Connect(configuration.GetSection("RedisConnection").Value);
        //    //连接到第一个数据库
        //    Database = multiplexer.GetDatabase(0);
        //}

        /// <summary>
        /// 通过Key数据库取值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Key"></param>
        /// <returns></returns>
        public List<T> GetList<T>(string Key)
        {
            if (Database.KeyExists(Key))
            {
                //如果存在这个数据库,则获取到这个Key的数据,再反序列化成为对象
                string getValue = Database.StringGet(Key);
                return JsonConvert.DeserializeObject<List<T>>(getValue);
            }
            //不存在返回空
            return null;
        }

        /// <summary>
        /// 通过Key键批量添加数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool BatchAdd<T>(List<T> entity, string Key)
        {
            //判断Key是否存在
            if (Database.KeyExists(Key))
            {
                //如果Key数据库存在则把已有数据追加到entity
                string getValue = Database.StringGet(Key);
                List<T> list = JsonConvert.DeserializeObject<List<T>>(getValue);
                list.AddRange(entity);

                //再删除原来的Key,最后添加数据
                Database.KeyDelete(Key);
                var getResult = JsonConvert.SerializeObject(list);
                long getCount = Database.StringAppend(Key, getResult);
                return getCount > 0 ? true : false;
            }
            else
            {
                //不存在则直接添加entity
                var getResult = JsonConvert.SerializeObject(entity);
                long getCount = Database.StringAppend(Key, getResult);
                return getCount > 0 ? true : false;
            }
        }

        /// <summary>
        /// 单个存值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetKey(string key, string value)
        {
            return Database.StringSet(key, value);
        }
    }
}
