﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;
using YanYiQuanSystem.IRepository;
using YanYiQuanSystem.IService;

namespace YanYiQuanSystem.Service
{
    public class AttentionService : IAttentionService
    {
        private IAttentionRepository AttentionRepository { get; set; }
        public AttentionService(IAttentionRepository attentionRepository)
        {
            this.AttentionRepository = attentionRepository;
        }

        public Task<bool> AddData(Attention entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchAddData(List<Attention> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Attention>> GetListAsync()
        {
            return await AttentionRepository.GetListAsync();
        }

        public Task<List<Attention>> GetListByWhereAsync(Expression<Func<Attention, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(Attention entity)
        {
            throw new NotImplementedException();
        }
    }
}
