﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.IRepository;
    using System.Linq.Expressions;

     public class LabelService:ILabelService
    {
        public ILabelRepository labelRepository { get; set; }
        public LabelService(ILabelRepository labelRepository)
        {
            this.labelRepository = labelRepository;
        }

        public Task<List<Label>> GetLabels()
        {
            return labelRepository.GetListAsync();
        }

        public Task<bool> AddLabel(Label label)
        {
            return labelRepository.AddData(label);
        }

        public Task<bool> DeleteLabel(string Id)
        {
            return labelRepository.DeleteData(Id);
        }
    }
}
