﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;

    public class AssistantService : IAssistantService
    {
        private IAssistantRepository AssistantRepository { get; set; }
        public AssistantService(IAssistantRepository assistantRepository)
        {
            this.AssistantRepository = assistantRepository;
        }

        public async Task<bool> AddData(Assistant entity)
        {
            return await AssistantRepository.AddData(entity);
        }

        public Task<bool> BatchAddData(List<Assistant> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            return await AssistantRepository.DeleteData(Id);
        }

        public async Task<List<Assistant>> GetListAsync()
        {
            return await AssistantRepository.GetListAsync();
        }

        public Task<bool> UpdateData(Assistant entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateAssistant(CommonData entity)
        {
            return await AssistantRepository.UpdateAssistant(entity);
        }

        public async Task<List<Assistant>> GetListByWhereAsync(Expression<Func<Assistant, bool>> whereExpression)
        {
            return await AssistantRepository.GetListByWhereAsync(whereExpression);
        }
    }
}
