﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;
using YanYiQuanSystem.IRepository;
using YanYiQuanSystem.IService;

namespace YanYiQuanSystem.Service
{
    public class PurchaseRecordService : IPurchaseRecordService
    {
        private IPurchaseRecordRepository PurchaseRecordRepository { get; set; }
        public PurchaseRecordService(IPurchaseRecordRepository recordRepository)
        {
            this.PurchaseRecordRepository = recordRepository;
        }

        public Task<bool> AddData(PurchaseRecord entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchAddData(List<PurchaseRecord> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<PurchaseRecord>> GetListAsync()
        {
            return await PurchaseRecordRepository.GetListAsync();
        }

        public Task<List<PurchaseRecord>> GetListByWhereAsync(Expression<Func<PurchaseRecord, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(PurchaseRecord entity)
        {
            throw new NotImplementedException();
        }
    }
}
