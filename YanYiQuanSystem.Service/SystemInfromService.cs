﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
   public class SystemInfromService:ISystemInfromService
    {
        public ISystemInfromRespository systemInfromRespository { get; set; }
        public SystemInfromService(ISystemInfromRespository systemInfromRespository)
        {
            this.systemInfromRespository = systemInfromRespository;
        }

        public async Task<bool> AddSystemInfrom(SystemInfrom systemInfrom)
        {
            return await systemInfromRespository.AddData(systemInfrom);
        }

        public async Task<bool> UpdateSystemInfrom(SystemInfrom systemInfrom)
        {
            return await systemInfromRespository.UpdateData(systemInfrom);
        }

        public async Task<bool> DeleteSystemInfrom(string id)
        {
            return await systemInfromRespository.DeleteData(id);
        }

        public async Task<List<SystemInfrom>> GetSystemInfromBywere(Expression<Func<SystemInfrom, bool>> whereExpression)
        {
            return await systemInfromRespository.GetListByWhereAsync(whereExpression);
        }

        public async Task<List<SystemInfrom>> GetSystemInfrom()
        {
            return await systemInfromRespository.GetListAsync();
        }
    }
}
