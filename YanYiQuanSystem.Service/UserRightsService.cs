﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.IRepository;
    using System.Threading.Tasks;
    using System.Linq.Expressions;

    public class UserRightsService : IUserRightsService
    {
        public IUserRightsRepository userRightsRepository;
        public UserRightsService(IUserRightsRepository userRightsRepository)
        {
            this.userRightsRepository = userRightsRepository;
        }

        public Task<bool> AddUserRightsManager(UserRights userRights)
        {
            return userRightsRepository.AddData(userRights);
        }

        public Task<bool> DeleteUserRightsManager(string Id)
        {
            return userRightsRepository.DeleteData(Id);
        }


        public async Task<List<UserRights>> GetSensibilityManagerWhereAsync(Expression<Func<UserRights, bool>> whereExpression)
        {
            return await userRightsRepository.GetListByWhereAsync(whereExpression);
        }

        public async Task<List<UserRights>> GetUserRights()
        {
            return await userRightsRepository.GetListAsync();
        }

        public Task<bool> UpdateUserRightsManager(UserRights userRights)
        {
            return userRightsRepository.UpdateData(userRights);
        }
    }
}
