﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;

    /// <summary>
    /// 模特列表服务层
    /// </summary>
    public class ModelLibraryService : IModelLibraryService
    {
        /// <summary>
        /// 注入模特列表仓储层接口
        /// </summary>
        private IModelLibraryRepository ModelLibraryRepository { get; set; }
        public ModelLibraryService(IModelLibraryRepository modelLibraryRepository)
        {
            this.ModelLibraryRepository = modelLibraryRepository;
        }

        public async Task<bool> AddData(ModelsLibrary entity)
        {
            return await ModelLibraryRepository.AddData(entity);
        }

        public Task<bool> BatchAddData(List<ModelsLibrary> entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> BatchDelete(List<string> Ids)
        {
            return await ModelLibraryRepository.BatchDelete(Ids);
        }

        public async Task<bool> DeleteData(string Id)
        {
            return await ModelLibraryRepository.DeleteData(Id);
        }

        public async Task<List<ModelsLibrary>> GetListAsync()
        {
            return await ModelLibraryRepository.GetListAsync();
        }

        public Task<List<ModelsLibrary>> GetListByPage<TKey>(Expression<Func<ModelsLibrary, bool>> whereExpression, Expression<Func<ModelsLibrary, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<ModelsLibrary>> GetListByWhereAsync(Expression<Func<ModelsLibrary, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateData(ModelsLibrary entity)
        {
            return await ModelLibraryRepository.UpdateData(entity);
        }

        /// <summary>
        /// 根据ID修改是否在首页状态
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateFirstPage(List<string> Ids)
        {
            return await ModelLibraryRepository.UpdateFirstPage(Ids);
        }

        public async Task<bool> UpdateUserModel(CommonData entity)
        {
            return await ModelLibraryRepository.UpdateUserModel(entity);
        }

        public async Task<bool> UpdateUserRank(User user)
        {
            return await ModelLibraryRepository.UpdateUserRank(user);
        }

        public async Task<bool> UpdateAuditStatus(string photoId)
        {
            return await ModelLibraryRepository.UpdateAuditStatus(photoId);
        }

        public async Task<bool> UpdateAduitStatus(string photoId)
        {
            return await ModelLibraryRepository.UpdateAuditStatus(photoId);
        }

        public Task<List<ModelsLibrary>> GetEmpWhereAsync(Expression<Func<ModelsLibrary, bool>> whereExpression)
        {
            return ModelLibraryRepository.GetListByWhereAsync(whereExpression);
        }

        public async Task<bool> UpdateFirst(ModelsLibrary modelsLibrary)
        {
            return await ModelLibraryRepository.UpdateFirst(modelsLibrary);
        }
    }
}
