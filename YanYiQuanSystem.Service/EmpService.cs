﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    using System.Linq;
    public class EmpService : IEmpService
    {

        /// <summary>
        /// 注入用户仓储层接口
        /// </summary>
        private IEmpRepository EmpRepository { get; set; }
        public EmpService(IEmpRepository EmpRepository)
        {
            this.EmpRepository = EmpRepository;
        }

        public Task<List<Emp>> GetEmpAsync()
        {
            return EmpRepository.GetListAsync();
        }

        public Task<bool> AddEmp(Emp entity)
        {
            return EmpRepository.AddData(entity);
        }

        public Task<bool> BatchAddEmp(List<Emp> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteEmp(string Id)
        {
            return EmpRepository.DeleteData(Id);
        }

        public Task<bool> BatchDeleteEmp(List<Emp> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateEmp(Emp entity)
        {
            return EmpRepository.UpdateData(entity);
        }

        public Task<List<Emp>> GetEmpWhereAsync(Expression<Func<Emp, bool>> whereExpression)
        {
            return EmpRepository.GetListByWhereAsync(whereExpression);
        }

        public Task<List<Emp>> GetListByPage(Expression<Func<Emp, bool>> whereExpression, Expression<Func<Emp>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
