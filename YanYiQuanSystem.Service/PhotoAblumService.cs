﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;

    public class PhotoAblumService : IPhotoAblumService
    {
        /// <summary>
        /// 注入相册库仓储层接口
        /// </summary>
        private IPhotoAlbumRepository PhotoAlbumRepository { get; set; }
        public PhotoAblumService(IPhotoAlbumRepository photoAlbumRepository)
        {
            this.PhotoAlbumRepository = photoAlbumRepository;
        }

        public async Task<List<PhotoAlbums>> GetListAsync()
        {
            return await PhotoAlbumRepository.GetListAsync();
        }

        public Task<bool> UpdateData(PhotoAlbums entity)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 批量删除个人照片
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async  Task<bool> DeleteData(string Id)
        {
           return await PhotoAlbumRepository.DeleteData(Id);
        }
        /// <summary>
        /// 批量添加相册
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> BatchAddData(List<PhotoAlbums> entity)
        {
            return await PhotoAlbumRepository.BatchAddData(entity);
        }
    }
}
