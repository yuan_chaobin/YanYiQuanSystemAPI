﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    using System.Linq;
    public class Role_MenuService : IRole_MenuService
    {
        /// <summary>
        /// 注入用户仓储层接口
        /// </summary>
        private IRole_MenuRepository role_MenuRepository { get; set; }
        public Role_MenuService(IRole_MenuRepository role_MenuRepository)
        {
            this.role_MenuRepository = role_MenuRepository;
        }

        public Task<bool> AddEmp(Role_Menu entity)
        {
            return role_MenuRepository.AddData(entity);
        }

        public Task<List<Role_Menu>> GetRole_MenuAsync()
        {
            return role_MenuRepository.GetListAsync();
        }

        public Task<List<Role_Menu>> GetRole_MenuWhereAsync(Expression<Func<Role_Menu, bool>> whereExpression)
        {
            return role_MenuRepository.GetListByWhereAsync(whereExpression);
        }

        public Task<bool> DeleteRole_Menu(string Id)
        {
            return role_MenuRepository.DeleteData(Id);
        }

        public Task<List<Role_Menu>> GetRole_MenuWhereMenuId(Expression<Func<Role_Menu, bool>> whereExpression)
        {
            return role_MenuRepository.GetListByWhereAsync(whereExpression);
        }

        public Task<bool> BatchAddData(List<Role_Menu> entity)
        {
            return role_MenuRepository.BatchAddData(entity);
        }
    }
}
