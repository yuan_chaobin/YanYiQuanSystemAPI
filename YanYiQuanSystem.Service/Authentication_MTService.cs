﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.CommonUnitity;

    public class Authentication_MTService : IAuthentication_MTIService
    {
        public IMT_Repository Repository { get; set; }
        public Authentication_MTService(IMT_Repository Repository)
        {
            this.Repository = Repository;
        }


        public async Task<bool> AddData(Authentication_MT entity)
        {
            return await Repository.AddData(entity);
        }

        public Task<bool> BatchAddData(List<Authentication_MT> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            return Repository.DeleteData(Id);
        }

        public Task<List<Authentication_MT>> GetListAsync()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 显示数据
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public  Task<List<Authentication_MT>> GetListByPage<TKey>(Expression<Func<Authentication_MT, bool>> whereExpression, Expression<Func<Authentication_MT, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return  Repository.GetListByPage(whereExpression,orderExpression,pageIndex,pageSize);
        }
        public Task<List<Authentication_MT>> GetListByWhereAsync(Expression<Func<Authentication_MT, bool>> whereExpression)
        {
            return Repository.GetListByWhereAsync(whereExpression);
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateData(Authentication_MT entity)
        {
            return await Repository.UpdateData(entity);
        }
        /// <summary>
        /// 修改模特的基本信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> wxUpdateUserModel(CommonData entity)
        {
            return await Repository.wxUpdateUserModel(entity);
        }
        /// <summary>
        /// 修改用户的头像
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> wxExitorImg(string id, string img )
        {
            return await Repository.wxExitorImg( id, img);
        }
    }
}
