﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;

    public class ReCompanyService : IReCompanyService
    {
        /// <summary>
        /// 注入招聘公司表仓储层接口
        /// </summary>
        private IReCompanyRepository ReCompanyRepository { get; set; }
        public ReCompanyService(IReCompanyRepository reCompanyRepository)
        {
            this.ReCompanyRepository = reCompanyRepository;
        }

        public async Task<bool> AddData(RecruitmentCompanys entity)
        {
            return await ReCompanyRepository.AddData(entity);
        }

        public Task<bool> BatchAddData(List<RecruitmentCompanys> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            return await ReCompanyRepository.DeleteData(Id);
        }

        public async Task<List<RecruitmentCompanys>> GetListAsync()
        {
            return await ReCompanyRepository.GetListAsync();
        }

        public Task<List<RecruitmentCompanys>> GetListByPage<TKey>(Expression<Func<RecruitmentCompanys, bool>> whereExpression, Expression<Func<RecruitmentCompanys, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<RecruitmentCompanys>> GetListByWhereAsync(Expression<Func<RecruitmentCompanys, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(RecruitmentCompanys entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateCompany(CommonData entity)
        {
            return await ReCompanyRepository.UpdateCompany(entity);
        }
    }
}
