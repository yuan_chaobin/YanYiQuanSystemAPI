﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;

    /// <summary>
    /// 用户服务层
    /// </summary>
    public class UsersService : IUsersService
    {
        /// <summary>
        /// 注入用户仓储层接口
        /// </summary>
        private IUsersRepository UsersRepository { get; set; }
        public UsersService(IUsersRepository usersRepository)
        {
            this.UsersRepository = usersRepository;
        }


        public Task<List<User>> GetUsersAsync()
        {
            return UsersRepository.GetListAsync();
        }

        public async Task<bool> AddData(User entity)
        {
            return await UsersRepository.AddData(entity);
        }

        public Task<bool> UpdateData(User user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateCurrentRole(User user)
        {
            return UsersRepository.UpdateCurrentRole(user);
        }

        public Task<List<User>> GetUsreByWhere(Expression<Func<User, bool>> whereExpression)
        {
            return UsersRepository.GetListByWhereAsync(whereExpression);
        }
    }
}
