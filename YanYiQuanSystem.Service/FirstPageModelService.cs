﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;
using YanYiQuanSystem.IRepository;
using YanYiQuanSystem.IService;

namespace YanYiQuanSystem.Service
{
    public class FirstPageModelService : IFirstPageModelService
    {
        private IFirstPageModelRepository FirstPageModelRepository { get; set; }
        public FirstPageModelService(IFirstPageModelRepository firstPageModelRepository)
        {
            this.FirstPageModelRepository = firstPageModelRepository;
        }

        public async Task<List<FirstPageModels>> GetListAsync()
        {
            return await FirstPageModelRepository.GetListAsync();
        }
    }
}
