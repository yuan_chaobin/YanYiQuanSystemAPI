﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    public class ServiceActivity_zone:IServiceActivity_zone
    {
        /// <summary>
        /// 注入仓储层
        /// </summary>
        public YanYiQuanSystem.IRepository.IRepository<Activity_zone> RepositoryActivity_zone { get; set; }
        public ServiceActivity_zone(YanYiQuanSystem.IRepository.IRepository<Activity_zone> RepositoryActivity_zone)
        {
            this.RepositoryActivity_zone = RepositoryActivity_zone;

        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">类的对象</param>
        /// <returns></returns>
        public Task<bool> AddData(Activity_zone entity)
        {
            return RepositoryActivity_zone.AddData(entity);
        }
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity">类的对象集合</param>
        /// <returns></returns>
        public Task<bool> BatchAddData(List<Activity_zone> entity)
        {
            return RepositoryActivity_zone.BatchAddData(entity);
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Ids">编号集合</param>
        /// <returns></returns>
        public Task<bool> BatchDelete(List<string> Ids)
        {
            return RepositoryActivity_zone.BatchDelete(Ids);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id">编号</param>
        /// <returns></returns>
        public Task<bool> DeleteData(string Id)
        {
            return RepositoryActivity_zone.DeleteData(Id);
        }
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns></returns>
        public Task<List<Activity_zone>> GetListAsync()
        {
            return RepositoryActivity_zone.GetListAsync();
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression">查询条件</param>
        /// <param name="orderExpression">排序</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">尺寸</param>
        /// <returns></returns>
        public Task<List<Activity_zone>> GetListByPage<TKey>(Expression<Func<Activity_zone, bool>> whereExpression, Expression<Func<Activity_zone, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return RepositoryActivity_zone.GetListByPage(whereExpression, orderExpression, pageIndex, pageSize);
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        public Task<List<Activity_zone>> GetListByWhereAsync(Expression<Func<Activity_zone, bool>> whereExpression)
        {
            return RepositoryActivity_zone.GetListByWhereAsync(whereExpression);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity">ID已有的类的对象</param>
        /// <returns></returns>
        public Task<bool> UpdateData(Activity_zone entity)
        {
            return RepositoryActivity_zone.UpdateData(entity);
        }
    }
}
