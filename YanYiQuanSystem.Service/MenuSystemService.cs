﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.IRepository;
    using System.Linq.Expressions;

    public class MenuSystemService : IMenuSystemService
    {
        /// <summary>
        /// 注入菜单仓储层接口
        /// </summary>
        public IMenuSystemRepository MenuSystemRepository { get; set; }
        public MenuSystemService(IMenuSystemRepository menuSystemRepository)
        {
            this.MenuSystemRepository = menuSystemRepository;
        }
        /// <summary>
        /// 添加菜单信息
        /// </summary>
        /// <returns></returns>
        public Task<bool> AddMenuSystemAsync(MenuSystem menuSystem)
        {
            return MenuSystemRepository.AddData(menuSystem);
        }

        /// <summary>
        /// 批量添加菜单信息
        /// </summary>
        /// <returns></returns>
        public Task<bool> BatchAddMenuSystem()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 批量删除菜单信息
        /// </summary>
        /// <returns></returns>

        public Task<bool> BatchDeleteMenuSystemAsync()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 删除菜单信息
        /// </summary>
        /// <returns></returns>
        public Task<bool> DeleteMenuSystemAsync(string Id)
        {
            return MenuSystemRepository.DeleteData(Id);
        }
        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<MenuSystem>> GetMenuSystemAsync()
        {
            return  await MenuSystemRepository.GetListAsync();
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <returns></returns>
        public async Task<List<MenuSystem>> GetMenuSystemsWhereAsync(Expression<Func<MenuSystem, bool>> whereExpression)
        {
            return await MenuSystemRepository.GetListByWhereAsync(whereExpression);
        }
        /// <summary>
        /// 修改菜单信息
        /// </summary>
        /// <returns></returns>
        public Task<bool> UpdateMenuSystemAsync(MenuSystem menuSystem)
        {
            return MenuSystemRepository.UpdateData(menuSystem);
        }

       
    }
}
