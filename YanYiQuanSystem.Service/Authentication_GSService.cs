﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.CommonUnitity;

    public   class Authentication_GSService:IAuthentication_GSIService
    {
        public IGS_Repository Repository { get; set; }
        public Authentication_GSService(IGS_Repository repository)
        {
            this.Repository = repository;
        }

        public async Task<bool> AddData(Authentication_GS entity)
        {
            return await Repository.AddData(entity);
        }

        public Task<bool> BatchAddData(List<Authentication_GS> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteData(string Id)
        {
            return await Repository.DeleteData(Id);
        }

        public Task<List<Authentication_GS>> GetListAsync()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 分页显示数据
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<Authentication_GS>> GetListByPage<TKey>(Expression<Func<Authentication_GS, bool>> whereExpression, Expression<Func<Authentication_GS, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return await Repository.GetListByPage(whereExpression, orderExpression, pageIndex, pageSize);
        }
      
        public async Task<List<Authentication_GS>> GetListByWhereAsync(Expression<Func<Authentication_GS, bool>> whereExpression)
        {
            return await Repository.GetListByWhereAsync(whereExpression);
        }
        /// <summary>
        /// 修改公司的基本信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateData(Authentication_GS entity)
        {
            return await Repository.UpdateData(entity);
        }

        public async Task<bool> UpdateUserModel(CommonData entity)
        {
            return await Repository.UpdateUserModel(entity);
        }

        public async Task<bool> UpdateUserModelZL(CommonData entity)
        {
            return await Repository.UpdateUserModelZL(entity);
        }
    }
}
