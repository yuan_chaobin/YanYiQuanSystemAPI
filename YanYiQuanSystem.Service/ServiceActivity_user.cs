﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    public class ServiceActivity_user:IServiceActity_user
    { /// <summary>
      /// 注入仓储层
      /// </summary>
        public YanYiQuanSystem.IRepository.IRepository<Activity_user> RepositoryActivity_user { get; set; }
        public ServiceActivity_user(YanYiQuanSystem.IRepository.IRepository<Activity_user> RepositoryActivity_user)
        {
            this.RepositoryActivity_user = RepositoryActivity_user;

        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">类的对象</param>
        /// <returns></returns>
        public Task<bool> AddData(Activity_user entity)
        {
            return RepositoryActivity_user.AddData(entity);
        }
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity">类的对象集合</param>
        /// <returns></returns>
        public Task<bool> BatchAddData(List<Activity_user> entity)
        {
            return RepositoryActivity_user.BatchAddData(entity);
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Ids">编号集合</param>
        /// <returns></returns>
        public Task<bool> BatchDelete(List<string> Ids)
        {
            return RepositoryActivity_user.BatchDelete(Ids);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id">编号</param>
        /// <returns></returns>
        public Task<bool> DeleteData(string Id)
        {
            return RepositoryActivity_user.DeleteData(Id);
        }
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns></returns>
        public Task<List<Activity_user>> GetListAsync()
        {
            return RepositoryActivity_user.GetListAsync();
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression">查询条件</param>
        /// <param name="orderExpression">排序</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">尺寸</param>
        /// <returns></returns>
        public Task<List<Activity_user>> GetListByPage<TKey>(Expression<Func<Activity_user, bool>> whereExpression, Expression<Func<Activity_user, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return RepositoryActivity_user.GetListByPage(whereExpression, orderExpression, pageIndex, pageSize);
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        public Task<List<Activity_user>> GetListByWhereAsync(Expression<Func<Activity_user, bool>> whereExpression)
        {
            return RepositoryActivity_user.GetListByWhereAsync(whereExpression);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity">ID已有的类的对象</param>
        /// <returns></returns>
        public Task<bool> UpdateData(Activity_user entity)
        {
            return RepositoryActivity_user.UpdateData(entity);
        }
    }
}
