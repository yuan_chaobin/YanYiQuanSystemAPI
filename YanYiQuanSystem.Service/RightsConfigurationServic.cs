﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    using System.Linq;
    public class RightsConfigurationServic : IRightsConfigurationService
    {
        /// <summary>
        /// 注入用户仓储层接口
        /// </summary>
        private IRightsConfigurationRepository  rightsConfigurationRepository { get; set; }
        public RightsConfigurationServic(IRightsConfigurationRepository rightsConfigurationRepository)
        {
            this.rightsConfigurationRepository = rightsConfigurationRepository;
        }
        public Task<bool> AddRightsConfiguration(RightsConfiguration entity)
        {
            return rightsConfigurationRepository.AddData(entity);
        }

        public Task<bool> DeleteRightsConfiguration(string Id)
        {
            return rightsConfigurationRepository.DeleteData(Id);
        }

        public Task<List<RightsConfiguration>> GetEmpWhereAsync(Expression<Func<RightsConfiguration, bool>> whereExpression)
        {
            return rightsConfigurationRepository.GetListByWhereAsync(whereExpression);
        }

        public Task<List<RightsConfiguration>> GetRightsConfigurationAsync()
        {
            return rightsConfigurationRepository.GetListAsync();
        }

        public Task<bool> UpdateRightsConfiguration(RightsConfiguration entity)
        {
            return rightsConfigurationRepository.UpdateData(entity);
        }
    }
}
