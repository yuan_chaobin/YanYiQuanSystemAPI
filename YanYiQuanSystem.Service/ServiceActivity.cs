﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;
using YanYiQuanSystem.IService;

namespace YanYiQuanSystem.Service
{
    public class ServiceActivity : IServiceActivity
    {
        /// <summary>
        /// 注入仓储层
        /// </summary>
        public YanYiQuanSystem.IRepository.IRepository<Activity> RepositoryActivity { get; set; }
        public ServiceActivity(YanYiQuanSystem.IRepository.IRepository<Activity> RepositoryActivity)
        {
            this.RepositoryActivity = RepositoryActivity;

        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">类的对象</param>
        /// <returns></returns>
        public Task<bool> AddData(Activity entity)
        {
            return RepositoryActivity.AddData(entity);
        }
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity">类的对象集合</param>
        /// <returns></returns>
        public Task<bool> BatchAddData(List<Activity> entity)
        {
            return RepositoryActivity.BatchAddData(entity);
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Ids">编号集合</param>
        /// <returns></returns>
        public Task<bool> BatchDelete(List<string> Ids)
        {
            return RepositoryActivity.BatchDelete(Ids);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id">编号</param>
        /// <returns></returns>
        public Task<bool> DeleteData(string Id)
        {
            return RepositoryActivity.DeleteData(Id);
        }
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns></returns>
        public Task<List<Activity>> GetListAsync()
        {
            return RepositoryActivity.GetListAsync();
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression">查询条件</param>
        /// <param name="orderExpression">排序</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">尺寸</param>
        /// <returns></returns>
        public Task<List<Activity>> GetListByPage<TKey>(Expression<Func<Activity, bool>> whereExpression, Expression<Func<Activity, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return RepositoryActivity.GetListByPage(whereExpression, orderExpression, pageIndex, pageSize);
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        public Task<List<Activity>> GetListByWhereAsync(Expression<Func<Activity, bool>> whereExpression)
        {
            return RepositoryActivity.GetListByWhereAsync(whereExpression);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity">ID已有的类的对象</param>
        /// <returns></returns>
        public Task<bool> UpdateData(Activity entity)
        {
            return RepositoryActivity.UpdateData(entity);
        }
    }
}
