﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.IRepository;
    using System.Linq.Expressions;
    public class QuanYiWeiHuService:IQuanYiWeiHuService
    {
        public IQuanYiWeiHuRepository quanYiWeiHuRepository { get; set; }
        public QuanYiWeiHuService(IQuanYiWeiHuRepository quanYiWeiHuRepository)
        {
            this.quanYiWeiHuRepository = quanYiWeiHuRepository;
        }

        public async Task<List<QuanYiWeiHu>> GetQuanYiWeiHus()
        {
            return await quanYiWeiHuRepository.GetListAsync();
        }

        public Task<bool> AddQuanYiWeiHu(QuanYiWeiHu quanYiWeiHu)
        {
            return quanYiWeiHuRepository.AddData(quanYiWeiHu);
        }

        public Task<bool> DeleteQuanYiWeiHu(string Id)
        {
            return quanYiWeiHuRepository.DeleteData(Id);
        }

        public Task<bool> UpdateQuanYiWeiHu(QuanYiWeiHu quanYiWeiHu)
        {
            return quanYiWeiHuRepository.UpdateData(quanYiWeiHu);
        }

        public  async Task<List<QuanYiWeiHu>> GetQuanYiWeiHuBywere(Expression<Func<QuanYiWeiHu, bool>> whereExpression)
        {
            return await quanYiWeiHuRepository.GetListByWhereAsync(whereExpression);
        }
    }
}
