﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YanYiQuanSystem.Domain;
using YanYiQuanSystem.IRepository;
using YanYiQuanSystem.IService;

namespace YanYiQuanSystem.Service
{
    public class Assistant_ModelService : IAssistant_ModelService
    {
        private IAssistant_ModelRepository Assistant_ModelRepository { get; set; }
        public Assistant_ModelService(IAssistant_ModelRepository assistant_ModelRepository)
        {
            this.Assistant_ModelRepository = assistant_ModelRepository;
        }

        public Task<bool> AddData(Assistant_Model entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchAddData(List<Assistant_Model> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            return Assistant_ModelRepository.DeleteData(Id);
        }

        public Task<List<Assistant_Model>> GetEmpWhereAsync(Expression<Func<Assistant_Model, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<List<Assistant_Model>> GetListAsync()
        {
            return Assistant_ModelRepository.GetListAsync();
        }

        public Task<List<Assistant_Model>> GetListByPage<TKey>(Expression<Func<Assistant_Model, bool>> whereExpression, Expression<Func<Assistant_Model, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<List<Assistant_Model>> GetListByWhereAsync(Expression<Func<Assistant_Model, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateData(Assistant_Model entity)
        {
            throw new NotImplementedException();
        }
    }
}
