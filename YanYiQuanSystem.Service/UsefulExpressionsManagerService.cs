﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
     
    public class UsefulExpressionsManagerService:IUsefulExpressionsManagerService
    {
        public IUsefulExpressionsManagerRespository usefulExpressionsManagerRespository { get; set; }

        public UsefulExpressionsManagerService(IUsefulExpressionsManagerRespository usefulExpressionsManagerRespository)
        {
            this.usefulExpressionsManagerRespository = usefulExpressionsManagerRespository;
        }
        /// <summary>
        /// 获取常用语数据
        /// </summary>
        /// <returns></returns>
        public async Task<List<UsefulExpressionsManager>> GetUsefulExpressionsManagerAsync()
        {
            return await usefulExpressionsManagerRespository.GetListAsync();
        }
        /// <summary>
        /// 添加常用语
        /// </summary>
        /// <param name="usefulExpressionsManager"></param>
        /// <returns></returns>
        public async Task<bool> AddUsefulExpressionsManager(UsefulExpressionsManager usefulExpressionsManager)
        {
            return await usefulExpressionsManagerRespository.AddData(usefulExpressionsManager);
        }
        /// <summary>
        /// 删除常用语
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteUsefulExpressionsManager(string Id)
        {
            return await usefulExpressionsManagerRespository.DeleteData(Id);
        }
        /// <summary>
        /// 修改常用语
        /// </summary>
        /// <param name="usefulExpressionsManager"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUsefulExpressionsManager(UsefulExpressionsManager usefulExpressionsManager)
        {
            return await usefulExpressionsManagerRespository.UpdateData(usefulExpressionsManager);
        }

        /// <summary>
        /// 条件查询常用语
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<UsefulExpressionsManager>> GetUsefulExpressionsManagerWhereAsync(Expression<Func<UsefulExpressionsManager, bool>> whereExpression)
        {
            return await usefulExpressionsManagerRespository.GetListByWhereAsync(whereExpression);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Task<bool> UpdateEusefulExpressionsManager(UsefulExpressionsManager entity)
        {
            return usefulExpressionsManagerRespository.UpdateData(entity);
        }
    }
}
