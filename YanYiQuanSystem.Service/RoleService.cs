﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    using System.Linq;
    public class RoleService : IRoleService
    {
        /// <summary>
        /// 注入用户仓储层接口
        /// </summary>
        private IRoleRepository  RoleRepository { get; set; }
        public RoleService(IRoleRepository RoleRepositorye)
        {
            this.RoleRepository = RoleRepositorye;
        }

        public async Task<List<Role>> GetRoleAsync()
        {
            return await  RoleRepository.GetListAsync();
        }

        public Task<bool> AddRole(Role entity)
        {
            return RoleRepository.AddData(entity);
        }

        public Task<bool> BatchAddRole(List<Role> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteRole(string Id)
        {
            return RoleRepository.DeleteData(Id);
        }

        public Task<bool> BatchDeleteRole(List<Role> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateRole(Role entity)
        {
            return RoleRepository.UpdateData(entity);
        }

        public Task<List<Role>> GetRoleWhereAsync(Expression<Func<Role, bool>> whereExpression)
        {
            return RoleRepository.GetListByWhereAsync(whereExpression);
        }

        public Task<List<Role>> GetListByPage(Expression<Func<Role, bool>> whereExpression, Expression<Func<Role>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
