﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    public class NoteService:INoteService
    {
        private INoteRepository noteRepository { get; set; }
        public NoteService(INoteRepository  noteRepository)
        {
            this.noteRepository = noteRepository;
        }

        public Task<List<NoteManager>> GetNoteWhereAsync(Expression<Func<NoteManager, bool>> whereExpression)
        {
            return noteRepository.GetListByWhereAsync(whereExpression);
        }

        public Task<List<NoteManager>> GetNoteAsync()
        {
            return noteRepository.GetListAsync();
        }
    }
}
