﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    public class RecruitmentLibraryService : IRecruitmentLibraryService
    {
        public IRepository<RecruitmentLibrary> Repository { get; set; }
        public RecruitmentLibraryService(IRepository<RecruitmentLibrary> Repository)
        {
            this.Repository = Repository;
        }
        public async Task<bool> AddData(RecruitmentLibrary entity)
        {
            return await Repository.AddData(entity);
        }

        public Task<bool> BatchAddData(List<RecruitmentLibrary> entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BatchDelete(List<string> Ids)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteData(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<List<RecruitmentLibrary>> GetListAsync()
        {
            return Repository.GetListAsync();
        }

        public async Task<List<RecruitmentLibrary>> GetListByPage<TKey>(Expression<Func<RecruitmentLibrary, bool>> whereExpression, Expression<Func<RecruitmentLibrary, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return await Repository.GetListByPage(whereExpression, orderExpression,pageIndex,pageSize);
        }

        public async Task<List<RecruitmentLibrary>> GetListByWhereAsync(Expression<Func<RecruitmentLibrary, bool>> whereExpression)
        {
           return  await Repository.GetListByWhereAsync(whereExpression);
        }

        public async Task<bool> UpdateData(RecruitmentLibrary entity)
        {
            return  await Repository.UpdateData(entity);
        }
    }
}
