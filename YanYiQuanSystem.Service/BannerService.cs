﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    using System.Linq;
    public class BannersService : IBannerService
    {
        /// <summary>
        /// 注入用户仓储层接口
        /// </summary>
        private IBannerRepository BannersRepository { get; set; }
        public BannersService(IBannerRepository BannersRepository)
        {
            this.BannersRepository = BannersRepository;
        }

        public Task<bool> AddBanner(Banners entity)
        {
            return BannersRepository.AddData(entity);
        }

        public Task<List<Banners>> GetBannerAsync()
        {
            return BannersRepository.GetListAsync();
        }

        public Task<bool> UpdateBanner(Banners entity)
        {
            return BannersRepository.UpdateData(entity);
        }

        public Task<List<Banners>> GetBannerWhereAsync(Expression<Func<Banners, bool>> whereExpression)
        {
            return BannersRepository.GetListByWhereAsync(whereExpression);
        }

        public Task<List<Banners>> GetListByPage(Expression<Func<Banners, bool>> whereExpression, Expression<Func<Banners>> orderExpression, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
