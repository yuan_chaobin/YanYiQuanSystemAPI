﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    public class ServiceActivity_vote:IServiceActivity_vote
    {
        /// <summary>
        /// 注入仓储层
        /// </summary>
        public YanYiQuanSystem.IRepository.IRepository<Activity_vote> RepositoryActivity_vote { get; set; }
        public ServiceActivity_vote(YanYiQuanSystem.IRepository.IRepository<Activity_vote> RepositoryActivity_vote)
        {
            this.RepositoryActivity_vote = RepositoryActivity_vote;

        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">类的对象</param>
        /// <returns></returns>
        public Task<bool> AddData(Activity_vote entity)
        {
            return RepositoryActivity_vote.AddData(entity);
        }
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entity">类的对象集合</param>
        /// <returns></returns>
        public Task<bool> BatchAddData(List<Activity_vote> entity)
        {
            return RepositoryActivity_vote.BatchAddData(entity);
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Ids">编号集合</param>
        /// <returns></returns>
        public Task<bool> BatchDelete(List<string> Ids)
        {
            return RepositoryActivity_vote.BatchDelete(Ids);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id">编号</param>
        /// <returns></returns>
        public Task<bool> DeleteData(string Id)
        {
            return RepositoryActivity_vote.DeleteData(Id);
        }
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns></returns>
        public Task<List<Activity_vote>> GetListAsync()
        {
            return RepositoryActivity_vote.GetListAsync();
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereExpression">查询条件</param>
        /// <param name="orderExpression">排序</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">尺寸</param>
        /// <returns></returns>
        public Task<List<Activity_vote>> GetListByPage<TKey>(Expression<Func<Activity_vote, bool>> whereExpression, Expression<Func<Activity_vote, TKey>> orderExpression, int pageIndex, int pageSize)
        {
            return RepositoryActivity_vote.GetListByPage(whereExpression, orderExpression, pageIndex, pageSize);
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        public Task<List<Activity_vote>> GetListByWhereAsync(Expression<Func<Activity_vote, bool>> whereExpression)
        {
            return RepositoryActivity_vote.GetListByWhereAsync(whereExpression);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity">ID已有的类的对象</param>
        /// <returns></returns>
        public Task<bool> UpdateData(Activity_vote entity)
        {
            return RepositoryActivity_vote.UpdateData(entity);
        }
    }
}
