﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YanYiQuanSystem.Service
{
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    public  class SensibilityManagerService:ISensibilityManagerService
    {
        private ISensibilityManagerRepository SensibilityManagerRepository { get; set; }

        public SensibilityManagerService(ISensibilityManagerRepository sensibilityManagerRepository)
        {
            this.SensibilityManagerRepository = sensibilityManagerRepository;
        }
        /// <summary>
        /// 获取敏感字词汇
        /// </summary>
        /// <returns></returns>
        public async Task<List<SensibilityManager>> GetSensibilityManagerAsync()
        {
            return await SensibilityManagerRepository.GetListAsync();
        }
        /// <summary>
        /// 添加敏感字
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        public async Task<bool> AddSensibilityManager(SensibilityManager sensibilityManager)
        {
            return await SensibilityManagerRepository.AddData(sensibilityManager);
        }
        /// <summary>
        /// 删除敏感字
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSensibilityManager(string Id)
        {
            return await SensibilityManagerRepository.DeleteData(Id);
        }
        /// <summary>
        /// 修改敏感字
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        public async Task<bool> UpdateSensibilityManager(SensibilityManager sensibilityManager)
        {
            return await SensibilityManagerRepository.UpdateData(sensibilityManager);
        }
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<SensibilityManager>> GetSensibilityManagerWhereAsync(Expression<Func<SensibilityManager, bool>> whereExpression)
        {
            return await SensibilityManagerRepository.GetListByWhereAsync(whereExpression);
        }
    }
}
