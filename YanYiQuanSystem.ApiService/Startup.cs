using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace YanYiQuanSystem.ApiService
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    using YanYiQuanSystem.Repository;
    using YanYiQuanSystem.Service;

    public class Startup
    {
        private IConfiguration Configuration { get; set; }
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                // 忽略循环引用
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                // 不使用驼峰
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                // 设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                // 如字段为null值，该字段不会返回到前端
                // options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });

            //添加跨域
            services.AddCors(r => {
                r.AddPolicy("Or", r =>
                {
                    r.AllowAnyOrigin();
                    r.AllowAnyMethod();
                    r.AllowAnyHeader();
                });
            });

            //添加NLog
            NLog.LogManager.LoadConfiguration("NLog.config").GetCurrentClassLogger();
            NLog.LogManager.Configuration.Variables["connectionString"] = Configuration["OracleConnection"];
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //避免日志中的中文输出乱码


            //添加注入NSwag
            services.AddSwaggerDocument();

            //添加注入上下文
            services.AddScoped<YanYiQuanSystemDbContext>();

            //添加Redis
            services.AddScoped<RedisDbHelper>();

            //添加注入用户表
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IUsersService, UsersService>();

            //添加注入模特列表
            services.AddScoped<IModelLibraryRepository, ModelLibraryRepository>();
            services.AddScoped<IModelLibraryService, ModelLibraryService>();

            //添加注入首页模特列表
            services.AddScoped<IFirstPageModelRepository, FirstPageModelRepository>();
            services.AddScoped<IFirstPageModelService, FirstPageModelService>();

            //添加注入招聘公司表
            services.AddScoped<IReCompanyRepository, ReCompanyRepository>();
            services.AddScoped<IReCompanyService, ReCompanyService>();

            //添加注入相册库表
            services.AddScoped<IPhotoAlbumRepository, PhotoAblumRepository>();
            services.AddScoped<IPhotoAblumService, PhotoAblumService>();

            //添加注入助理列表
            services.AddScoped<IAssistantRepository, AssistantRepository>();
            services.AddScoped<IAssistantService, AssistantService>();

            //添加注入助理模特列表
            services.AddScoped<IAssistant_ModelRepository, Assistant_ModelRepository>();
            services.AddScoped<IAssistant_ModelService, Assistant_ModelService>();

            //添加注入权益购买记录表
            services.AddScoped<IPurchaseRecordRepository, PurchaseRecordRepository>();
            services.AddScoped<IPurchaseRecordService, PurchaseRecordService>();

            //添加注入关注表
            services.AddScoped<IAttentionRepository, AttentionRepository>();
            services.AddScoped<IAttentionService, AttentionService>();

            //添加注入菜单表
            services.AddScoped<IMenuSystemRepository, MenuSystemRepository>();
            services.AddScoped<IMenuSystemService, MenuSystemService>();
            //添加注入公司招聘表
            services.AddScoped<IRepository<RecruitmentLibrary>, RecruitmentLibraryRepository>();
            services.AddScoped<IRecruitmentLibraryService, RecruitmentLibraryService>();
            //添加注入活动表
            services.AddScoped<IRepository<Activity>, RepositoryActivity>();
            services.AddScoped<IServiceActivity, ServiceActivity>();

            //添加注入活动用户表
            services.AddScoped<IRepository<Activity_user>, RepositoryActivity_user>();
            services.AddScoped<IServiceActity_user, ServiceActivity_user>();

            //添加注入票选表
            services.AddScoped<IRepository<Activity_vote>, RepositoryActivity_vote>();
            services.AddScoped<IServiceActivity_vote, ServiceActivity_vote>();

            //添加注入举报表
            services.AddScoped<IRepository<Activity_report>, RepositoryActivity_report>();
            services.AddScoped<IServiceActity_report, ServiceActivity_report>();

            //添加注入赛区表
            services.AddScoped<IRepository<Activity_zone>, RepositoryActivity_zone>();
            services.AddScoped<IServiceActivity_zone, ServiceActivity_zone>();
            //添加认证模特
            services.AddScoped<IMT_Repository, Authentication_MTRepository>();
            services.AddScoped<IAuthentication_MTIService, Authentication_MTService>();

            //添加认证公司
            services.AddScoped<IGS_Repository, Authentication_GSRepository>();
            services.AddScoped<IAuthentication_GSIService, Authentication_GSService>();
            //添加注入敏感字表
            services.AddScoped<ISensibilityManagerRepository, SensibilityManagerRepository>();
            services.AddScoped<ISensibilityManagerService, SensibilityManagerService>();


            //添加注入常用语表
            services.AddScoped<IUsefulExpressionsManagerRespository, UsefulExpressionsManagerRespository>();
            services.AddScoped<IUsefulExpressionsManagerService, UsefulExpressionsManagerService>();

            //添加注入员工表
            services.AddScoped<IEmpRepository, EmpRepository>();
            services.AddScoped<IEmpService, EmpService>();

            //添加注入敏感字表
            services.AddScoped<ISensibilityManagerRepository,SensibilityManagerRepository>();
            services.AddScoped<ISensibilityManagerService, SensibilityManagerService>();

          
            //添加注入常用语表
            services.AddScoped<IUsefulExpressionsManagerRespository, UsefulExpressionsManagerRespository>();
            services.AddScoped<IUsefulExpressionsManagerService, UsefulExpressionsManagerService>();

            //添加注入短信表
            services.AddScoped<INoteRepository, NoteRepository>();
            services.AddScoped<INoteService, NoteService>();


            //添加注入角色表
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IRoleService, RoleService>();

            //添加注入Banner维护表
            services.AddScoped<IBannerRepository, BannerRepository>();
            services.AddScoped<IBannerService, BannersService>();

            //添加注入Banner维护表
            //添加注入标签维护表
            services.AddScoped<ILabelRepository, LabelRepository>();
            services.AddScoped<ILabelService, LabelService>();

            //添加注入系统权益配置维护表
            services.AddScoped<IRightsConfigurationRepository, RightsConfigurationRepository>();
            services.AddScoped<IRightsConfigurationService, RightsConfigurationServic>();
            //添加注入系统权益维护表
            services.AddScoped<IQuanYiWeiHuRepository, QuanYiWeiHuRepository>();
            services.AddScoped<IQuanYiWeiHuService, QuanYiWeiHuService>();
            //用户权益
            services.AddScoped<IUserRightsRepository, UserRightsRepository>();
            services.AddScoped<IUserRightsService, UserRightsService>();
            //用户权益
            services.AddScoped<ISystemInfromRespository, SystemInfromRespository>();
            services.AddScoped<ISystemInfromService, SystemInfromService>();
            //员工权限分配
            services.AddScoped<IRole_MenuRepository, Role_MenuRepository>();
            services.AddScoped<IRole_MenuService, Role_MenuService>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseOpenApi();
            app.UseSwaggerUi3();
            app.UseCors("Or");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
