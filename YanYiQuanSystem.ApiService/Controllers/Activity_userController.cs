﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    [ApiController]
    public class Activity_userController : ControllerBase
    {
        private IServiceActivity serviceActivity { get; set; }
        private IServiceActity_user serviceActity_User { get; set; }//用户服务层
        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis


        public Activity_userController(IServiceActivity serviceActivity,IServiceActity_user serviceActity_User, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper)
        {
            this.serviceActivity = serviceActivity;
            this.serviceActity_User = serviceActity_User;
            this.logger = loggerFactory.CreateLogger<UsersController>();
            this.RedisDbHelper = redisDbHelper;
        }

        /// <summary>
        /// 异步获取所有用户信息
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityListuser")]
        [HttpGet]
        public async Task<ResponseFormat<List<Activity_user>>> GetUsersAsync()
        {
            try
            {
                ResponseFormat<List<Activity_user>> response = new ResponseFormat<List<Activity_user>>();

                response.Data = await serviceActity_User.GetListAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 异步获取分页信息
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityuserListPage")]
        [HttpGet]
        public async Task<ResponseFormat<List<Activity_user>>> GetActivityListPage(int page = 1, int limit = 2, string Id="",string userName = "", string phone = "", string nickName = "", string card = "", int firstPage = 0, string order_activity ="", string order ="")
        {
            try
            {
                ResponseFormat<List<Activity_user>> response = new ResponseFormat<List<Activity_user>>();
                List<Activity_user> activitys = await serviceActity_User.GetListAsync();
                activitys = activitys.Where(o => o.Id_activity == Id).ToList();
                //模特姓名
                if (!string.IsNullOrEmpty(userName))
                {
                    activitys = activitys.Where(o => o.Name_user.Contains(userName)).ToList();
                }
                //手机号
                if (!string.IsNullOrEmpty(phone))
                {
                    activitys = activitys.Where(o => o.Phone_user == phone).ToList();
                }
                //昵称
                if (!string.IsNullOrEmpty(nickName))
                {
                    activitys = activitys.Where(o => o.Nickname.Contains(nickName)).ToList();
                }
                //身份证号
                if (!string.IsNullOrEmpty(card))
                {
                    activitys = activitys.Where(o => o.Idcard_user == card).ToList();
                }
                //是否在首页
                if (firstPage != 0)
                {
                    activitys = activitys.Where(o => o.Is_allow_index == firstPage).ToList();
                }
                if (!string.IsNullOrEmpty(order_activity) && !string.IsNullOrEmpty(order))
                {
                    if (order_activity == "手机" && order == "顺序")
                    {
                        response.Data = activitys.OrderBy(o => o.Phone_user).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "姓名" && order == "顺序")
                    {
                        response.Data = activitys.OrderBy(o => o.Name_user).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "手机" && order == "倒序")
                    {
                        response.Data = activitys.OrderByDescending(o => o.Phone_user).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "姓名" && order == "倒序")
                    {
                        response.Data = activitys.OrderByDescending(o => o.Name_user).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                }
                else
                {
                response.Data = activitys.OrderBy(o => o.Uuid).Skip((page - 1) * limit).Take(limit).ToList();
                }
                response.Code = 0;
                response.Count = activitys.Count();
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 异步获取一条活动用户
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityuser")]
        [HttpGet]
        public async Task<ResponseFormat<Activity_user>> GetActivity(string Id)
        {
            try
            {
                ResponseFormat<Activity_user> response = new ResponseFormat<Activity_user>();

                List<Activity_user> activities = await serviceActity_User.GetListByWhereAsync(o => o.Uuid == Id);
                response.Data = activities.FirstOrDefault();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取一条数据");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 添加活动
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("AddActivitys")]
        [HttpPost]
        public async Task<ResponseFormat<string>> AddActivitys([FromBody]Activity_user activity)
        {
            activity.Uuid = Guid.NewGuid().ToString();
            ResponseFormat<string> response = new ResponseFormat<string>();
            List<Activity_user> activities =serviceActity_User.GetListByWhereAsync(o => o.Id_user == activity.Id_user).Result;
            foreach (var item in activities)
            {
                if(activity.Id_activity==item.Id_activity)
                {
                    response.Code = 300;
                    response.Msg = "已存在此活动";
                    return response;
                }
                if (item.Date_birthday < activity.Date_birthday&& activity.Date_birthday < item.Create_time)
                {
                    response.Code = 300;
                    response.Msg = "活动时间重叠，不能报名";
                    return response;
                }
                if (item.Date_birthday < activity.Create_time && activity.Create_time < item.Date_birthday)
                {
                    response.Code = 300;
                    response.Msg = "活动时间重叠，不能报名";
                    return response;
                }
                if (activity.Date_birthday<item.Date_birthday && item.Create_time < activity.Create_time)
                {
                    response.Code = 300;
                    response.Msg = "活动时间重叠，不能报名";
                    return response;
                }
                if (item.Date_birthday < activity.Date_birthday && activity.Create_time < item.Create_time)
                {
                    response.Code = 300;
                    response.Msg = "活动时间重叠，不能报名";
                    return response;
                }
                if (item.Date_birthday == activity.Date_birthday || activity.Create_time == item.Create_time)
                {
                    response.Code = 300;
                    response.Msg = "活动时间重叠，不能报名";
                    return response;
                }
            }
            Activity activity1 = serviceActivity.GetListByWhereAsync(o => o.Uuid == activity.Id_activity).Result.FirstOrDefault();
            if (activity1.Time_to_baoming < DateTime.Now) {
                response.Code = 300;
                response.Msg = "已经错过报名时间，不能报名";
                return response;
            }
            if (DateTime.Now < activity1.Time_from_baoming)
            {
                response.Code = 300;
                response.Msg = "还未到报名时间  ，不能报名";
                return response;
            }
            bool n = await serviceActity_User.AddData(activity);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 修改活动用户
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("EditActivityuser")]
        [HttpPost]
        public async Task<ResponseFormat<string>> EditActivity([FromBody]Activity_user activity)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.UpdateData(activity);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "修改成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "修改失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 批量添加活动用户
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("AddActivityListuser")]
        [HttpPost]
        public async Task<ResponseFormat<string>> AddActivityList([FromBody]List<Activity_user> activitys)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.BatchAddData(activitys);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 删除活动用户
        /// </summary>
        /// <param name="activity"></param>
        /// <returns></returns>
        [Route("DeleteActivityuser")]
        [HttpPost]
        public async Task<ResponseFormat<string>> DeleteActivity([FromBody]Activity_user activity)
        {
            string Id = activity.Uuid;
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.DeleteData(Id);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 批量删除活动用户
        /// </summary>
        /// <param name="activitys"></param>
        /// <returns></returns>
        [Route("DeleteActivityListuser")]
        [HttpPost]
        public async Task<ResponseFormat<string>> DeleteActivityList([FromBody]List<string> Ids)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.BatchDelete(Ids);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
    }
}