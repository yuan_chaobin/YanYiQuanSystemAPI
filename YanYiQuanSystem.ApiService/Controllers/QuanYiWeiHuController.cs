﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class QuanYiWeiHuController : ControllerBase
    {

        public IQuanYiWeiHuService  quanYiWeiHuService { get; set; }
        public IUserRightsService userRightsService { get; set; }
        public IUsersService usersService { get; set; }
        public QuanYiWeiHuController(IQuanYiWeiHuService  quanYiWeiHuService,IUserRightsService userRightsService, IUsersService usersService)
        {
            this.quanYiWeiHuService = quanYiWeiHuService;
            this.userRightsService = userRightsService;
            this.usersService = usersService;
        }
        /// <summary>
        /// 获取系统权益信息
        /// </summary>
        /// <param name="ProductName"></param>
        /// <param name="DayNumber"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetQuanYi(string ProductName="",string DayNumber="",string PhoneNumber="", int page = 1, int limit = 3)
        {
            ResponseFormat<List<CommonData>> responseFormat = new ResponseFormat<List<CommonData>>();
            try
            {
                List<QuanYiWeiHu> quanYiWeiHus = await quanYiWeiHuService.GetQuanYiWeiHus();
                List<UserRights> userRights = await userRightsService.GetUserRights();
                List<User> users = await usersService.GetUsersAsync();
                var query = from a in quanYiWeiHus
                            join b in userRights on a.UserRightsId equals b.UserRightsId
                            join c in users on b.UsersID equals c.UserId
                            select new CommonData
                            {
                                OrderById=a.OrderById,
                                ProductName=a.ProductName,
                                ProductId=a.ProductId,
                                Originalamount=a.Originalamount,
                                ActualAmount=a.ActualAmount,
                                DayNumber=a.DayNumber,
                                IsState=a.IsState,
                                Remark=a.Remark,
                                PhoneNumber=c.PhoneNumber
                            };
                var count = query.Count();
                if (!string.IsNullOrEmpty(ProductName))
                {
                    query = query.Where(o => o.ProductName.Contains(ProductName)).ToList();
                }
                if (!string.IsNullOrEmpty(DayNumber))
                {
                    query = query.Where(o => o.DayNumber==Convert.ToInt32( DayNumber)).ToList();
                }
                if (!string.IsNullOrEmpty(PhoneNumber))
                {
                    query = query.Where(o => o.PhoneNumber.Contains(PhoneNumber)).ToList();
                }
                responseFormat.Data = query.OrderBy(o => o.OrderById).Skip((page - 1) * limit).Take(limit).ToList();
                responseFormat.Count = query.Count();
                return responseFormat;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 添加权益
        /// </summary>
        /// <param name="quanYiWeiHu"></param>
        /// <returns></returns>
        ///
        [HttpPost]
        public async Task<ResponseFormat<bool>> AddQuanYi(QuanYiWeiHu quanYiWeiHu)
        {
                ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {
                quanYiWeiHu.ProductId = Guid.NewGuid().ToString();
                bool result = await quanYiWeiHuService.AddQuanYiWeiHu(quanYiWeiHu);
                if (result)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "添加成功";
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "添加失败";
                }
            
            }
            catch (Exception)
            {
                responseFormat.Code = 500;
                responseFormat.Msg = "系统繁忙";
                throw;
            }
            return responseFormat;
        }
        /// <summary>
        /// 获取单条信息
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<QuanYiWeiHu>> GetByID (string ID)
        {
            ResponseFormat<QuanYiWeiHu> responseFormat = new ResponseFormat<QuanYiWeiHu>();
            try
            {
                List<QuanYiWeiHu> quanYiWeiHus = await quanYiWeiHuService.GetQuanYiWeiHuBywere(o => o.ProductId.Equals(ID));
                responseFormat.Data = quanYiWeiHus.FirstOrDefault();
                return responseFormat;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// 修改权益
        /// </summary>
        /// <param name="quanYiWeiHu"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateQuanYi(QuanYiWeiHu quanYiWeiHu)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {             
                bool result = await quanYiWeiHuService.UpdateQuanYiWeiHu(quanYiWeiHu);
                if (result)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "修改成功";
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "修改失败";
                }

            }
            catch (Exception)
            {
                responseFormat.Code = 500;
                responseFormat.Msg = "系统繁忙";
                throw;
            }
            return responseFormat;
        }
    }
}
