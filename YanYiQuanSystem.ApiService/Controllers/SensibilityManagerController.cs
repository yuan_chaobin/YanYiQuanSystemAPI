﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    using System.Linq;
    [Route("api/[controller]/[action]")]
    [ApiController]
    
    public class SensibilityManagerController : ControllerBase
    {
        private ISensibilityManagerService sensibilityManagerService { get; set; }
        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis
        public SensibilityManagerController(ISensibilityManagerService sensibilityManagerService, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper)
        {
            this.sensibilityManagerService = sensibilityManagerService;
            this.logger = loggerFactory.CreateLogger<SensibilityManagerController>();
            this.RedisDbHelper = redisDbHelper;
        }
        /// <summary>
        /// 获取敏感字管理
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetSensibilityManager(int page = 1, string SensibilityManagerName = "", int limit = 2)
        {
            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();
            try
            {//获取所有菜单信息
                List<SensibilityManager> sensibilityManagers = await sensibilityManagerService.GetSensibilityManagerAsync();
                var query = from a in sensibilityManagers
                            select new CommonData
                            {
                                SensibilityManagerID=a.SensibilityManagerID,
                                SensibilityManagerName=a.SensibilityManagerName
                            };
                         logger.LogInformation("获取数据成功");
                //模糊查询
                if (!string.IsNullOrEmpty(SensibilityManagerName))
                {
                    query = query.Where(o => o.SensibilityManagerName.Contains(SensibilityManagerName)).ToList();
                }
                response.Data = query.OrderBy(o => o.SensibilityManagerID).Skip((page - 1) * limit).Take(limit).ToList();
                response.Code = 200;
                response.Count = query.Count();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return response;
        }
        /// <summary>
        /// 添加敏感字
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> AddSensibilityManager(SensibilityManager sensibilityManager)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();

            try
            {
                sensibilityManager.SensibilityManagerID = Guid.NewGuid().ToString();
                bool result = await sensibilityManagerService.AddSensibilityManager(sensibilityManager);
                if (result==true)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "添加敏感字成功";
                    logger.LogInformation("添加敏感字成功");
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "添加敏感字失败";
                    logger.LogInformation("添加敏感字失败");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return responseFormat;
        }

        /// <summary>
        /// 删除敏感字
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> DeleteSensibilityManager(string Id)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();

            try
            {
                bool result = await sensibilityManagerService.DeleteSensibilityManager(Id);
                if (result == true)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "删除敏感字成功";
                    logger.LogInformation("删除敏感字成功");
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "删除敏感字失败";
                    logger.LogInformation("删除敏感字失败");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return responseFormat;
        }
        /// <summary>
        /// 获取修改的单条信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<SensibilityManager>> GetById(string Id)
        {
            ResponseFormat<SensibilityManager> responseFormat = new ResponseFormat<SensibilityManager>();
            try
            {
                List<SensibilityManager> emps = await sensibilityManagerService.GetSensibilityManagerWhereAsync(o=>o.SensibilityManagerID.Equals(Id));          
                responseFormat.Data = emps.FirstOrDefault();
                return responseFormat;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 修改敏感字
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateSensibilityManager(SensibilityManager  sensibilityManager)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {
               
                bool result = await sensibilityManagerService.UpdateSensibilityManager(sensibilityManager);
                if (result == true)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "修改敏感字成功";
                    logger.LogInformation("修改敏感字成功");
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "修改敏感字失败";
                    logger.LogInformation("修改敏感字失败");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return responseFormat;
        }
    }
}
