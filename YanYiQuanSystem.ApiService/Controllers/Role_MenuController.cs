﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class Role_MenuController : ControllerBase
    {
        private IRole_MenuService role_MenuService { get; set; }//员工服务层

        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis
        public IMenuSystemService menuSystemService { get; set; }
        private IRoleService roleService { get; set; }


        public Role_MenuController(IRole_MenuService role_MenuService, IRoleService roleService, IMenuSystemService menuSystemService, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper)
        {
            this.role_MenuService = role_MenuService;
            this.logger = loggerFactory.CreateLogger<Role_MenuController>();
            this.RedisDbHelper = redisDbHelper;
            this.menuSystemService = menuSystemService;
            this.roleService = roleService;
        }

        /// <summary>
        /// 添加角色与菜单的联系
        /// </summary>
        /// <param name="emp"></param>E:\YYQVUE\yanyiquansystem\src\views\FuRole\AddRole.vue
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddRole_Menu([FromBody] Role_Menu  role_Menu)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();

            List<Role_Menu> role_Menus = role_MenuService.GetRole_MenuWhereAsync(o => o.RoleId.Equals(role_Menu.RoleId)).Result;
            for (int i = 0; i < role_Menus.Count;i++)
            {
                if (role_Menu.MenuID == role_Menus[i].MenuID)
                {
                    response.Code = 400;
                    response.Msg = "该菜单已存在";
                    return Ok(response);
                }
            }
            role_Menu.EMId = Guid.NewGuid().ToString();
            bool n = role_MenuService.AddEmp(role_Menu).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }

        /// <summary>
        /// 根据角色Id进行查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Role_Menu>>> ShowRole_MenuById(string id)
        {
            List<Role_Menu>  role_Menus = await role_MenuService.GetRole_MenuWhereAsync(o => o.RoleId.Equals(id));
            ResponseFormat<List<Role_Menu>> responseFormat = new ResponseFormat<List<Role_Menu>>();
            responseFormat.Data = role_Menus;
            return responseFormat;
        }

        /// <summary>
        /// 分页查询显示
        /// </summary>
        /// <param name="EmpName"></param>
        /// <param name="Phone"></param>
        /// <param name="UserName"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Role_Menu>>> GetRole_Menus(string id, int page = 1, int limit = 3)
        {
            ResponseFormat<List<Role_Menu>> response = new ResponseFormat<List<Role_Menu>>();
            List<Role> roles = await roleService.GetRoleAsync();
            List<MenuSystem> menuSystems = await menuSystemService.GetMenuSystemAsync();
            List<Role_Menu> role_Menus = await role_MenuService.GetRole_MenuWhereAsync(o => o.RoleId.Equals(id));
            var query = from rm in role_Menus
                        join r in roles on rm.RoleId equals r.RoleId
                        join m in menuSystems on rm.MenuID equals m.MenuID
                        select new Role_Menu
                        {
                           EMId = rm.EMId, 
                           MenuID = m.MenuName,
                           RoleId = r.RoleId,
                           PID = m.MenuPid
                        };
            if (!string.IsNullOrEmpty(id))
            {
                query = query.Where(o => o.RoleId.Contains(id)).ToList();
            }
            List<Role_Menu> role_Menus1 = query.ToList();
            var query1 = from rm in role_Menus1
                        join m in menuSystems on rm.PID equals m.MenuID
                        select new Role_Menu
                        {
                            EMId = rm.EMId,
                            MenuID = rm.MenuID,
                            RoleId = rm.RoleId,
                            PID = m.MenuPid,
                            Pname=m.MenuName
                        };
            var count = query1.Count();
            response.Data = query1.OrderBy(o => o.PID).Skip((page - 1) * limit).Take(limit).ToList();
            response.Count = query1.Count();
            return response;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>

        [HttpPost]
        public IActionResult DeleteRole_Menu([FromBody] Role_Menu role_Menu)
        {
            string Id = role_Menu.EMId;
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = role_MenuService.DeleteRole_Menu(Id).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }


        /// <summary>
        /// 根据菜单Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Role_Menu>>> ShowRole_MenuWhereMenuId(string id)
        {
            List<Role_Menu> role_Menus = await role_MenuService.GetRole_MenuWhereMenuId(o => o.MenuID.Equals(id));
            ResponseFormat<List<Role_Menu>> responseFormat = new ResponseFormat<List<Role_Menu>>();
            responseFormat.Data = role_Menus;
            return responseFormat;
        }

        /// <summary>
        /// 获取所有菜单信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<MenuSystem>>> ShowMenu()
        {
            try
            {
                ResponseFormat<List<MenuSystem>> response = new ResponseFormat<List<MenuSystem>>();

                response.Data = await menuSystemService.GetMenuSystemAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");
                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 批量添加权限
        /// </summary>
        /// <param name="role_Menu"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ToAddRole_Menu([FromBody] List<Role_Menu> role_Menu)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            List<Role_Menu> role_Menus1 = new List<Role_Menu>();
            List<Role_Menu> roleAll = role_MenuService.GetRole_MenuAsync().Result;
            for (int j = 0; j < roleAll.Count; j++)
            {
                for (int i = 0; i < role_Menu.Count; i++)
                {
                
                    if (roleAll[j].RoleId==role_Menu[i].RoleId)
                    {
                        role_Menus1.Add(roleAll[j]);
                    }
                }
            }


            for (int j = 0; j < role_Menus1.Count; j++)
            {
                for (int i = 0; i < role_Menu.Count; i++)
                {
                
                    if (role_Menu[i].MenuID == role_Menus1[j].MenuID)
                    {
                        response.Code = 400;
                        response.Msg = "所选菜单中包含已存在的权限";
                        return Ok(response);
                    }
                }
            }

            for (int i = 0; i < role_Menu.Count(); i++)
            {
                role_Menu[i].EMId = Guid.NewGuid().ToString();
            }

            bool n = role_MenuService.BatchAddData(role_Menu).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }
    }
}
