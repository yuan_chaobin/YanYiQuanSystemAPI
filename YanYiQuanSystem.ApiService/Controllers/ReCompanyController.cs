﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ReCompanyController : ControllerBase
    {
        private IReCompanyService ReCompanyService { get; set; }
        private IUsersService UsersService { get; set; }

        public ReCompanyController(IReCompanyService reCompanyService, IUsersService usersService)
        {
            this.ReCompanyService = reCompanyService;
            this.UsersService = usersService;
        }

        /// <summary>
        /// 异步获取所有招聘公司数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetReCompany(string name="",string phone="",string person="")
        {
            try
            {
                ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

                List<RecruitmentCompanys> comList = await ReCompanyService.GetListAsync();
                List<User> userList = await UsersService.GetUsersAsync();

                var query = from a in comList
                            join b in userList on a.UsersID equals b.UserId where b.CurrentRole ==2 || b.CurrentRole == 3 && a.ApproveState ==2
                            select new CommonData
                            {
                                HrId = a.HrId,
                                UsersID = a.UsersID,
                                CompanyType = a.CompanyType,
                                CompanyName = a.CompanyName,
                                CompanyPhone = a.CompanyPhone,
                                CompanyPerson = a.CompanyPerson,
                                CompanyAddress = a.CompanyAddress,
                                TaxpayerNum = a.TaxpayerNum,
                                WorkPosition = a.WorkPosition,
                                ApproveState = a.ApproveState,
                                LoggingStatus = a.LoggingStatus,
                                MyCode = a.MyCode,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = b.Birthday,
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(o => o.CompanyName.Contains(name)).ToList();
                }
                if (!string.IsNullOrEmpty(phone))
                {
                    query = query.Where(o => o.CompanyPhone == phone).ToList();
                }
                if (!string.IsNullOrEmpty(person))
                {
                    query = query.Where(o => o.CompanyPerson.Contains(person)).ToList();
                }
                response.Data = query.ToList();
                response.Count = response.Data.Count();
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// 根据ID获取公司详细信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetReCompanyById(string hrId)
        {
            try
            {
                ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

                List<RecruitmentCompanys> comList = await ReCompanyService.GetListAsync();
                List<User> userList = await UsersService.GetUsersAsync();

                var query = from a in comList
                            join b in userList on a.UsersID equals b.UserId
                            where b.CurrentRole == 2 || b.CurrentRole == 3 && a.ApproveState == 2
                            select new CommonData
                            {
                                HrId = a.HrId,
                                UsersID = a.UsersID,
                                CompanyType = a.CompanyType,
                                CompanyName = a.CompanyName,
                                CompanyPhone = a.CompanyPhone,
                                CompanyPerson = a.CompanyPerson,
                                CompanyAddress = a.CompanyAddress,
                                TaxpayerNum = a.TaxpayerNum,
                                WorkPosition = a.WorkPosition,
                                ApproveState = a.ApproveState,
                                LoggingStatus = a.LoggingStatus,
                                MyCode = a.MyCode,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = b.Birthday,
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };
                if (!string.IsNullOrEmpty(hrId))
                {
                    query = query.Where(o => o.HrId == hrId).ToList();
                }
                response.Data = query.ToList();
                response.Count = response.Data.Count();
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// 修改招聘公司
        /// </summary>
        /// <param name="commonData"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateCompany(CommonData commonData)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ReCompanyService.UpdateCompany(commonData);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 删除招聘公司
        /// </summary>
        /// <param name="hrId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<bool>> DeleteCompany(string Id)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ReCompanyService.DeleteData(Id);
                response.Code = 200;
                response.Data = result;
                response.Msg = "删除成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }
    }
}
