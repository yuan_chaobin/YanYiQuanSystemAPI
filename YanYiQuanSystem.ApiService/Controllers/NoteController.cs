﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.CommonUnitity;
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        public INoteService noteService { get; set; }
        public NoteController(INoteService noteService)
        {
            this.noteService = noteService;
        }
        [HttpGet]
        public async Task<ResponseFormat<List<NoteManager>>> GetNodeManager()
        {
            ResponseFormat<List<NoteManager>> responseFormat = new ResponseFormat<List<NoteManager>>();
            try
            {
                responseFormat.Data = await noteService.GetNoteAsync();
                return responseFormat;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetNoteManagerWhere(string PhoneNumber="", int page = 1, int limit = 2)
        {
            ResponseFormat<List<CommonData>> responseFormat = new ResponseFormat<List<CommonData>>();
            try
            {// 获取所有短信信息
                List<NoteManager> list = await noteService.GetNoteAsync();
                var query = from a in list
                            select new CommonData
                            {
                                NodeID = a.NodeID,
                                PhoneNumber = a.PhoneNumber,
                                VerificationCode=a.VerificationCode,
                                CreateTime=a.CreateTime
                            };
                //模糊查询
                if (!string.IsNullOrEmpty(PhoneNumber))
                {
                    query = query.Where(o => o.PhoneNumber.Contains(PhoneNumber)).ToList();
                }
                responseFormat.Data = query.OrderBy(o => o.NodeID).Skip((page - 1) * limit).Take(limit).ToList();
                responseFormat.Code = 200;
                responseFormat.Count = query.Count();
              
            }
            catch (Exception ex)
            {

                throw;
            }
            return responseFormat;
        }
    }
}
