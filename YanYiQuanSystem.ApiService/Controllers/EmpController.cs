﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EmpController : ControllerBase
    {
        private IEmpService EmpService { get; set; }//员工服务层
        private IRoleService roleService { get; set; }

        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis


        public EmpController(IRoleService roleService, IEmpService EmpService, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper)
        {
            this.EmpService = EmpService;
            this.roleService = roleService;
            this.logger = loggerFactory.CreateLogger<UsersController>();
            this.RedisDbHelper = redisDbHelper;
        }

        /// <summary>
        /// 异步获取所有员工信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Emp>>> GetEmpAsync() 
        {
            try
            {
                ResponseFormat<List<Emp>> response = new ResponseFormat<List<Emp>>();

                response.Data = await EmpService.GetEmpAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");
                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 获取所有角色
        /// </summary>
        [HttpGet]
        public async Task<ResponseFormat<List<Role>>> GetRoleAsync()
        {
            try
            {
                ResponseFormat<List<Role>> response = new ResponseFormat<List<Role>>();

                response.Data = await roleService.GetRoleAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");
                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 分页查询显示
        /// </summary>
        /// <param name="EmpName"></param>
        /// <param name="Phone"></param>
        /// <param name="UserName"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Emp>>> GetEmps(string EmpName="",string Phone="",string UserName="",int page=1,int limit =3)
         {
            ResponseFormat<List<Emp>> response = new ResponseFormat<List<Emp>>();
            List<Role> roles = await roleService.GetRoleAsync();
            List<Emp> emps = await EmpService.GetEmpAsync();
            var query = from e in emps
                        join r in roles on e.RoleId equals r.RoleId
                        select new Emp
                        {
                            EmpId = e.EmpId,
                            EmpName = e.EmpName,
                            Phone = e.Phone,
                            UserName = e.UserName,
                            IdentityCard = e.IdentityCard,
                            RoleId = r.RoleName,
                            UserPass = e.UserPass,
                            IsStates = e.IsStates
                        };
            var count = query.Count();
            if (!string.IsNullOrEmpty(EmpName))
            {
                query = query.Where(o => o.EmpName.Contains(EmpName)).ToList();
            }
            if (!string.IsNullOrEmpty(Phone))
            {
                query = query.Where(o => o.Phone.Contains(Phone)).ToList();
            }
            if (!string.IsNullOrEmpty(UserName))
            {
                query = query.Where(o => o.UserName.Contains(UserName)).ToList();
            }
            response.Data = query.OrderBy(o => o.EmpId).Skip((page - 1) * limit).Take(limit).ToList();
            response.Count = query.Count();
            return response;
        }

        /// <summary>
        /// 添加员工
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddEmp([FromBody]Emp emp)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            emp.EmpId = Guid.NewGuid().ToString();
            emp.RoleId = "597C1BB8-ECB9-49C4-9CBA-10492971EB40";
            bool n = EmpService.AddEmp(emp).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }
        /// <summary>
        /// 员工的删除
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>

        [HttpPost]
        public IActionResult DeleteEmp([FromBody]Emp emp)
        {
            string Id = emp.EmpId;
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = EmpService.DeleteEmp(Id).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }
        /// <summary>
        /// 根据员工Id获取角色的信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<Emp>> ShowEmpById(string id)
        {
            List<Emp> emps = await EmpService.GetEmpWhereAsync(o => o.EmpId.Equals(id));
            ResponseFormat<Emp> responseFormat = new ResponseFormat<Emp>();
            responseFormat.Data = emps.FirstOrDefault();
            return responseFormat;
        }
        /// <summary>
        /// 员工的修改
        /// </summary>
        [HttpPost]
        public IActionResult UpdateEmp(Emp emp)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = EmpService.UpdateEmp(emp).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "修改成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "修改失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);

        }
    }
}
