﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.IRepository;
    using YanYiQuanSystem.CommonUnitity;
    using System.Linq.Expressions;
    using Newtonsoft.Json;

    /// <summary>
    /// 认证api
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private IUsersService usersService { get; set; }
        private IRecruitmentLibraryService recruitmentLibraryService { get; set; }//招聘管理
        public IAuthentication_MTIService mtserver { get; set; }//模特管理
        public IAuthentication_GSIService gsserver { get; set; }//公司
        public IAssistantService Asserver { get; set; }//助理的server
        private IReCompanyService ReCompanyService { get; set; }
        public IModelLibraryService modelLibraryService { get; set; }
        public IUsersService userservice { get; set; }


        public AuthenticationController(IAuthentication_GSIService authentication_GSIService, IAuthentication_MTIService authentication_MTIService, IModelLibraryService modelLibraryService, IUsersService usersService, IRecruitmentLibraryService recruitmentLibraryService, IReCompanyService ReCompanyService, IAssistantService Asserver, IUsersService userservice)
        {
            this.mtserver = authentication_MTIService;
            this.gsserver = authentication_GSIService;
            this.modelLibraryService = modelLibraryService;
            this.usersService = usersService;
            this.recruitmentLibraryService = recruitmentLibraryService;
            this.ReCompanyService = ReCompanyService;
            this.Asserver = Asserver;
            this.userservice = userservice;
        }

        #region 线上认证模特


        /// <summary>
        /// 显示认证模特
        /// </summary>
        /// <param name="ModelName">姓名</param>
        /// <param name="PnoneNumber">手机号</param>
        /// <param name="IsIdentity">身份类型</param>
        /// <param name="Austate">认证状态</param>
        /// <param name="IdentityNumber">身份证号</param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>

        [HttpGet]
        public async Task<ResponseFormat<List<Authentication_MT>>> ShowMT(string ModelName = "", string PnoneNumber = "", int IsIdentity = -1, int Austate = -1, string IdentityNumber = "", string Standby1 = "", int page = 1, int limit = 2)
        {
            ResponseFormat<List<Authentication_MT>> responseFormats = new ResponseFormat<List<Authentication_MT>>();
            Expression<Func<Authentication_MT, bool>> ex = t => true;
            if (Standby1 != "")
            {
                ex = ex.And(t => t.Standby1 == Standby1);
            }

            if (ModelName != null)
            {
                ex = ex.And(t => t.ModelName.Contains(ModelName));
            }
            if (PnoneNumber != null)
            {
                ex = ex.And(t => t.PnoneNumber.Contains(PnoneNumber));
            }
            if (IsIdentity > 0)
            {
                ex = ex.And(t => t.IsIdentity.Equals(IsIdentity));
            }
            if (Austate > 0)
            {
                ex = ex.And(t => t.Austate.Equals(Austate));
            }
            if (IdentityNumber != null)
            {
                ex = ex.And(t => t.IdentityNumber.Contains(IdentityNumber));
            }


            List<Authentication_MT> authentication_s = await mtserver.GetListByPage(ex, o => o.AuMTid, page, limit);
            List<Authentication_MT> aucount = await mtserver.GetListByWhereAsync(ex);

            responseFormats.Data = authentication_s;
            responseFormats.Count = aucount.Count;
            return responseFormats;
        }

        /// <summary>
        /// 微信添加信息
        /// </summary>
        /// <param name="authentication_MT"></param>
        /// <returns></returns>
        [HttpPost]

        public IActionResult wxaddAuthenticationMT([FromForm] string nnn)
        {
            AuMTCommon auMTCommon = JsonConvert.DeserializeObject<AuMTCommon>(nnn);
            return addAuthenticationMT(auMTCommon);
        }

        /// <summary>
        /// 添加模特
        /// </summary>
        /// <param name="authentication_MT"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult addAuthenticationMT(AuMTCommon auMTCommon)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = false;
            bool m = false;
            List<User> users = userservice.GetUsreByWhere(o => o.PhoneNumber.Equals(auMTCommon.PnoneNumber)).Result;
            if (users.Count() > 0)
            {
                response.Code = 300;
                response.Msg = "改手机号已经注册了角色";
            }
            else
            {
                Authentication_MT authentication_MT = new Authentication_MT();//认证表的
                ModelsLibrary modelsLibrary = new ModelsLibrary();//模特表
                Assistant assistant = new Assistant();
                //给认证表添加
                authentication_MT.Austate = 1;//
                authentication_MT.Standby1 = "1";//
                assistant.UsersID = modelsLibrary.UserID = authentication_MT.AuMTid = Guid.NewGuid().ToString();//用户表id
                authentication_MT.ModelName = auMTCommon.ModelName;
                authentication_MT.PnoneNumber = auMTCommon.PnoneNumber;
                authentication_MT.IdentityNumber = auMTCommon.IdentityNumber;
                authentication_MT.IsIdentity = auMTCommon.IsIdentity;
                authentication_MT.Sex = auMTCommon.Sex;
                authentication_MT.zjZhaopian = auMTCommon.zjZhaopian;
                authentication_MT.Yingyezhizhao = auMTCommon.Yingyezhizhao;
                authentication_MT.AuditName = auMTCommon.AuditName;
                authentication_MT.Birthday = auMTCommon.Birthday;
                authentication_MT.HeadPortrait = auMTCommon.HeadPortrait;

                //给模特表添加
                if (auMTCommon.IsIdentity == 1)
                {
                    modelsLibrary.ModelId = Guid.NewGuid().ToString();
                    modelsLibrary.BloodType = auMTCommon.BloodType;
                    modelsLibrary.NickName = auMTCommon.NickName;
                    modelsLibrary.IsAssistant = 2;
                    modelsLibrary.IsFirstPage = 2;
                    modelsLibrary.RegisterTime = DateTime.Now;
                    modelsLibrary.MyCode = Guid.NewGuid().ToString().Substring(0, 6);//邀请码
                    modelsLibrary.Constellation = auMTCommon.Constellation;
                    modelsLibrary.Height = auMTCommon.Height;
                    modelsLibrary.Weight = auMTCommon.Weight;
                    modelsLibrary.Size = auMTCommon.Size;
                    modelsLibrary.TheChest = auMTCommon.TheChest;
                    modelsLibrary.Waistline = auMTCommon.Waistline;
                    modelsLibrary.Hipline = auMTCommon.Hipline;
                    modelsLibrary.LoginAccount = auMTCommon.PnoneNumber;
                    modelsLibrary.IsLogging1 = 1;
                    modelsLibrary.ApproveStatus = 2;
                    modelsLibrary.PhotoStatus = 1;
                    modelsLibrary.LoggingStatus = 1;
                    modelsLibrary.InvitationCode = auMTCommon.InvitationCode;



                    m = modelLibraryService.AddData(modelsLibrary).Result;

                }
                //给助理表添加
                if (auMTCommon.IsIdentity == 2)
                {
                    assistant.AssistantId = Guid.NewGuid().ToString();
                    assistant.InvitationCode = auMTCommon.InvitationCode;
                    assistant.MyCode = Guid.NewGuid().ToString().Substring(0, 6);//我的邀请码
                    assistant.RegisterTime = DateTime.Now;//注册时间
                    assistant.IsLogging = 1;//登录
                    m = Asserver.AddData(assistant).Result;

                }

                n = mtserver.AddData(authentication_MT).Result;

                if (n && m)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            return Ok(response);

        }



        /// <summary>
        /// 删除认证模特
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DelAuthenticationMT(string Id)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = mtserver.DeleteData(Id).Result;

            if (n)
            {
                response.Code = 200;
                response.Msg = "删除成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "删除失败";
            }
            return Ok(response);

        }

        /// <summary>
        /// 根据模特id获取模特的信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<Authentication_MT>> ShowMTById(string id = "9B369037-91C2-4A65-9A96-A0A636D1728")
        {

            List<Authentication_MT> authentication_s = await mtserver.GetListByWhereAsync(o => o.AuMTid.Equals(id));
            ResponseFormat<Authentication_MT> responseFormat = new ResponseFormat<Authentication_MT>();
            responseFormat.Data = authentication_s.FirstOrDefault();
            return responseFormat;


        }

        /// <summary>
        /// 微信小程序调用修改
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public async Task<ResponseFormat<bool>> wxUpdateModels([FromForm] string models)
        {
            CommonData commonData = JsonConvert.DeserializeObject<CommonData>(models);
            return await UpdateModels(commonData);
        }


        /// <summary>
        /// 修改模特数据
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateModels(CommonData models)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await mtserver.wxUpdateUserModel(models);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }





        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="authentication_MT"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult delnticationMT(string id = "ee26751b-7f39-417f-949d-630491b1dda4")
        {
            ResponseFormat<string> response = new ResponseFormat<string>();

            bool n = mtserver.DeleteData(id).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "删除成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "删除失败";
            }
            return Ok(response);
        }
        /// <summary>
        /// 修改通过状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Exitauthentication(string id, int isastu = 0)
        {
            //根据id获取模特信息
            Authentication_MT authentication_s = mtserver.GetListByWhereAsync(o => o.AuMTid.Equals(id)).Result.FirstOrDefault();
            if (isastu == 2)
            {
                authentication_s.Austate = 2;
            }
            if (isastu == 3)
            {
                authentication_s.Austate = 3;
            }


            ResponseFormat<string> response = new ResponseFormat<string>();

            bool n = mtserver.UpdateData(authentication_s).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "认证成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "认证失败";
            }
            return Ok(response);
        }
        /// <summary>
        /// 线上转线下
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult onlinezhuanxai(string id, string Standby1)
        {
            //根据id获取模特信息
            Authentication_MT authentication_s = mtserver.GetListByWhereAsync(o => o.AuMTid.Equals(id)).Result.FirstOrDefault();
            if (Standby1 == "1")
            {
                authentication_s.Standby1 = "2";
            }
            if (Standby1 == "2")
            {
                authentication_s.Standby1 = "1";
            }


            ResponseFormat<string> response = new ResponseFormat<string>();

            bool n = mtserver.UpdateData(authentication_s).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "失败";
            }
            return Ok(response);
        }


        public IActionResult AddUserMT(string id)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            //根据认证模特表id
            Authentication_MT authentication_MT = mtserver.GetListByWhereAsync(o => o.AuMTid.Equals(id)).Result.FirstOrDefault();

            User user = new User();
            user.UserId = authentication_MT.AuMTid;
            user.UserName = authentication_MT.ModelName;
            user.Password = authentication_MT.PnoneNumber;
            user.Sex = authentication_MT.Sex;
            if (authentication_MT.IsIdentity == 1)
            {
                user.CurrentRole = 1;
            }
            if (authentication_MT.IsIdentity == 2)
            {
                user.CurrentRole = 4;
            }


            user.Birthday = authentication_MT.Birthday;
            user.HeadPortrait = authentication_MT.HeadPortrait;
            user.IdentityNumber = authentication_MT.IdentityNumber;
            user.PhoneNumber = authentication_MT.PnoneNumber;
            user.PresentAddress = authentication_MT.PresentAddress;
            user.UsersRank = "1";
            //添加用户信息
            bool n = usersService.AddData(user).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "添加成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "添加失败";
            }
            return Ok(response);
        }
        #endregion


        #region 线上认证公司
        /// <summary>
        /// 显示认证公司
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Authentication_GS>>> ShowGS(string Name = "", string PnoneNumber = "", int Austate = -1, string IdentityNumber = "", string Standby1 = "", int page = 1, int limit = 2)
        {
            ResponseFormat<List<Authentication_GS>> responseFormats = new ResponseFormat<List<Authentication_GS>>();
            Expression<Func<Authentication_GS, bool>> ex = t => true;

            if (!string.IsNullOrEmpty(Standby1))
            {
                ex = ex.And(t => t.Standby1 == Standby1);
            }
            if (!string.IsNullOrEmpty(Name))
            {
                ex = ex.And(t => t.Name.Contains(Name));
            }
            if (!string.IsNullOrEmpty(PnoneNumber))
            {
                ex = ex.And(t => t.PnoneNumber.Contains(PnoneNumber));

            }
            if (!string.IsNullOrEmpty(IdentityNumber))
            {
                ex = ex.And(t => t.IdentityNumber.Contains(IdentityNumber));

            }
            if (Austate > 0)
            {
                ex = ex.And(t => t.Austate.Equals(Austate));
            }
            List<Authentication_GS> authentication_s = await gsserver.GetListByPage(ex, o => o.AuGSid, page, limit);
            List<Authentication_GS> aucount = await gsserver.GetListByWhereAsync(ex);
            responseFormats.Data = authentication_s;
            responseFormats.Count = aucount.Count;
            return responseFormats;
        }


        /// <summary>
        /// 微信小程序添加公司
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult wxaddAuthenticationGS([FromForm] string model)
        {
            AuGScommon auGScommon = JsonConvert.DeserializeObject<AuGScommon>(model);
            return addAuthenticationGS(auGScommon);
        }


        /// <summary>
        /// 添加公司
        /// </summary>
        /// <param name="authentication_MT"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult addAuthenticationGS(AuGScommon auGScommon)
        {
            Authentication_GS authentication_GS = new Authentication_GS();
            RecruitmentCompanys recruitmentCompanys = new RecruitmentCompanys();
            ResponseFormat<string> response = new ResponseFormat<string>();

            List<User> users = userservice.GetUsreByWhere(o => o.PhoneNumber.Equals(auGScommon.PnoneNumber)).Result;
            if (users.Count() > 0)
            {
                response.Code = 300;
                response.Msg = "该手机号已经注册了角色";
            }
            else
            {

                authentication_GS.Austate = 1;
                //authentication_GS.AuditTime = DateTime.Now;
                recruitmentCompanys.UsersID = authentication_GS.AuGSid = Guid.NewGuid().ToString();
                authentication_GS.GSHeadPortraits = auGScommon.GSHeadPortraits;
                authentication_GS.Name = auGScommon.Name;
                authentication_GS.PnoneNumber = auGScommon.PnoneNumber;
                authentication_GS.IdentityNumber = auGScommon.IdentityNumber;
                authentication_GS.IsIdentity = auGScommon.CompanyType;
                authentication_GS.Austate = 1;
                authentication_GS.Sex = auGScommon.Sex;
                authentication_GS.GSName = auGScommon.CompanyName;
                authentication_GS.Legalperson = auGScommon.CompanyPerson;
                authentication_GS.GSdizhi = auGScommon.CompanyAddress;
                authentication_GS.Taxpayer = auGScommon.TaxpayerNum;
                authentication_GS.GSemail = auGScommon.GSemail;


                //公司表
                recruitmentCompanys.HrId = Guid.NewGuid().ToString();
                recruitmentCompanys.InvitationCode = auGScommon.InvitationCode;
                recruitmentCompanys.CompanyType = Convert.ToInt32(auGScommon.CompanyType);
                recruitmentCompanys.CompanyName = auGScommon.CompanyName;
                recruitmentCompanys.CompanyPhone = auGScommon.CompanyPhone;
                recruitmentCompanys.CompanyPerson = auGScommon.CompanyPerson;
                recruitmentCompanys.CompanyAddress = auGScommon.CompanyAddress;
                recruitmentCompanys.TaxpayerNum = auGScommon.TaxpayerNum;
                recruitmentCompanys.WorkPosition = auGScommon.WorkPosition;

                recruitmentCompanys.MyCode = Guid.NewGuid().ToString().Substring(0, 6);
                recruitmentCompanys.LoggingStatus = 1;//允许登录
                recruitmentCompanys.RegisterTime = DateTime.Now;//注册时间
                recruitmentCompanys.ApproveState = 2;


                bool n = gsserver.AddData(authentication_GS).Result;
                bool m = ReCompanyService.AddData(recruitmentCompanys).Result;
                if (n && m)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            return Ok(response);
        }



        /// <summary>
        /// 删除认证公司信息
        /// </summary>
        /// <param name="authentication_MT"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DelAuthenticationGS(string Id)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();

                bool n = gsserver.DeleteData(Id).Result;
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            return Ok(response);
        }


        /// <summary>
        /// 根据公司id获取公司的信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<Authentication_GS>> ShowGSById(string id)
        {
            List<Authentication_GS> authentication_s = await gsserver.GetListByWhereAsync(o => o.AuGSid.Equals(id));
            ResponseFormat<Authentication_GS> responseFormat = new ResponseFormat<Authentication_GS>();
            responseFormat.Data = authentication_s.FirstOrDefault();
            return responseFormat;
        }

        /// <summary>
        /// 微信小程序调用修改公司
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public async Task<ResponseFormat<bool>> wxgsUpdateModels([FromForm] string models)
        {
            CommonData commonData = JsonConvert.DeserializeObject<CommonData>(models);
            return await gsUpdateModels(commonData);

        }
        /// <summary>
        /// 修改模特数据
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> gsUpdateModels(CommonData models)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await gsserver.UpdateUserModel(models);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// 微信小程序调用修改助理
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public async Task<ResponseFormat<bool>> wxzlUpdateModels([FromForm] string models)
        {
            CommonData commonData = JsonConvert.DeserializeObject<CommonData>(models);
            return await zlUpdateModels(commonData);

        }
        /// <summary>
        /// 修改模特数据
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> zlUpdateModels(CommonData models)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await gsserver.UpdateUserModelZL(models);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 修改通过拒绝状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ExitauthenticationGS(string id, int isastu = 0, string name = "")
        {
            //根据id获取模特信息
            Authentication_GS authentication_s = gsserver.GetListByWhereAsync(o => o.AuGSid.Equals(id)).Result.FirstOrDefault();
            if (isastu == 2)
            {
                authentication_s.Austate = 2;
            }
            if (isastu == 3)
            {
                authentication_s.Austate = 3;
            }
            authentication_s.AuditTime = DateTime.Now;
            authentication_s.AuditName = name;
            ResponseFormat<string> response = new ResponseFormat<string>();

            bool n = gsserver.UpdateData(authentication_s).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "认证成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "认证失败";
            }
            return Ok(response);
        }

        /// <summary>
        /// 认证成功添加到user表中
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddUsergs(string id)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            //根据认证模特表id
            Authentication_GS authentication_MT = gsserver.GetListByWhereAsync(o => o.AuGSid.Equals(id)).Result.FirstOrDefault();

            User user = new User();
            user.UserId = authentication_MT.AuGSid;
            user.UserName = authentication_MT.Name;
            user.Password = authentication_MT.PnoneNumber;
            user.Sex = authentication_MT.Sex;
            if (authentication_MT.IsIdentity == 1)// 1: 模特 2:企业  3: 个人企业  4: 助理
            {
                user.CurrentRole = 3;
            }
            if (authentication_MT.IsIdentity == 2)
            {
                user.CurrentRole = 2;
            }


            user.HeadPortrait = authentication_MT.GSHeadPortraits;
            user.IdentityNumber = authentication_MT.IdentityNumber;
            user.PhoneNumber = authentication_MT.PnoneNumber;
            //添加用户信息
            bool n = usersService.AddData(user).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "添加成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "添加失败";
            }
            return Ok(response);
        }
        /// <summary>
        /// 线上转线下
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult onlinezhuanxaiGS(string id, string Standby1)
        {
            //根据id获取模特信息
            Authentication_GS authentication_s = gsserver.GetListByWhereAsync(o => o.AuGSid.Equals(id)).Result.FirstOrDefault();
            if (Standby1 == "1")
            {
                authentication_s.Standby1 = "2";
            }
            if (Standby1 == "2")
            {
                authentication_s.Standby1 = "1";
            }


            ResponseFormat<string> response = new ResponseFormat<string>();

            bool n = gsserver.UpdateData(authentication_s).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "失败";
            }
            return Ok(response);
        }

        #endregion


        #region 认证公司招聘

        /// <summary>
        /// 添加公司招聘
        /// </summary>
        /// <param name="authentication_MT"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult addRecruitmentLibrary(RecruitmentLibrary recruitment)
        {
            recruitment.AuditStatus = 2;//未审核
            recruitment.CreateTime = DateTime.Now;
            recruitment.RecLibraryId = Guid.NewGuid().ToString();

            ResponseFormat<string> response = new ResponseFormat<string>();

            bool n = recruitmentLibraryService.AddData(recruitment).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "添加成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "添加失败";
            }
            return Ok(response);
        }

        /// <summary>
        ///  显示招聘计划认证
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<RecruitmentLibrary>>> ShowRecruitmentLibrary(int page = 1, int limit = 2, string CompanyName = "",
            int PostSex = 0, int MinSalary = 0, int MaxSalary = 0, int AuditStatus = 0)
        {
            ResponseFormat<List<RecruitmentLibrary>> responseFormats = new ResponseFormat<List<RecruitmentLibrary>>();
            Expression<Func<RecruitmentLibrary, bool>> ex = t => true;

            if (!string.IsNullOrEmpty(CompanyName))
            {
                ex = ex.And(t => t.CompanyName.Contains(CompanyName));
            }
            if (PostSex != 0)
            {
                ex = ex.And(t => t.PostSex.Equals(PostSex));
            }
            if (MinSalary != 0)
            {
                ex = ex.And(t => t.PostSex.Equals(MinSalary));
            }
            if (MaxSalary != 0)
            {
                ex = ex.And(t => t.PostSex.Equals(MaxSalary));
            }
            if (AuditStatus > 0)
            {
                ex = ex.And(t => t.AuditStatus.Equals(AuditStatus));
            }



            List<RecruitmentLibrary> authentication_s = await recruitmentLibraryService.GetListByPage(ex, o => o.RecLibraryId, page, limit);
            List<RecruitmentLibrary> aucount = await recruitmentLibraryService.GetListByWhereAsync(ex);

            responseFormats.Data = authentication_s;
            responseFormats.Count = aucount.Count;
            return responseFormats;
        }



        /// <summary>
        /// 通过认证
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ExitRecruitmentLibrary(string id, int isastu)
        {
            //根据id获取模特信息
            RecruitmentLibrary recruitment = recruitmentLibraryService.GetListByWhereAsync(o => o.RecLibraryId.Equals(id)).Result.FirstOrDefault();

            recruitment.AuditStatus = isastu;

            ResponseFormat<string> response = new ResponseFormat<string>();

            bool n = recruitmentLibraryService.UpdateData(recruitment).Result;
            if (n)
            {
                response.Code = 200;
                response.Msg = "认证成功";
            }
            else
            {
                response.Code = 300;
                response.Msg = "认证失败";
            }
            return Ok(response);
        }


        /// <summary>
        /// 根据模特id获取模特的信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<RecruitmentLibrary>> ShowzhaopinById(string id)
        {
            List<RecruitmentLibrary> authentication_s = await recruitmentLibraryService.GetListByWhereAsync(o => o.RecLibraryId.Equals(id));
            ResponseFormat<RecruitmentLibrary> responseFormat = new ResponseFormat<RecruitmentLibrary>();
            responseFormat.Data = authentication_s.FirstOrDefault();
            return responseFormat;


        }


        #endregion


        /// <summary>
        /// 添加招聘库信息
        /// </summary>
        [HttpPost]
        public ResponseFormat<bool> PostAddRecruitmentLibrary([FromForm] string recruitmentLibrary)
        {
            //转换成json格式
            RecruitmentLibrary recruitmentLibrary1 = JsonConvert.DeserializeObject<RecruitmentLibrary>(recruitmentLibrary);
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {
                //生成guid
                recruitmentLibrary1.RecLibraryId = Guid.NewGuid().ToString();
                bool m = recruitmentLibraryService.AddData(recruitmentLibrary1).Result;
                if (m)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "添加成功";
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "添加失败";
                }
                return responseFormat;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 修改所有用户头像
        /// </summary>
        /// <param name="recruitmentLibrary"></param>
        /// <returns></returns>
        public ResponseFormat<bool> exitorimg([FromForm] string id, [FromForm] string img)
        {
            //转换成json格式
            //CommonData commonData = JsonConvert.DeserializeObject<CommonData>(data);
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {

                bool m = mtserver.wxExitorImg(id, img).Result;
                if (m)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "修改成功";
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "修改失败";
                }
                return responseFormat;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
