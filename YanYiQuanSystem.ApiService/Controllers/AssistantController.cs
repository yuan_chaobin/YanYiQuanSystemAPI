﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.Service;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AssistantController : ControllerBase
    {
        private IAssistantService AssistantService { get; set; }
        private IUsersService UsersService { get; set; }
        private IAssistant_ModelService Assistant_ModelService { get; set; }
        private IModelLibraryService ModelLibraryService { get; set; }

        public AssistantController(IAssistantService assistantService, IUsersService usersService, IAssistant_ModelService assistant_ModelService, IModelLibraryService modelLibraryService)
        {
            this.AssistantService = assistantService;
            this.UsersService = usersService;
            this.Assistant_ModelService = assistant_ModelService;
            this.ModelLibraryService = modelLibraryService;
        }


        /// <summary>
        /// 异步获取所有助理列表数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetAssistant(string name="",string phone="",string idCard="")
        {
            try
            {
                ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

                List<Assistant> asList = await AssistantService.GetListAsync();
                List<User> userList = await UsersService.GetUsersAsync();

                var query = from a in asList
                            join b in userList on a.UsersID equals b.UserId
                            where b.CurrentRole == 4 && a.IsLogging == 1
                            select new CommonData
                            {
                                AssistantId = a.AssistantId,
                                UsersID = a.UsersID,
                                MyCode = a.MyCode,
                                InvitationCode = a.InvitationCode,
                                RegisterTime = a.RegisterTime,
                                IsLogging = a.IsLogging,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = b.Birthday,
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };
                //姓名
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(o => o.UserName.Contains(name)).ToList();
                }
                //手机号
                if (!string.IsNullOrEmpty(phone))
                {
                    query = query.Where(o => o.PhoneNumber == phone).ToList();
                }
                //身份证号
                if (!string.IsNullOrEmpty(idCard))
                {
                    query = query.Where(o => o.IdentityNumber == idCard).ToList();
                }
                response.Data = query.ToList();
                response.Count = response.Data.Count();
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// 删除助理列表
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<bool>> DeleteAssistant(string Id)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await AssistantService.DeleteData(Id);
                response.Code = 200;
                response.Data = result;
                response.Msg = "删除成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 修改助理列表
        /// </summary>
        /// <param name="commonData"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateAssistant(CommonData commonData)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await AssistantService.UpdateAssistant(commonData);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 获取每个助理所管理的模特
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetAssistantModel(string assId,string address="",string name="",string phone="",string idCard="")
        {
            try
            {
                ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

                List<User> users = await UsersService.GetUsersAsync();//用户
                List<ModelsLibrary> mlList = await ModelLibraryService.GetListAsync();//模特列表
                List<Assistant_Model> models = await Assistant_ModelService.GetListAsync();//
                List<Assistant> assistants = await AssistantService.GetListAsync();

                var query = from a in users
                            join b in mlList on a.UserId equals b.UserID
                            join c in models on b.ModelId equals c.ModelID
                            join d in assistants on c.AssistantID equals d.AssistantId where d.AssistantId == assId
                            select new CommonData
                            {
                                ModelId = b.ModelId,
                                Assistant_ModelId = c.Assistant_ModelId,
                                AssistantId = d.AssistantId,
                                AssistantID = c.AssistantID,
                                ModelID = c.ModelID,
                                HeadPortrait = a.HeadPortrait,
                                UserName = a.UserName,
                                PhoneNumber = a.PhoneNumber,
                                Sex = a.Sex,
                                Birthday = a.Birthday,
                                PresentAddress = a.PresentAddress,
                                Height = b.Height,
                                Constellation = b.Constellation,
                                NickName = b.NickName,
                                IdentityNumber = a.IdentityNumber,
                                Weight = b.Weight,
                                LoginAccount = b.LoginAccount,
                                IsLogging1 = b.IsLogging1
                            };
                if (!string.IsNullOrEmpty(address))
                {
                    query = query.Where(o => o.PresentAddress.Contains(address)).ToList();
                }
                //姓名
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(o => o.UserName.Contains(name)).ToList();
                }
                //手机号
                if (!string.IsNullOrEmpty(phone))
                {
                    query = query.Where(o => o.PhoneNumber == phone).ToList();
                }
                //身份证号
                if (!string.IsNullOrEmpty(idCard))
                {
                    query = query.Where(o => o.IdentityNumber == idCard).ToList();
                }
                response.Data = query.ToList();
                response.Count = response.Data.Count();
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
