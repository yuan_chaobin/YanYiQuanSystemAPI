﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    
    [ApiController]
    public class ActivityController : ControllerBase
    {
        private IServiceActivity serviceActivity { get; set; }//用户服务层
        private IServiceActity_user serviceActity_User { get; set; }//用户服务层
        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis


        public ActivityController(IServiceActivity serviceActivity, IServiceActity_user serviceActity_User,ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper)
        {
            this.serviceActivity = serviceActivity;
            this.serviceActity_User = serviceActity_User;
            this.logger = loggerFactory.CreateLogger<UsersController>();
            this.RedisDbHelper = redisDbHelper;
        }

        /// <summary>
        /// 异步获取所有用户信息
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityList")]
        [HttpGet]
        public async Task<ResponseFormat<List<Activity>>> GetUsersAsync()
        {
            try
            {
                ResponseFormat<List<Activity>> response = new ResponseFormat<List<Activity>>();

                response.Data = await serviceActivity.GetListAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 异步获取分页信息
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityListPage")]
        [HttpGet]
        public async Task<ResponseFormat<List<Activity>>> GetActivityListPage(int page = 1, int limit = 2,string Desc_activity="",string category_activity="",DateTime? time_from_baoming=null, DateTime? time_to_baoming =null, DateTime? time_from_xuanba = null, DateTime? time_to_xuanba = null,string order_activity="",string order="")
        {
            try
            {
                ResponseFormat<List<Activity>> response = new ResponseFormat<List<Activity>>();
                List<Activity> activitys= await serviceActivity.GetListAsync();
                if (!string.IsNullOrEmpty(Desc_activity))
                {
                    activitys = activitys.Where(o => o.Desc_activity.Contains(Desc_activity)).ToList();
                }
                if (category_activity=="常规活动"|| category_activity == "非常规活动")
                {
                    activitys = activitys.Where(o => o.Category_activity== category_activity).ToList();
                }
                if (time_from_baoming!=null)
                {
                    activitys = activitys.Where(o => o.Time_from_baoming> time_from_baoming).ToList();
                }
                if (time_to_baoming != null)
                {
                    activitys = activitys.Where(o => o.Time_to_baoming < time_to_baoming).ToList();
                }
                if (time_from_xuanba != null)
                {
                    activitys = activitys.Where(o => o.Time_from_xuanba > time_from_xuanba).ToList();
                }
                if (time_to_xuanba != null)
                {
                    activitys = activitys.Where(o => o.Time_to_xuanba < time_to_xuanba).ToList();
                }
                if (order_activity != null && order != null)
                {
                    if (order_activity == "活动主题" && order == "顺序")
                    {
                        response.Data = activitys.OrderBy(o => o.Desc_activity).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "活动选拔时间" && order == "顺序")
                    {
                        response.Data = activitys.OrderBy(o => o.Time_from_xuanba).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "活动主题" && order == "倒序")
                    {
                        response.Data = activitys.OrderByDescending(o => o.Desc_activity).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "活动选拔时间" && order == "倒序")
                    {
                        response.Data = activitys.OrderByDescending(o => o.Time_from_xuanba).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                }
                else {
                    response.Data = activitys.OrderBy(o => o.Desc_activity).Skip((page - 1) * limit).Take(limit).ToList();
                }
                response.Code = 0;
                response.Count = activitys.Count();
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 异步获取人数未达上限的活动分页信息
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityListPageNoM")]
        [HttpGet]
        public async Task<ResponseFormat<List<Activity>>> GetActivityListPageNoM(int page = 1, int limit = 2, string Desc_activity = "", string category_activity = "", DateTime? time_from_baoming = null, DateTime? time_to_baoming = null, DateTime? time_from_xuanba = null, DateTime? time_to_xuanba = null, string order_activity = "", string order = "")
        {
            try
            {
                ResponseFormat<List<Activity>> response = new ResponseFormat<List<Activity>>();
                List<Activity> activitys = await serviceActivity.GetListAsync();
                List<Activity> nActivitys = new List<Activity>();
                foreach (var item in activitys)
                {
                    int count = serviceActity_User.GetListByWhereAsync(r => r.Id_activity == item.Uuid).Result.Count;
                    if (count >= item.Count_max)
                    {
                        nActivitys.Add(item);
                    }
                }
                foreach (var item in nActivitys)
                {
                    activitys.Remove(item);
                }
                if (!string.IsNullOrEmpty(Desc_activity))
                {
                    activitys = activitys.Where(o => o.Desc_activity.Contains(Desc_activity)).ToList();
                }
                if (category_activity == "常规活动" || category_activity == "非常规活动")
                {
                    activitys = activitys.Where(o => o.Category_activity == category_activity).ToList();
                }
                if (time_from_baoming != null)
                {
                    activitys = activitys.Where(o => o.Time_from_baoming > time_from_baoming).ToList();
                }
                if (time_to_baoming != null)
                {
                    activitys = activitys.Where(o => o.Time_to_baoming < time_to_baoming).ToList();
                }
                if (time_from_xuanba != null)
                {
                    activitys = activitys.Where(o => o.Time_from_xuanba > time_from_xuanba).ToList();
                }
                if (time_to_xuanba != null)
                {
                    activitys = activitys.Where(o => o.Time_to_xuanba < time_to_xuanba).ToList();
                }
                if (order_activity != null && order != null)
                {
                    if (order_activity == "活动主题" && order == "顺序")
                    {
                        response.Data = activitys.OrderBy(o => o.Desc_activity).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "活动选拔时间" && order == "顺序")
                    {
                        response.Data = activitys.OrderBy(o => o.Time_from_xuanba).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "活动主题" && order == "倒序")
                    {
                        response.Data = activitys.OrderByDescending(o => o.Desc_activity).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "活动选拔时间" && order == "倒序")
                    {
                        response.Data = activitys.OrderByDescending(o => o.Time_from_xuanba).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                }
                else
                {
                    response.Data = activitys.OrderBy(o => o.Desc_activity).Skip((page - 1) * limit).Take(limit).ToList();
                }
                response.Code = 0;
                response.Count = activitys.Count();
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 异步获取一条活动
        /// </summary>
        /// <returns></returns>
        [Route("GetActivity")]
        [HttpGet]
        public async Task<ResponseFormat<Activity>> GetActivity(string Id)
        {
            try
            {
                ResponseFormat<Activity> response = new ResponseFormat<Activity>();

                List<Activity> activities = await serviceActivity.GetListByWhereAsync(o => o.Uuid == Id);
                response.Data = activities.FirstOrDefault();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取一条数据");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 添加活动
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("AddActivity")]
        [HttpPost]
        public async Task<ResponseFormat<string>> AddActivity([FromBody]Activity activity)
        {
            activity.Uuid = Guid.NewGuid().ToString();
            ResponseFormat<string> response = new ResponseFormat<string>();
            if(activity.Time_from_baoming <DateTime.Now)
            {
                response.Code = 300;
                response.Msg = "报名开始时间不能小于当前时间";
                return response;
            }
            bool n = await serviceActivity.AddData(activity);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 修改活动
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("EditActivity")]
        [HttpPost]
        public async Task<ResponseFormat<string>> EditActivity([FromBody]Activity activity)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActivity.UpdateData(activity);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "修改成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "修改失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 批量添加活动
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("AddActivityList")]
        [HttpPost]
        public async Task<ResponseFormat<string>> AddActivityList([FromBody]List<Activity> activitys)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActivity.BatchAddData(activitys);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 删除活动
        /// </summary>
        /// <param name="activity"></param>
        /// <returns></returns>
        [Route("DeleteActivity")]
        [HttpPost]
        public async Task<ResponseFormat<string>> DeleteActivity([FromBody]Activity activity)
        {
            string Id = activity.Uuid;
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActivity.DeleteData(Id);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 批量删除活动
        /// </summary>
        /// <param name="activitys"></param>
        /// <returns></returns>
        [Route("DeleteActivityList")]
        [HttpPost]
        public async Task<ResponseFormat<string>> DeleteActivityList([FromBody]List<string> Ids)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActivity.BatchDelete(Ids);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
    }
}