﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    [ApiController]
    public class ActivityreportController : ControllerBase
    {
        private IServiceActity_report serviceActity_User { get; set; }//用户服务层
        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis


        public ActivityreportController(IServiceActity_report serviceActity_User, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper)
        {
            this.serviceActity_User = serviceActity_User;
            this.logger = loggerFactory.CreateLogger<UsersController>();
            this.RedisDbHelper = redisDbHelper;
        }

        /// <summary>
        /// 异步获取所有用户信息
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityListreports")]
        [HttpGet]
        public async Task<ResponseFormat<List<Activity_report>>> GetUsersAsync()
        {
            try
            {
                ResponseFormat<List<Activity_report>> response = new ResponseFormat<List<Activity_report>>();
                response.Data = await serviceActity_User.GetListAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 异步获取分页信息
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityreportsListPage")]
        [HttpGet]
        public async Task<ResponseFormat<List<Activity_report>>> GetActivityListPage(int page = 1, int limit = 2, DateTime? Time_trans = null, string Id_user = "", string Id_user_report_by = "", string order_activity = "", string order = "")
        {
            try
            {
                ResponseFormat<List<Activity_report>> response = new ResponseFormat<List<Activity_report>>();
                List<Activity_report> activitys = await serviceActity_User.GetListAsync();
                if (Time_trans!=null)
                {
                    activitys = activitys.Where(o => o.Time_trans== Time_trans).ToList();
                }
                if (!string.IsNullOrEmpty(Id_user))
                {
                    activitys = activitys.Where(o => o.Id_user.Contains(Id_user)).ToList();
                }
                if (!string.IsNullOrEmpty(Id_user_report_by))
                {
                    activitys = activitys.Where(o => o.Id_user_report_by.Contains(Id_user_report_by)).ToList();
                }
                if (order_activity != null && order != null)
                {
                    if (order_activity == "举报时间" && order == "顺序")
                    {
                        response.Data = activitys.OrderBy(o => o.Time_trans).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "参赛人" && order == "顺序")
                    {
                        response.Data = activitys.OrderBy(o => o.Id_user).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "举报时间" && order == "倒序")
                    {
                        response.Data = activitys.OrderByDescending(o => o.Time_trans).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                    if (order_activity == "参赛人" && order == "倒序")
                    {
                        response.Data = activitys.OrderByDescending(o => o.Id_user).Skip((page - 1) * limit).Take(limit).ToList();
                    }
                }
                else
                {
                response.Data = activitys.OrderBy(o => o.Uuid).Skip((page - 1) * limit).Take(limit).ToList();
                }
                response.Code = 0;
                response.Count = activitys.Count();
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 异步获取一条活动用户
        /// </summary>
        /// <returns></returns>
        [Route("GetActivityreports")]
        [HttpGet]
        public async Task<ResponseFormat<Activity_report>> GetActivity(string Id)
        {
            try
            {
                ResponseFormat<Activity_report> response = new ResponseFormat<Activity_report>();

                List<Activity_report> activities = await serviceActity_User.GetListByWhereAsync(o => o.Uuid == Id);
                response.Data = activities.FirstOrDefault();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取一条数据");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 添加活动用户
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("AddActivityrs")]
        [HttpPost]
        public async Task<ResponseFormat<string>> AddActivityrs([FromBody]Activity_report activity)
        {
            activity.Uuid = Guid.NewGuid().ToString();
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.AddData(activity);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 修改活动用户
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("EditActivityreports")]
        [HttpPost]
        public async Task<ResponseFormat<string>> EditActivity([FromBody]Activity_report activity)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.UpdateData(activity);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "修改成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "修改失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 批量添加活动用户
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [Route("AddActivityListreports")]
        [HttpPost]
        public async Task<ResponseFormat<string>> AddActivityList([FromBody]List<Activity_report> activitys)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.BatchAddData(activitys);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 删除活动用户
        /// </summary>
        /// <param name="activity"></param>
        /// <returns></returns>
        [Route("DeleteActivityreport")]
        [HttpPost]
        public async Task<ResponseFormat<string>> DeleteActivity([FromBody]Activity_report activity)
        {
            string Id = activity.Uuid;
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.DeleteData(Id);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
        /// <summary>
        /// 批量删除活动用户
        /// </summary>
        /// <param name="activitys"></param>
        /// <returns></returns>
        [Route("DeleteActivityreports")]
        [HttpPost]
        public async Task<ResponseFormat<string>> DeleteActivityList([FromBody]List<string> Ids)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = await serviceActity_User.BatchDelete(Ids);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = ex.Message;
            }
            return response;
        }
    }
}