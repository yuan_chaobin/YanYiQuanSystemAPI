﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    using Newtonsoft.Json;
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUsersService UsersService { get; set; }//用户服务层
        private readonly ILogger logger;    //NLog
        private IRoleService roleService { get; set; }//注入角色
        private RedisDbHelper RedisDbHelper { get; set; } //redis
        private IEmpService empService { get; set; }


        public UsersController(IUsersService usersService, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper, IRoleService roleService, IEmpService empService)
        {
            this.UsersService = usersService;
            this.logger = loggerFactory.CreateLogger<UsersController>();
            this.RedisDbHelper = redisDbHelper;
            this.roleService = roleService;
            this.empService = empService;
        }
        /// <summary>
        /// 员工登录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<Emp>> GetEmp(Emp emp)
        {
            ResponseFormat<Emp> responseFormat = new ResponseFormat<Emp>();
            try
            {
                List<Emp> list = await empService.GetEmpWhereAsync(o => o.UserName == emp.UserName && o.UserPass == emp.UserPass);
                responseFormat.Data = list.FirstOrDefault();
                responseFormat.Code = 200;
                responseFormat.Msg = "获取信息成功";
                return responseFormat;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 异步获取所有用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<User>>> GetUsersAsync()
        {
            try
            {
                ResponseFormat<List<User>> response = new ResponseFormat<List<User>>();

                response.Data = await UsersService.GetUsersAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 手机号查询
        /// </summary>
        /// <param name="PhoneNumber"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<User>>> GetUser(string PhoneNumber = "", int page = 1, int limit = 3)
        {
            ResponseFormat<List<User>> responseFormat = new ResponseFormat<List<User>>();
            try
            {
                var query = await UsersService.GetUsersAsync();
                if (!string.IsNullOrEmpty(PhoneNumber))
                {
                    query = query.Where(o => o.PhoneNumber.Contains(PhoneNumber)).ToList();
                }
                responseFormat.Data = query.OrderBy(o => o.UserId).Skip((page - 1) * limit).Take(limit).ToList();
                responseFormat.Count = query.Count();
                return responseFormat;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        /// <summary>
        /// 前台用户登录 手机号加密码登录
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name=""></param>
        /// <returns></returns>

        [HttpPost]
        public async Task<ResponseFormat<User>> LoginUser([FromForm] string user)
        {
            ResponseFormat<User> responseFormat = new ResponseFormat<User>();
            try
            {
                //转换成json格式
                User user1 = JsonConvert.DeserializeObject<User>(user);

                List<User> users = await UsersService.GetUsreByWhere(o => o.PhoneNumber == user1.PhoneNumber && o.Password == user1.Password);

                responseFormat.Data = users.FirstOrDefault();

                responseFormat.Code = 200;
                responseFormat.Count = users.Count;
                return responseFormat;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 手机号查询
        /// </summary>
        /// <param name="PhoneNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<User>>> GetUsersByPhone(string phone = "", int pageIndex = 1, int pageSize = 3)
        {
            ResponseFormat<List<User>> responseFormat = new ResponseFormat<List<User>>();
            try
            {
                List<User> users = await UsersService.GetUsersAsync();
                if (!string.IsNullOrEmpty(phone))
                {
                    users = users.Where(o => o.PhoneNumber == phone).ToList();
                }
                responseFormat.Data = users.OrderBy(o => o.UserId).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                responseFormat.Count = users.Count();
                return responseFormat;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


    }
}
