﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    using System.Linq;
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsefulExpressionsManagerController : ControllerBase
    {
        public IUsefulExpressionsManagerService usefulExpressionsManagerService { get; set; }
        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis
        public UsefulExpressionsManagerController(IUsefulExpressionsManagerService usefulExpressionsManagerService, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper)
        {
            this.usefulExpressionsManagerService = usefulExpressionsManagerService;
            this.logger = loggerFactory.CreateLogger<MenuSystemController>();
            this.RedisDbHelper = redisDbHelper;
        }
        /// <summary>
        /// 获取常用语数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetUsefulExpressionsManager(string UsefulExpressionsManagerName = "", int page = 1, int limit = 2)
        {
            //获取所有菜单信息
            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();
            try
            {
               List<UsefulExpressionsManager> usefulExpressionsManagers = await usefulExpressionsManagerService.GetUsefulExpressionsManagerAsync();
                var query = from a in usefulExpressionsManagers
                            select new CommonData
                            {
                                UsefulExpressionsManagerID = a.UsefulExpressionsManagerID,
                                UsefulExpressionsManagerName = a.UsefulExpressionsManagerName,
                                UsefulOrderId=a.UsefulOrderId
                            };
                logger.LogInformation("获取数据成功");
                //模糊查询
                if (!string.IsNullOrEmpty(UsefulExpressionsManagerName))
                {
                    query = query.Where(o => o.UsefulExpressionsManagerName.Contains(UsefulExpressionsManagerName)).ToList();
                }
                response.Data = query.OrderBy(o => o.UsefulOrderId).Skip((page - 1) * limit).Take(limit).ToList();
                response.Code = 200;
                response.Count = query.Count();
          

                // RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 添加常用语
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> AddUsefulExpressionsManager(UsefulExpressionsManager usefulExpressionsManager)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();

            try
            {
                usefulExpressionsManager.UsefulExpressionsManagerID= Guid.NewGuid().ToString();
                bool result = await usefulExpressionsManagerService.AddUsefulExpressionsManager(usefulExpressionsManager);
                if (result == true)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "添加常用语成功";
                    logger.LogInformation("添加常用语成功");
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "添加常用语失败";
                    logger.LogInformation("添加常用语失败");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return responseFormat;
        }

        /// <summary>
        /// 删除常用语
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> DeleteUsefulExpressionsManager(string Id)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();

            try
            {
                bool result = await usefulExpressionsManagerService.DeleteUsefulExpressionsManager(Id);
                if (result == true)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "删除常用语成功";
                    logger.LogInformation("删除常用语成功");
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "删除常用语失败";
                    logger.LogInformation("删除常用语失败");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return responseFormat;
        }
        /// <summary>
        /// 获取修改的单条信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<UsefulExpressionsManager>> GetById(string Id)
        {
            ResponseFormat<UsefulExpressionsManager> responseFormat = new ResponseFormat<UsefulExpressionsManager>();
            try
            {
                List<UsefulExpressionsManager> emps = await usefulExpressionsManagerService.GetUsefulExpressionsManagerWhereAsync(o => o.UsefulExpressionsManagerID.Equals(Id));
                responseFormat.Data = emps.FirstOrDefault();
                return responseFormat;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 修改敏感字
        /// </summary>
        /// <param name="sensibilityManager"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateUsefulExpressionsManager(UsefulExpressionsManager  usefulExpressionsManager)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();

            try
            {
                
                bool result = await usefulExpressionsManagerService.UpdateUsefulExpressionsManager(usefulExpressionsManager);
                if (result == true)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "修改常用语成功";
                    logger.LogInformation("修改常用语成功");
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "修改常用语失败";
                    logger.LogInformation("修改常用语失败");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return responseFormat;
        }

    }
}
