﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LabelController : ControllerBase
    {
        public ILabelService labelService { get; set; }
        public LabelController(ILabelService labelService)
        {
            this.labelService = labelService;
        }
        [HttpGet]
        public async Task<DtreeFormat> GetLabel(string Pid="1")
        {
            DtreeFormat dTrees = new DtreeFormat();
            List<TreeNodes> list = new List<TreeNodes>();
            try
            {
                GetLabels(Pid, list);
                dTrees.data = list;
                return dTrees;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public void GetLabels(string Pid,List<TreeNodes> labels)
        {
            List<Label> menus = labelService.GetLabels().Result;
            menus = menus.Where(o => o.LabelPid == Pid).ToList();
            if (menus != null)
            {
                foreach (var item in menus.Where(o => o.LabelPid == Pid))
                {
                    TreeNodes nodes = new TreeNodes();
                    nodes.value = item.LabelTypeID;
                    nodes.label = item.LabelName;
                    nodes.LabelPid = item.LabelPid;
                    nodes.children = new List<TreeNodes>();
                    labels.Add(nodes);
                    GetLabels(item.LabelTypeID, nodes.children);
                }
            }
        }
        [HttpGet]
        public async Task<ResponseFormat<List<Label>>> GetResponseFormat(string Pid="0")
        {

            ResponseFormat<List<Label>> responseFormat = new ResponseFormat<List<Label>>();
            try
            {
               List<Label> list = await labelService.GetLabels();
                var query = from a in list 
                            select new Label
                            {
                                LabelName = a.LabelName,
                                LabelPid=a.LabelPid,
                                LabelTypeID=a.LabelTypeID
                                
                            };
                string id = Convert.ToString(Pid);
                if (id=="0"||id==null)
                {
                    id = "";
                }
                if (!string.IsNullOrEmpty(id))
                {
                    query = query.Where(o => o.LabelPid.Contains(id)).ToList();
                }
                responseFormat.Data = query.ToList();
                responseFormat.Code = 200;
            }
            catch (Exception ex)
            {

                throw;
            }
            return responseFormat;
        }

        [HttpPost]
        public async Task<ResponseFormat<bool>> AddLabel(Label label)
        {
            label.LabelTypeID = Guid.NewGuid().ToString();
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {
                bool result = await labelService.AddLabel(label);
                if (result)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "添加成功";
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                responseFormat.Code = 500;
                responseFormat.Msg = "系统繁忙";
                throw;
            }
            return responseFormat;
        }
        /// <summary>
        /// 删除标签
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> DeleteLabel(string Id)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();

            try
            {
                bool result = await labelService.DeleteLabel(Id);
                if (result)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "删除成功";
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return responseFormat;
        }
    }
}
