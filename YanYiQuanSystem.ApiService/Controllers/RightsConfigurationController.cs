﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RightsConfigurationController : ControllerBase
    {
        private IRightsConfigurationService  rightsConfigurationService { get; set; }//员工服务层

        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis


        public RightsConfigurationController(IRightsConfigurationService rightsConfigurationService, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper)
        {
            this.rightsConfigurationService = rightsConfigurationService;
            this.logger = loggerFactory.CreateLogger<RightsConfigurationController>();
            this.RedisDbHelper = redisDbHelper;
        }
        /// <summary>
        /// 异步获取所有信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<RightsConfiguration>>> GetRightsConfigurationAsync()
        {
            try
            {
                ResponseFormat<List<RightsConfiguration>> response = new ResponseFormat<List<RightsConfiguration>>();

                response.Data = await rightsConfigurationService.GetRightsConfigurationAsync();
                response.Code = 0;
                logger.LogInformation("获取数据成功");
                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 分页查询显示
        /// </summary>
        /// <param name="EmpName"></param>
        /// <param name="Phone"></param>
        /// <param name="UserName"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<RightsConfiguration>>> GetRightsConfigurations(string RightsName = "",int RightsClass=0,int RightsRole=0, int page = 1, int limit = 3)
        {
            ResponseFormat<List<RightsConfiguration>> response = new ResponseFormat<List<RightsConfiguration>>();
            List<RightsConfiguration>  rightsConfigurations = await rightsConfigurationService.GetRightsConfigurationAsync();
            var query = from r in rightsConfigurations
                        select new RightsConfiguration
                        {
                            RightsId = r.RightsId,
                            RightsName = r.RightsName,
                            Sort = r.Sort,
                            RightsClass=r.RightsClass,
                            RightsRole=r.RightsRole,
                            RightWay=r.RightWay,
                            Explain=r.Explain,
                            Start=r.Start
                        };
            var count = query.Count();
            if (!string.IsNullOrEmpty(RightsName))
            {
                query = query.Where(o => o.RightsName.Contains(RightsName)).ToList();
            }
            if (RightsClass > 0)
            {
                query = query.Where(o => o.RightsClass==RightsClass).ToList();
            }
            if (RightsRole > 0)
            {
                query = query.Where(o => o.RightsRole == RightsRole).ToList();
            }
            response.Data = query.OrderBy(o => o.RightsRole).Skip((page - 1) * limit).Take(limit).ToList();
            response.Count = query.Count();
            return response;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddRightsConfiguration([FromBody] RightsConfiguration  rightsConfiguration)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            rightsConfiguration.RightsId = Guid.NewGuid().ToString();
            bool n = rightsConfigurationService.AddRightsConfiguration(rightsConfiguration).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }
        /// <summary>
        /// 根据Id获取信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<RightsConfiguration>> ShowRightsConfigurationById(string id)
        {
            List<RightsConfiguration>  rightsConfigurations = await rightsConfigurationService.GetEmpWhereAsync(o => o.RightsId.Equals(id));
            ResponseFormat<RightsConfiguration> responseFormat = new ResponseFormat<RightsConfiguration>();
            responseFormat.Data = rightsConfigurations.FirstOrDefault();
            return responseFormat;
        }
        /// <summary>
        /// 修改
        /// </summary>
        [HttpPost]
        public IActionResult UpdateRightsConfiguration(RightsConfiguration  rightsConfiguration)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = rightsConfigurationService.UpdateRightsConfiguration(rightsConfiguration).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "修改成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "修改失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);

        }

    }
}
