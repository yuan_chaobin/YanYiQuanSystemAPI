﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.EntityFrameWorkCoreOracle;
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SystemInfromController : ControllerBase
    {
        public ISystemInfromService systemInfromService { get; set; }
        public SystemInfromController(ISystemInfromService systemInfromService)
        {
            this.systemInfromService = systemInfromService;
        }
        public async Task<ResponseFormat<List<CommonData>>> GetSystemInfrom(string Content = "", int page = 1, int limit = 2)
        {
            ResponseFormat<List<CommonData>> responseFormat = new ResponseFormat<List<CommonData>>();
            try
            {// 获取所有短信信息
                List<SystemInfrom> list = await systemInfromService.GetSystemInfrom();
                var query = from a in list
                            select new CommonData
                            {
                                InfromID = a.InfromID,
                                Content = a.Content,
                                CreateTime = a.CreateTime,
                                UserId = a.UserId,
                                IsValid=a.IsValid
                            };
                //模糊查询
                if (!string.IsNullOrEmpty(Content))
                {
                    query = query.Where(o => o.Content.Contains(Content)).ToList();
                }
                responseFormat.Data = query.OrderBy(o => o.NodeID).Skip((page - 1) * limit).Take(limit).ToList();
                responseFormat.Code = 200;
                responseFormat.Count = query.Count();

            }
            catch (Exception ex)
            {

                throw;
            }
            return responseFormat;
        }

        [HttpPost]
        public IActionResult AddsystemInfrom([FromBody] SystemInfrom  systemInfrom)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            systemInfrom.InfromID = Guid.NewGuid().ToString();
            systemInfrom.CreateTime = DateTime.Now;
            systemInfrom.UserId = "1";
            systemInfrom.IsValid = 1;
           
            bool n = systemInfromService.AddSystemInfrom(systemInfrom).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }
        /// <summary>
        /// 员工的删除
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>

        [HttpPost]
        public async Task<ResponseFormat<bool>> DeletesystemInfrom(string Id)
        {
            
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            bool n = await systemInfromService.DeleteSystemInfrom(Id);
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return response ;
        }

        /// <summary>
        /// 根据角色Id获取角色的信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<SystemInfrom>> GetByID(string id)
        {
            List<SystemInfrom> emps = await systemInfromService.GetSystemInfromBywere(o => o.InfromID.Equals(id));
            ResponseFormat<SystemInfrom> responseFormat = new ResponseFormat<SystemInfrom>();
            responseFormat.Data = emps.FirstOrDefault();
            return responseFormat;
        }
        /// <summary>

        /// </summary>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateSystemInfrom(SystemInfrom  systemInfrom)
        {
            systemInfrom.CreateTime = DateTime.Now;
            systemInfrom.UserId = "1";
            systemInfrom.IsValid = 1;
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {
                bool result = await systemInfromService.UpdateSystemInfrom(systemInfrom);
                if (result)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "修改成功";
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "修改失败";
                }

            }
            catch (Exception)
            {
                responseFormat.Code = 500;
                responseFormat.Msg = "系统繁忙";
                throw;
            }
            return responseFormat;
        }

    }
}
