﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserRightsController : ControllerBase
    {
        private IUserRightsService UserRightsService { get; set; }
        private IUsersService UsersService { get; set; }
        private IPurchaseRecordService PurchaseRecordService { get; set; }

        public UserRightsController(IUserRightsService userRightsService, IUsersService usersService, IPurchaseRecordService purchaseRecord)
        {
            this.UserRightsService = userRightsService;
            this.UsersService = usersService;
            this.PurchaseRecordService = purchaseRecord;
        }


        /// <summary>
        /// 异步获取所有用户权益列表数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetUserRights(string name="",string phone="",string riName="")
        {
            try
            {
                ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

                List<UserRights> urList = await UserRightsService.GetUserRights();
                List<User> userList = await UsersService.GetUsersAsync();

                var query = from a in urList
                            join b in userList on a.UsersID equals b.UserId
                            select new CommonData
                            {
                                UserRightsId = a.UserRightsId,
                                UsersID = a.UsersID,
                                RightsRole = a.RightsRole,
                                RightsName = a.RightsName,
                                RightsType = a.RightsType,
                                RightsTypeWay = a.RightsTypeWay,
                                Times = a.Times,
                                StratTime = a.StratTime,
                                EndTime = a.EndTime,
                                Remarks = a.Remarks,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = b.Birthday,
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };
                //姓名
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(o => o.UserName.Contains(name)).ToList();
                }
                //手机号
                if (!string.IsNullOrEmpty(phone))
                {
                    query = query.Where(o => o.PhoneNumber == phone).ToList();
                }
                //身份证号
                if (!string.IsNullOrEmpty(riName))
                {
                    query = query.Where(o => o.RightsName == riName).ToList();
                }
                response.Data = query.ToList();
                response.Count = response.Data.Count();
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// 异步获取所有用户权益购买记录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetPurchaseRecord(string name = "", string phone = "")
        {
            try
            {
                ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

                List<PurchaseRecord> userList = await PurchaseRecordService.GetListAsync();
                List<UserRights> urList = await UserRightsService.GetUserRights();
                List<User> users = await UsersService.GetUsersAsync();

                var query = from a in userList
                            join b in urList on a.UserRightsID equals b.UserRightsId
                            join c in users on b.UsersID equals c.UserId
                            select new CommonData
                            {
                                PurchaseRecordId = a.PurchaseRecordId,
                                UserRightsID = a.UserRightsID,
                                BuyRole = a.BuyRole,
                                ProductName = a.ProductName,
                                MoneySum = a.MoneySum,
                                DemandPayTime = a.DemandPayTime,
                                SuccessPayTime = a.SuccessPayTime,
                                UserRightsId = b.UserRightsId,
                                UsersID = b.UsersID,
                                RightsRole = b.RightsRole,
                                RightsName = b.RightsName,
                                RightsType = b.RightsType,
                                RightsTypeWay = b.RightsTypeWay,
                                Times = b.Times,
                                StratTime = b.StratTime,
                                EndTime = b.EndTime,
                                Remarks = b.Remarks,
                                UserId = c.UserId,
                                UserName = c.UserName,
                                Password = c.Password,
                                Sex = c.Sex,
                                CurrentRole = c.CurrentRole,
                                Birthday = c.Birthday,
                                HeadPortrait = c.HeadPortrait,
                                IdentityNumber = c.IdentityNumber,
                                PhoneNumber = c.PhoneNumber,
                                PresentAddress = c.PresentAddress,
                                UsersRank = c.UsersRank
                            };
                //姓名
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(o => o.UserName.Contains(name)).ToList();
                }
                //手机号
                if (!string.IsNullOrEmpty(phone))
                {
                    query = query.Where(o => o.PhoneNumber == phone).ToList();
                }
                response.Data = query.ToList();
                response.Count = response.Data.Count();
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
