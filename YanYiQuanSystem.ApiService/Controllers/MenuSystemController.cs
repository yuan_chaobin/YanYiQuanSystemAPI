﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MenuSystemController : ControllerBase
    {
        /// <summary>
        /// 菜单表的接口注入
        /// </summary>
        public IMenuSystemService menuSystemService { get; set; }
        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis
        private IRoleService roleService { get; set; }//角色注入
        private IEmpService empService { get; set; }
        private IRole_MenuService role_MenuService { get; set; }

        public MenuSystemController(IMenuSystemService menuSystemService, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper,IRoleService roleService, IEmpService empService, IRole_MenuService role_MenuService)
        {
            this.menuSystemService = menuSystemService;
            this.logger = loggerFactory.CreateLogger<UsersController>();
            this.RedisDbHelper = redisDbHelper;
            this.roleService = roleService;
            this.empService = empService;
            this.role_MenuService = role_MenuService;
        }
        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<DtreeFormat> GetMenuSystem(string EmpID= "")//审核员
        {
            try
            {
                DtreeFormat dTrees = new DtreeFormat();

                List<TreeNodes> list = new List<TreeNodes>();

                List<Emp> emps = await empService.GetEmpWhereAsync(o=>o.EmpId.Equals(EmpID));//获取员工

                List<Role> roles = await roleService.GetRoleAsync();//获取所有角色信息

                List<MenuSystem> menus =await menuSystemService.GetMenuSystemAsync();//获取菜单信息

                List<Role_Menu> role_Menus = await role_MenuService.GetRole_MenuAsync();//获取角色菜单信息
                //组合查询菜单id
                var query = from a in emps
                            join b in roles on a.RoleId equals b.RoleId
                            join c in role_Menus on b.RoleId equals c.RoleId
                            join m in menus on c.MenuID equals m.MenuID
                            select m;

                GetMenu("0", list,query.ToList());
                
               
                // RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                dTrees.data = list;
                return dTrees;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }

     /// <summary>
     /// 
     /// </summary>
     /// <param name="MenuId"></param>
     /// <param name="treeNodes"></param>
     /// <param name="menus"></param>
        private void GetMenu(string MenuId, [FromBody]List<TreeNodes> treeNodes, [FromBody] List<MenuSystem> menus)
        {
            
             var getData=  menus.Where(o => o.MenuPid == MenuId && o.Isdelete == 0).ToList();
            if (menus!=null)
            {
                foreach (var item in getData)
                {
                    TreeNodes nodes = new TreeNodes();
                    nodes.value = item.MenuID;
                    nodes.label = item.MenuName;
                    nodes.MenuUrl = item.MenuUrl;
                    nodes.MenuPid = item.MenuPid;
                    nodes.ModifyTime = item.ModifyTime;
                    nodes.Isdelete = item.Isdelete;
                    nodes.MenuIcon1 = item.MenuIcon1;
                    nodes.CreateTime = item.CreateTime;
                    nodes.children = new List<TreeNodes>();
                    treeNodes.Add(nodes);
                    GetMenu(item.MenuID, nodes.children,menus);
                }
            }
          
        }
        /// <summary>
        /// 获取菜单的数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetMenus()
        {
            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();
            try
            {
                List<MenuSystem> menuSystems= await menuSystemService.GetMenuSystemAsync();
                var query = from a in menuSystems
                            select new CommonData
                            {
                                MenuName=a.MenuName,
                                MenuUrl=a.MenuUrl,
                                Is_delete=a.Isdelete,
                                CreateTime=a.CreateTime
                            };
                response.Count = query.Count();
                response.Data = query.ToList(); 
                response.Code = 200;
              
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return response;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> DeleteMenu(string Id)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {                
                List<TreeNodes> list = new List<TreeNodes>();
                List<MenuSystem> menus = new List<MenuSystem>();
                MenuSystem menu = new MenuSystem();
                menu.MenuID = Id;
                menus.Add(menu);
                GetDeleteMenu(Id, list);
               
                foreach (var  m in list)
                {
                    MenuSystem menu1 = new MenuSystem();
                    menu1.MenuID = m.value;
                    menus.Add(menu1);
                }
                Console.WriteLine(menus);
                foreach (var item in menus)
                {
                    responseFormat.Data= await  menuSystemService.DeleteMenuSystemAsync(item.MenuID);
                }
                if (responseFormat.Data==true)
                {
                    responseFormat.Msg = "删除成功";
                    responseFormat.Code = 200;
                    logger.LogInformation("删除成功");
                }
                else
                {
                    responseFormat.Msg = "删除失败";
                    responseFormat.Code = 300;
                    logger.LogInformation("删除失败");
                }
                return responseFormat;
                
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        
        }
        //获取删除子节点
        [HttpGet]
        public void GetDeleteMenu(string Pid, List<TreeNodes> treeNodes)
        {
          List<MenuSystem>  menus = menuSystemService.GetMenuSystemAsync().Result;
            menus = menus.Where(o => o.MenuPid == Pid).ToList();
            if (menus.Count>0)
            {
                foreach (var item in menus.Where(o => o.MenuPid == Pid))
                {
                    TreeNodes nodes = new TreeNodes();
                    nodes.value = item.MenuID;
                    nodes.children = new List<TreeNodes>();
                    treeNodes.Add(nodes);
                    GetDeleteMenu(item.MenuID, nodes.children);
                }
            }


        }
        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="menuSystem"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> AddMenu(MenuSystem menuSystem)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {
                menuSystem.MenuID = Guid.NewGuid().ToString();
                menuSystem.CreateTime = DateTime.Now;
                menuSystem.ModifyTime = DateTime.Now;
                if (menuSystem.MenuPid==null|| menuSystem.MenuPid=="")
                {
                    menuSystem.MenuPid = "0";
                }
              
                bool result = await menuSystemService.AddMenuSystemAsync(menuSystem);
                if (result)
                {
                    responseFormat.Msg = "添加菜单成功";
                    responseFormat.Code = 200;
                    logger.LogInformation("添加成功");
                }
                else
                {
                    responseFormat.Msg = "添加菜单失败";
                    responseFormat.Code = 200;
                    logger.LogInformation("添加失败");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return responseFormat;
        }

        /// <summary>
        /// 获取单条菜单
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<MenuSystem>> GetById(string Id)
        {
            ResponseFormat<MenuSystem> responseFormat = new ResponseFormat<MenuSystem>();
            try
            {
                List<MenuSystem> emps = await menuSystemService.GetMenuSystemsWhereAsync(o => o.MenuID.Equals(Id));
                responseFormat.Data = emps.FirstOrDefault();
                return responseFormat;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 修改菜单
        /// </summary>
        /// <param name="menuSystem"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateMenu(MenuSystem menuSystem)
        {
            ResponseFormat<bool> responseFormat = new ResponseFormat<bool>();
            try
            {
                menuSystem.ModifyTime = DateTime.Now;
                bool result = await menuSystemService.UpdateMenuSystemAsync(menuSystem);
                if (result == true)
                {
                    responseFormat.Code = 200;
                    responseFormat.Msg = "修改菜单成功";
                    logger.LogInformation("修改菜单成功");
                }
                else
                {
                    responseFormat.Code = 300;
                    responseFormat.Msg = "修改菜单失败";
                    logger.LogInformation("修改菜单失败");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
            return responseFormat;
        }

        /// <summary>
        /// 获取所有菜单
        /// </summary>
        [HttpGet]

        public async Task<DtreeFormat> GetMenuAll()
        {
            try
            {
                DtreeFormat dTrees = new DtreeFormat();
                List<TreeNodes> list = new List<TreeNodes>();

                GetMenuAlls("0", list);

                // RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");

                dTrees.data = list;
                return dTrees;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        [HttpGet]
        public void GetMenuAlls(string Pid, List<TreeNodes> treeNodes)
        {
            List<MenuSystem> menus = menuSystemService.GetMenuSystemAsync().Result;
            menus = menus.Where(o => o.MenuPid == Pid && o.Isdelete == 0).ToList();
            if (menus != null)
            {
                foreach (var item in menus.Where(o => o.MenuPid == Pid))
                {
                    TreeNodes nodes = new TreeNodes();
                    nodes.value = item.MenuID;
                    nodes.label = item.MenuName;
                    nodes.MenuUrl = item.MenuUrl;
                    nodes.MenuPid = item.MenuPid;
                    nodes.ModifyTime = item.ModifyTime;
                    nodes.Isdelete = item.Isdelete;
                    nodes.MenuIcon1 = item.MenuIcon1;
                    nodes.CreateTime = item.CreateTime;
                    nodes.children = new List<TreeNodes>();
                    treeNodes.Add(nodes);
                    GetMenuAlls(item.MenuID, nodes.children);
                }
            }


        }

    }
  
}
