﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using YanYiQuanSystem.CommonUnitity;
using YanYiQuanSystem.Domain;
using YanYiQuanSystem.IService;
using YanYiQuanSystem.Service;

namespace YanYiQuanSystem.ApiService.Controllers
{


    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ModelLibraryController : ControllerBase
    {
        private IModelLibraryService ModelLibraryService { get; set; }
        private IAssistantService AssistantService { get; set; }
        private IUsersService UsersService { get; set; }
        private IPhotoAblumService PhotoAblumService { get; set; }
        private IFirstPageModelService FirstPageModelService { get; set; }
        private IAttentionService AttentionService { get; set; }

        public ModelLibraryController(IModelLibraryService modelLibraryService, IUsersService usersService, IPhotoAblumService photoAblumService, IFirstPageModelService firstPageModelService, IAttentionService attentionService, AssistantService assistantService)
        {
            this.ModelLibraryService = modelLibraryService;
            this.AssistantService = assistantService;
            this.UsersService = usersService;
            this.PhotoAblumService = photoAblumService;
            this.FirstPageModelService = firstPageModelService;
            this.AttentionService = attentionService;
        }

        /// <summary>
        /// 异步获取所有模特数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetModelLibrary(string userName = "", string phone = "", string nickName = "", string card = "", int firstPage = 0, int pageIndex = 1, int pageSize = 3)
        {
            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

            try
            {
                List<ModelsLibrary> mlList = await ModelLibraryService.GetListAsync();//模特列表
                List<User> userList = await UsersService.GetUsersAsync();//用户列表
                List<PhotoAlbums> photos = await PhotoAblumService.GetListAsync();

                var query = from a in mlList
                            join b in userList on a.UserID equals b.UserId
                            where b.CurrentRole == 1 && a.ApproveStatus == 2
                            select new CommonData
                            {
                                ModelId = a.ModelId,
                                NickName = a.NickName,
                                UserID = a.UserID,
                                IsAssistant = a.IsAssistant,
                                RegisterTime = a.RegisterTime,
                                InvitationCode = a.InvitationCode,
                                Education = a.Education,
                                HonorThat = a.HonorThat,
                                MyCode = a.MyCode,
                                Constellation = a.Constellation,
                                Height = a.Height,
                                Weight = a.Weight,
                                Size = a.Size,
                                TheChest = a.TheChest,
                                Waistline = a.Waistline,
                                Hipline = a.Hipline,
                                BloodType = a.BloodType,
                                IsActivity = a.IsActivity,
                                LoginAccount = a.LoginAccount,
                                IsLogging1 = a.IsLogging1,
                                IsFirstPage = a.IsFirstPage,
                                ApproveStatus = a.ApproveStatus,
                                PhotoStatus = a.PhotoStatus,
                                LoggingStatus = a.LoggingStatus,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = Convert.ToDateTime(b.Birthday),
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };

                //模特姓名
                if (!string.IsNullOrEmpty(userName))
                {
                    query = query.Where(o => o.UserName.Contains(userName)).ToList();
                }
                //手机号
                if (!string.IsNullOrEmpty(phone))
                {
                    query = query.Where(o => o.PhoneNumber == phone).ToList();
                }
                //昵称
                if (!string.IsNullOrEmpty(nickName))
                {
                    query = query.Where(o => o.NickName.Contains(nickName)).ToList();
                }
                //身份证号
                if (!string.IsNullOrEmpty(card))
                {
                    query = query.Where(o => o.IdentityNumber == card).ToList();
                }
                //是否在首页
                if (firstPage != 0)
                {
                    query = query.Where(o => o.IsFirstPage == firstPage).ToList();
                }
                response.Data = query.OrderBy(o => o.ModelId).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                response.Code = 0;
                response.Count = query.Count();

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 获取不在首页的模特
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetModels(/*string userName = "",*/ string phone = "", /*string nickName = "", string card = "", int firstPage = 0,*/ int pageIndex = 1, int pageSize = 3)
        {
            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

            try
            {
                List<ModelsLibrary> mlList = await ModelLibraryService.GetListAsync();//模特列表
                List<User> userList = await UsersService.GetUsersAsync();//用户列表
                List<PhotoAlbums> photos = await PhotoAblumService.GetListAsync();

                var query = from a in mlList
                            join b in userList on a.UserID equals b.UserId
                            where b.CurrentRole == 1 && a.ApproveStatus == 2 && a.IsFirstPage != 1
                            select new CommonData
                            {
                                ModelId = a.ModelId,
                                NickName = a.NickName,
                                UserID = a.UserID,
                                IsAssistant = a.IsAssistant,
                                RegisterTime = a.RegisterTime,
                                InvitationCode = a.InvitationCode,
                                MyCode = a.MyCode,
                                Constellation = a.Constellation,
                                Height = a.Height,
                                Weight = a.Weight,
                                Size = a.Size,
                                TheChest = a.TheChest,
                                Waistline = a.Waistline,
                                Hipline = a.Hipline,
                                BloodType = a.BloodType,
                                IsActivity = a.IsActivity,
                                LoginAccount = a.LoginAccount,
                                IsLogging1 = a.IsLogging1,
                                IsFirstPage = a.IsFirstPage,
                                ApproveStatus = a.ApproveStatus,
                                PhotoStatus = a.PhotoStatus,
                                LoggingStatus = a.LoggingStatus,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = Convert.ToDateTime(b.Birthday),
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };

                ////模特姓名
                //if (!string.IsNullOrEmpty(userName))
                //{
                //    query = query.Where(o => o.UserName.Contains(userName)).ToList();
                //}
                //手机号
                if (!string.IsNullOrEmpty(phone))
                {
                    query = query.Where(o => o.PhoneNumber == phone).ToList();
                }
                ////昵称
                //if (!string.IsNullOrEmpty(nickName))
                //{
                //    query = query.Where(o => o.NickName.Contains(nickName)).ToList();
                //}
                ////身份证号
                //if (!string.IsNullOrEmpty(card))
                //{
                //    query = query.Where(o => o.IdentityNumber == card).ToList();
                //}
                ////是否在首页
                //if (firstPage != 0)
                //{
                //    query = query.Where(o => o.IsFirstPage == firstPage).ToList();
                //}
                response.Data = query.OrderBy(o => o.ModelId).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                response.Code = 0;
                response.Count = query.Count();

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// 异步获取首页的模特数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetFirstPageModel(int pageIndex = 1, int pageSize = 5)
        {
            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

            try
            {
                List<User> userList = await UsersService.GetUsersAsync();//用户列表
                List<ModelsLibrary> mlList = await ModelLibraryService.GetListAsync();//模特列表
                List<FirstPageModels> pageList = await FirstPageModelService.GetListAsync(); //首页模特列表

                var query = from a in userList
                            join b in mlList on a.UserId equals b.UserID
                            join c in pageList on b.ModelId equals c.ModelID
                            where b.IsFirstPage == 1
                            select new CommonData
                            {
                                ModelId = b.ModelId,
                                NickName = b.NickName,
                                UserID = b.UserID,
                                IsAssistant = b.IsAssistant,
                                RegisterTime = b.RegisterTime,
                                InvitationCode = b.InvitationCode,
                                MyCode = b.MyCode,
                                IsFirstPage = b.IsFirstPage,
                                Education = b.Education,
                                HonorThat = b.HonorThat,
                                Constellation = b.Constellation,
                                Height = b.Height,
                                Weight = b.Weight,
                                Size = b.Size,
                                TheChest = b.TheChest,
                                Waistline = b.Waistline,
                                Hipline = b.Hipline,
                                BloodType = b.BloodType,
                                IsActivity = b.IsActivity,
                                LoginAccount = b.LoginAccount,
                                IsLogging1 = b.IsLogging1,
                                ApproveStatus = b.ApproveStatus,
                                PhotoStatus = b.PhotoStatus,
                                LoggingStatus = b.LoggingStatus,
                                UserId = a.UserId,
                                UserName = a.UserName,
                                Password = a.Password,
                                Sex = a.Sex,
                                CurrentRole = a.CurrentRole,
                                Birthday = Convert.ToDateTime(a.Birthday),
                                HeadPortrait = a.HeadPortrait,
                                IdentityNumber = a.IdentityNumber,
                                PhoneNumber = a.PhoneNumber,
                                PresentAddress = a.PresentAddress,
                                UsersRank = a.UsersRank,
                                FirstModelId = c.FirstModelId,
                                ModelID = c.ModelID,
                                SortNum = c.SortNum
                            };
                response.Data = query.OrderBy(o => o.SortNum).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                response.Code = 0;
                response.Count = query.Count();

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// 根据ID进行单条数据删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> DeleteModelLibrary(string Id)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ModelLibraryService.DeleteData(Id);
                response.Code = 0;
                response.Data = result;

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// 根据模特ID数据修改是否在首页状态
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateFirstPage(List<string> Ids)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ModelLibraryService.UpdateFirstPage(Ids);
                response.Code = 200;
                response.Data = result;

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// 添加模特
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> AddModelLibrary(ModelsLibrary modelsLibrary)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ModelLibraryService.AddData(modelsLibrary);
                response.Code = 0;
                response.Data = result;

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 微信小程序ID反填模特详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> wxGetModelById(string Id)
        {
            string Idd= JsonConvert.DeserializeObject<string>(Id);

            return await GetModelById(Idd);
        }


        /// <summary>
        /// 根据ID反填模特详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetModelById(string Id)
        {

            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

            try
            {
                List<ModelsLibrary> mlList = await ModelLibraryService.GetListAsync();//模特列表
                List<User> userList = await UsersService.GetUsersAsync();//用户列表

                var query = from a in mlList
                            join b in userList on a.UserID equals b.UserId
                            where b.CurrentRole == 1
                            select new CommonData
                            {
                                ModelId = a.ModelId,
                                NickName = a.NickName,
                                UserID = a.UserID,
                                IsAssistant = a.IsAssistant,
                                RegisterTime = a.RegisterTime,
                                InvitationCode = a.InvitationCode,
                                Education = a.Education,
                                HonorThat = a.HonorThat,
                                MyCode = a.MyCode,
                                Constellation = a.Constellation,
                                Height = a.Height,
                                Weight = a.Weight,
                                Size = a.Size,
                                TheChest = a.TheChest,
                                Waistline = a.Waistline,
                                Hipline = a.Hipline,
                                BloodType = a.BloodType,
                                IsActivity = a.IsActivity,
                                LoginAccount = a.LoginAccount,
                                IsLogging1 = a.IsLogging1,
                                ApproveStatus = a.ApproveStatus,
                                PhotoStatus = a.PhotoStatus,
                                LoggingStatus = a.LoggingStatus,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = Convert.ToDateTime(b.Birthday),
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };

                if (!string.IsNullOrEmpty(Id))
                {
                    query = query.Where(o => o.ModelId == Id).ToList();
                }
                response.Data = query.ToList();
                response.Code = 0;
                response.Count = query.Count();

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 修改模特数据
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateModels(CommonData models)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ModelLibraryService.UpdateUserModel(models);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 修改用户等级
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseFormat<bool>> UpdateUserRank(User user)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ModelLibraryService.UpdateUserRank(user);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 获取所有的相册库信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<PhotoAlbums>>> GetPhotoAlbum(string userId, int aduit = 0)
        {
            ResponseFormat<List<PhotoAlbums>> response = new ResponseFormat<List<PhotoAlbums>>();

            try
            {
                List<PhotoAlbums> photoAlbums = await PhotoAblumService.GetListAsync();

                var query = from a in photoAlbums
                            where a.UserID == userId
                            select new PhotoAlbums
                            {
                                PhotoId = a.PhotoId,
                                UserID = a.UserID,
                                Photos = a.Photos,
                                AuditStatus = a.AuditStatus
                            };
                if (aduit != 0)
                {
                    query = query.Where(o => o.AuditStatus == aduit).ToList();
                }
                response.Data = query.ToList();
                response.Code = 200;
                response.Count = query.Count();

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// 修改个人相册
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseFormat<bool>> UpdateMyImgs([FromForm]string img,[FromForm] string user)
        {
            List<string> imgs = JsonConvert.DeserializeObject<List<string>>(img);
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                List<PhotoAlbums> photoAlbums = new List<PhotoAlbums>();
                foreach (var item in imgs)
                {
                    PhotoAlbums photo = new PhotoAlbums();
                    photo.PhotoId = Guid.NewGuid().ToString();
                    photo.UserID = user;
                    photo.AuditStatus = 1;
                    photo.Photos = item;
                    photoAlbums.Add(photo);
                }
                bool n = await PhotoAblumService.DeleteData(user);
                bool m = await PhotoAblumService.BatchAddData(photoAlbums);
                if(n && m)
                {
                    response.Code = 200;
                    response.Data = true;
                    response.Msg = "修改成功";
                }else
                {
                    response.Code = 300;
                    response.Data = false;
                    response.Msg = "修改失败";
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }


        /// <summary>
        /// 修改照片审核状态
        /// </summary>
        /// <param name="photoId"></param>
        /// <returns></returns>
        public async Task<ResponseFormat<bool>> UpdateAuditStatus(string photoId)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ModelLibraryService.UpdateAduitStatus(photoId);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// 单个修改是否在首页
        /// </summary>
        /// <param name="modelsLibrary"></param>
        /// <returns></returns>
        public async Task<ResponseFormat<bool>> UpdateFirst(ModelsLibrary modelsLibrary)
        {
            ResponseFormat<bool> response = new ResponseFormat<bool>();
            try
            {
                bool result = await ModelLibraryService.UpdateFirst(modelsLibrary);
                response.Code = 200;
                response.Data = result;
                response.Msg = "修改成功";

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }

        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetModelLibrarys()
        {
            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

            try
            {
                List<ModelsLibrary> mlList = await ModelLibraryService.GetListAsync();//模特列表
                List<User> userList = await UsersService.GetUsersAsync();//用户列表
                List<PhotoAlbums> photos = await PhotoAblumService.GetListAsync();

                var query = from a in mlList
                            join b in userList on a.UserID equals b.UserId
                            where b.CurrentRole == 1 && a.ApproveStatus == 2
                            select new CommonData
                            {
                                ModelId = a.ModelId,
                                NickName = a.NickName,
                                UserID = a.UserID,
                                IsAssistant = a.IsAssistant,
                                RegisterTime = a.RegisterTime,
                                InvitationCode = a.InvitationCode,
                                MyCode = a.MyCode,
                                Constellation = a.Constellation,
                                Height = a.Height,
                                Weight = a.Weight,
                                Size = a.Size,
                                TheChest = a.TheChest,
                                Waistline = a.Waistline,
                                Hipline = a.Hipline,
                                BloodType = a.BloodType,
                                IsActivity = a.IsActivity,
                                LoginAccount = a.LoginAccount,
                                IsLogging1 = a.IsLogging1,
                                IsFirstPage = a.IsFirstPage,
                                ApproveStatus = a.ApproveStatus,
                                PhotoStatus = a.PhotoStatus,
                                LoggingStatus = a.LoggingStatus,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = Convert.ToDateTime(b.Birthday),
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };


                response.Data = query.OrderBy(o => o.ModelId).ToList();
                response.Code = 0;
                response.Count = query.Count();

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }



        /// <summary>
        /// 根据ID反填用户关注
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetModelAttention(string Id)
        {

            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

            try
            {
                List<ModelsLibrary> mlList = await ModelLibraryService.GetListAsync();//模特列表
                List<User> userList = await UsersService.GetUsersAsync();//用户列表
                List<Attention> attentions = await AttentionService.GetListAsync();

                var query = from a in mlList  
                            join b in userList on a.UserID equals b.UserId
                            join c in attentions on b.UserId equals c.UserID
                            select new CommonData
                            {
                                ModelId = a.ModelId,
                                NickName = a.NickName,
                                UserID = a.UserID,
                                IsAssistant = a.IsAssistant,
                                RegisterTime = a.RegisterTime,
                                InvitationCode = a.InvitationCode,
                                Education = a.Education,
                                HonorThat = a.HonorThat,
                                MyCode = a.MyCode,
                                Constellation = a.Constellation,
                                Height = a.Height,
                                Weight = a.Weight,
                                Size = a.Size,
                                TheChest = a.TheChest,
                                Waistline = a.Waistline,
                                Hipline = a.Hipline,
                                BloodType = a.BloodType,
                                IsActivity = a.IsActivity,
                                LoginAccount = a.LoginAccount,
                                IsLogging1 = a.IsLogging1,
                                ApproveStatus = a.ApproveStatus,
                                PhotoStatus = a.PhotoStatus,
                                LoggingStatus = a.LoggingStatus,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = Convert.ToDateTime(b.Birthday),
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank,
                                AttentionId = c.AttentionId,
                                AttentionTime = c.AttentionTime,
                                FollowerID = c.FollowerID,
                                FollowerType = c.FollowerType,
                                UserType = c.UserType
                            };

                if (!string.IsNullOrEmpty(Id))
                {
                    query = query.Where(o => o.FollowerID == Id).ToList();
                }
                response.Data = query.ToList();
                response.Code = 0;
                response.Count = query.Count();

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }
    }
}
