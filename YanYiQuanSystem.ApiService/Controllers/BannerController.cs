﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BannerController : ControllerBase
    {
        private IBannerService bannerService { get; set; }//员工服务层

        private readonly ILogger logger;    //NLog
        private RedisDbHelper RedisDbHelper { get; set; } //redis
        private IServiceActivity serviceActivity { get; set; }


        public BannerController( IBannerService bannerService, ILoggerFactory loggerFactory, RedisDbHelper redisDbHelper, IServiceActivity serviceActivity)
        {
            this.bannerService = bannerService;
            this.logger = loggerFactory.CreateLogger<UsersController>();
            this.RedisDbHelper = redisDbHelper;
            this.serviceActivity = serviceActivity;
        }

        /// <summary>
        /// 异步获取所有Banner信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Banners>>> GetBannerAsync()
        {
            try
            {
                ResponseFormat<List<Banners>> response = new ResponseFormat<List<Banners>>();

                response.Data = await bannerService.GetBannerAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");
                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 异步获取所有活动信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Activity>>> GetActivityAsync()
        {
            try
            {
                ResponseFormat<List<Activity>> response = new ResponseFormat<List<Activity>>();

                response.Data = await serviceActivity.GetListAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");
                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 分页查询显示
        /// </summary>
        /// <param name="EmpName"></param>
        /// <param name="Phone"></param>
        /// <param name="UserName"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Banners>>> GetBanners(string AdvertisingPlanning = "", string Uuid = "", int page = 1, int limit = 3)
        {
            ResponseFormat<List<Banners>> response = new ResponseFormat<List<Banners>>();
            List<Activity> activities = await serviceActivity.GetListAsync();
            List<Banners> banners = await bannerService.GetBannerAsync();
            var query = from b in banners
                        join a in activities on b.Uuid equals a.Uuid
                        select new Banners
                        {
                            BannerId = b.BannerId,
                            AdvertisingPlanning = b.AdvertisingPlanning,
                            ActiveTime =b.ActiveTime,
                            Sort = b.Sort,
                            ToShare = b.ToShare,
                            Title =b.Title,
                            Images= b.Images,
                            Uuid = a.Desc_activity,
                            ExpirationTime = b.ExpirationTime,
                            ShareURL = b.ShareURL,
                            Content = b.Content,
                            MobileURL = b.MobileURL
                        };
            var count = query.Count();
            if (!string.IsNullOrEmpty(AdvertisingPlanning))
            {
                query = query.Where(o => o.AdvertisingPlanning.Contains(AdvertisingPlanning)).ToList();
            }
            if (!string.IsNullOrEmpty(Uuid))
            {
                query = query.Where(o => o.Uuid.Contains(Uuid)).ToList();
            }
            response.Data = query.OrderBy(o => o.Sort).Skip((page - 1) * limit).Take(limit).ToList();
            response.Count = query.Count();
            return response;
        }


        /// <summary>
        /// 添加Banner
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddBanner([FromBody] Banners banner)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            banner.BannerId = Guid.NewGuid().ToString();
            bool n = bannerService.AddBanner(banner).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }

        /// <summary>
        /// 根据BannerId获取的信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<Banners>> ShowBannerById(string id)
        {
            List<Banners> banners  = await bannerService.GetBannerWhereAsync(o => o.BannerId.Equals(id));
            ResponseFormat<Banners> responseFormat = new ResponseFormat<Banners>();
            responseFormat.Data = banners.FirstOrDefault();
            return responseFormat;
        }

        /// <summary>
        /// 员工的修改
        /// </summary>
        [HttpPost]
        public IActionResult UpdateBanner(Banners banner)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = bannerService.UpdateBanner(banner).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "修改成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "修改失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);

        }
    }
}
