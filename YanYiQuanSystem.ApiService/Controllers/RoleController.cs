﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YanYiQuanSystem.ApiService.Controllers
{
    using Microsoft.Extensions.Logging;
    using YanYiQuanSystem.CommonUnitity;
    using YanYiQuanSystem.Domain;
    using YanYiQuanSystem.IService;
    using YanYiQuanSystem.RedisHelper;
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private IRoleService roleService { get; set; }
        private readonly ILogger logger;    //NLog
        private IModelLibraryService ModelLibraryService { get; set; }
        private IUsersService UsersService { get; set; }
        public RoleController(IRoleService roleService, ILoggerFactory loggerFactory,IModelLibraryService modelLibraryService, IUsersService usersService)
        {
            this.roleService = roleService;
            this.logger = loggerFactory.CreateLogger<UsersController>();
            this.ModelLibraryService = modelLibraryService;
            this.UsersService = usersService;
        }
        /// <summary>
        /// 异步获取所有角色
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Role>>> GetRoleAsync()
        {
            try
            {
                ResponseFormat<List<Role>> response = new ResponseFormat<List<Role>>();

                response.Data = await roleService.GetRoleAsync();
                response.Code = 0;
                //   RedisDbHelper.BatchAdd<Users>(response.Data, "UserName");
                logger.LogInformation("获取数据成功");
                return response;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 分页查询显示
        /// </summary>
        /// <param name="EmpName"></param>
        /// <param name="Phone"></param>
        /// <param name="UserName"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<Role>>> GetRoles(string RoleName = "", int page = 1, int limit = 3)
        {
            ResponseFormat<List<Role>> response = new ResponseFormat<List<Role>>();
            List<Role> roles = await roleService.GetRoleAsync();
            var query = from r in roles
                        select new Role
                        {
                            RoleId = r.RoleId,
                            RoleName = r.RoleName,
                            Remarks =r.Remarks
                        };
            var count = query.Count();
            if (!string.IsNullOrEmpty(RoleName))
            {
                query = query.Where(o => o.RoleName.Contains(RoleName)).ToList();
            }
            response.Data = query.OrderBy(o => o.RoleId).Skip((page - 1) * limit).Take(limit).ToList();
            response.Count = query.Count();
            return response;
        }

        /// <summary>
        /// 添加员工
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddRole([FromBody] Role role)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            role.RoleId = Guid.NewGuid().ToString();
            bool n = roleService.AddRole(role).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "添加成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "添加失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }
        /// <summary>
        /// 员工的删除
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>

        [HttpPost]
        public IActionResult DeleteRole([FromBody] Role role)
        {
            string Id = role.RoleId;
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = roleService.DeleteRole(Id).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "删除成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "删除失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);
        }

        /// <summary>
        /// 根据角色Id获取角色的信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<Role>> ShowRoleById(string id)
        {
            List<Role> emps = await roleService.GetRoleWhereAsync(o => o.RoleId.Equals(id));
            ResponseFormat<Role> responseFormat = new ResponseFormat<Role>();
            responseFormat.Data = emps.FirstOrDefault();
            return responseFormat;
        }
        /// <summary>
        /// 角色的修改
        /// </summary>
        [HttpPost]
        public IActionResult UpdateRole(Role role)
        {
            ResponseFormat<string> response = new ResponseFormat<string>();
            bool n = roleService.UpdateRole(role).Result;
            try
            {
                if (n)
                {
                    response.Code = 200;
                    response.Msg = "修改成功";
                }
                else
                {
                    response.Code = 300;
                    response.Msg = "修改失败";
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Msg = "网络错误";
            }
            return Ok(response);

        }



        /// <summary>
        /// 异步获取所有模特数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<List<CommonData>>> GetModelLibrary(string userName = "", string phone = "", string card = "", int page = 1, int limit = 5)
        {
            ResponseFormat<List<CommonData>> response = new ResponseFormat<List<CommonData>>();

            try
            {
                List<ModelsLibrary> mlList = await ModelLibraryService.GetListAsync();//模特列表
                List<User> userList = await UsersService.GetUsersAsync();//用户列表
                var query = from a in mlList
                            join b in userList on a.UserID equals b.UserId where a.IsActivity==1
                            select new CommonData
                            {
                                ModelId = a.ModelId,
                                NickName = a.NickName,
                                UserID = a.UserID,
                                IsAssistant = a.IsAssistant,
                                RegisterTime = a.RegisterTime,
                                InvitationCode = a.InvitationCode,
                                MyCode = a.MyCode,
                                Constellation = a.Constellation,
                                Height = a.Height,
                                Weight = a.Weight,
                                Size = a.Size,
                                TheChest = a.TheChest,
                                Waistline = a.Waistline,
                                Hipline = a.Hipline,
                                BloodType = a.BloodType,
                                IsActivity = a.IsActivity,
                                LoginAccount = a.LoginAccount,
                                IsLogging1 = a.IsLogging1,
                                ApproveStatus = a.ApproveStatus,
                                PhotoStatus = a.PhotoStatus,
                                LoggingStatus = a.LoggingStatus,
                                UserId = b.UserId,
                                UserName = b.UserName,
                                Password = b.Password,
                                Sex = b.Sex,
                                CurrentRole = b.CurrentRole,
                                Birthday = Convert.ToDateTime(b.Birthday),
                                HeadPortrait = b.HeadPortrait,
                                IdentityNumber = b.IdentityNumber,
                                PhoneNumber = b.PhoneNumber,
                                PresentAddress = b.PresentAddress,
                                UsersRank = b.UsersRank
                            };

                //模特姓名
                if (!string.IsNullOrEmpty(userName))
                {
                    query = query.Where(o => o.UserName.Contains(userName)).ToList();
                }
                //手机号
                if (!string.IsNullOrEmpty(phone))
                {
                    query = query.Where(o => o.PhoneNumber.Contains(phone)).ToList();
                }
                //身份证号
                if (!string.IsNullOrEmpty(card))
                {
                    query = query.Where(o => o.IdentityNumber.Contains(card)).ToList();
                }
                response.Data = query.OrderBy(o => o.ModelId).Skip((page - 1) * limit).Take(limit).ToList();
                response.Code = 0;
                response.Count = query.Count();

                return response;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                throw;
            }
        }
        /// <summary>
        /// 根据ID获取模特信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseFormat<ModelsLibrary>> ShowModelsById(string id)
        {
            List<ModelsLibrary> emps = await ModelLibraryService.GetEmpWhereAsync(o => o.ModelId.Equals(id));
            ResponseFormat<ModelsLibrary> responseFormat = new ResponseFormat<ModelsLibrary>();
            responseFormat.Data = emps.FirstOrDefault();
            return responseFormat;
        }
    }
}
